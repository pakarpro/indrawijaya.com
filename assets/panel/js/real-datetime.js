function real_datetime(id) {
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    h = date.getHours();
    if( h < 10) {
        h = "0"+h;
    }
    m = date.getMinutes();
    if( m < 10 ) {
        m = "0"+m;
    }
    s = date.getSeconds();
    if( s < 10 ) {
        s = "0"+s;
    }
    _date = days[day]+', '+d+' '+months[month]+' '+year;
    result = _date.bold()+' '+h+':'+m+':'+s+' WIB';
    document.getElementById(id).innerHTML = result;
    setTimeout('real_datetime("'+id+'");','1000');
    return true;
}