<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('ACTIVITIES', 'activities');
define('AREA', 'area');
define('COMMENTS', 'comments');
define('MAIL', 'mail');
define('MEDIAS', 'medias');
define('MEDIA_RELATIONSHIPS', 'media_relationships');
define('POSTS', 'posts');
define('BIOGRAPHS', 'biographs');
define('BIOGRAPH_TERMS', 'biograph_terms');
define('SOCIAL_MEDIA', 'social_media');
define('SOCIAL_MEDIA_ACCOUNT', 'social_media_account');
define('SOCIAL_MEDIA_CREDENTIALS', 'social_media_credentials');
define('TERMS', 'terms');
define('TERM_RELATIONSHIPS', 'term_relationships');
define('TERM_TAXONOMY', 'term_taxonomy');
define('UPLOADS', 'uploads');
define('UPLOADS_THUMB', 'uploads_thumb');
define('USERS', 'users');
define('USER_ROLE', 'user_role');

class Crud {
    
    private $CI;
    
    public function __construct() {
        $this->CI = &get_instance();
    }

    public function get($params) {
        if( !is_array($params) && empty($params) ) {
            return array();
        } else {
            // select
            if( isset($params['fields']) ) {
                $fields = '*';
                if( is_array($params['fields']) ) {
                    $fields = implode(', ', $params['fields']);
                } else {
                    $fields = $params['fields'];
                }
                $this->CI->db->select($fields);
            }

            // select_min
            if( isset($params['select_min']) ) {
                $this->CI->db->select_min($params['select_min']);
            }

            // select_max
            if( isset($params['select_max']) ) {
                $this->CI->db->select_max($params['select_max']);
            }

            // select_avg
            if( isset($params['select_avg']) ) {
                $this->CI->db->select_avg($params['select_avg']);
            }

            // select_sum
            if( isset($params['select_sum']) ) {
                $this->CI->db->select_sum($params['select_sum']);
            }

            // table
            if( isset($params['table']) ) {
                $this->CI->db->from($params['table']);
            }

            // where
            if( isset($params['where']) ) {
                foreach($params['where'] as $field => $value) {
                    $this->CI->db->where($field, $value);
                }
            }

            // or_where
            if( isset($params['or_where']) ) {
                foreach($params['or_where'] as $field => $value) {
                    $this->CI->db->or_where($field, $value);
                }
            }

            // join
            if( isset($params['join']) ) {
                foreach($params['join'] as $a => $b) {
                    $this->CI->db->join($a, $b);
                }
            }

            // like
            if( isset($params['like']) ) {
                foreach($params['like'] as $field => $value) {
                    if( is_array($value) ) {
                        foreach($value as $key => $val) {
                            $this->CI->db->like($key, $val);
                        }
                    } else {
                        $this->CI->db->like($field, $value);
                    }
                }
            }

            // or_like
            if( isset($params['or_like']) ) {
                foreach($params['or_like'] as $field => $value) {
                    if( is_array($value) ) {
                        foreach($value as $key => $val) {
                            $this->CI->db->or_like($key, $val);
                        }
                    } else {
                        $this->CI->db->or_like($field, $value);
                    }
                }
            }

            // group_by
            if( isset($params['group_by']) ) {
                $this->CI->db->group_by($params['group_by']);
            }

            // order_by
            if( isset($params['order_by']) ) {
                foreach($params['order_by'] as $field => $type) {
                    $this->CI->db->order_by($field, $type);
                }
            }

            // limit
            if( isset($params['limit']) ) {
                if( is_array($params['limit']) ) {
                    $limit = $params['limit'][0];
                    $offset = $params['limit'][1];
                    $this->CI->db->limit($limit, $offset);
                } else {
                    $this->CI->db->limit($params['limit']);
                }
            }

            $get = $this->CI->db->get();
            if( isset($params['count']) ) {
                if( $get->num_rows() != 0 ) {
                    $return = $get->num_rows();
                } else {
                    $return = 0;
                }
            } else {
                $return = $get->result_array();
            }
                
            return $return;
        }
    }
    
    public function set($params, $type='insert') {
        if( !is_array($params) && empty($params) ) {
            return array();
        } else {
            switch($type) {
                case 'insert':
                    $this->CI->db->insert($params['table'], $params['data']);
                    $last_id = array('id' => $this->CI->db->insert_id());
                    return $last_id;
                    break;
                case 'update':
                    $this->CI->db->trans_start();
                    // key parameter for updating data
                    if( isset($params['where']) ) {
                        foreach($params['where'] as $field => $value) {
                            $this->CI->db->where($field, $value);
                        }
                    }

                    // update action
                    $this->CI->db->update($params['table'], $params['data']);
                    $this->CI->db->trans_complete();
                    
                    if( $this->CI->db->trans_status() === FALSE ) {
                        return 'FAILED';
                    } else {
                        return 'SUCCESSFUL';
                    }
                    break;
                default:
                    return array();
            }
        }
    }
    
    public function delete($params) {
        if( !is_array($params) && empty($params) ) {
            return array();
        } else {
            // criteria for deleting data
            $this->CI->db->trans_start();
            if( isset($params['where']) ) {
                foreach($params['where'] as $field => $value) {
                    $this->CI->db->where($field, $value);
                }
            }
            
            // delete action
            $this->CI->db->delete($params['table']);
            $this->CI->db->trans_complete();
            
            if( $this->CI->db->trans_status() === FALSE ) {
                return 'FAILED';
            } else {
                return 'SUCCESSFUL';
            }
        }
    }
}
