<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $admin_data['user_login']; ?></div>
                <div class="email"><?php echo $admin_data['user_email']; ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?php echo site_url('panel/profile'); ?>"><i class="material-icons">edit</i>Edit My Profile</a></li>
                        <li><a href="<?php echo site_url('panel/logout'); ?>"><i class="material-icons">input</i>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MENU</li>
                <li <?php if(!empty($menu) && $menu=='dashboard'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel');?>">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                <li <?php if(!empty($menu) && $menu=='mail'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/mail/list');?>" class="waves-effect">
                        <i class="material-icons">message</i>
                        <span>Mail</span>
                        <?php if( isset($unread_mail) ) {
                            echo '<span style="position:absolute;right:15px;">('.$unread_mail.')</span>';
                        } ?>
                    </a>
                </li>

                <?php /*
                <li <?php if(!empty($menu) && $menu=='slider'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/slider');?>" class="waves-effect waves-block">
                        <i class="material-icons">perm_media</i>
                        <span>Slider</span>
                    </a>                    
                </li>
                */ ?>
                
                <li <?php if(!empty($menu) && $menu=='article'){ echo 'class="active"';}?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">description</i>
                        <span>Posts</span>
                    </a>
                    <ul class="ml-menu">
                        <li <?php if(!empty($submenu) && $submenu=='all'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/article/list');?>">All Posts</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='category'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/term/category/list');?>">Category</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='tag'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/term/tag/list');?>">Tag</a>
                        </li>
                    </ul>
                </li>
                
                <?php if( $admin_data['role'] == 1 ) { ?>
                    <li <?php if(!empty($menu) && $menu=='user'){ echo 'class="active"';}?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">group</i>
                            <span>Users</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if(!empty($submenu) && $submenu=='user'){ echo 'class="active"';}?>>
                                <a href="<?php echo site_url('panel/user/list');?>">All Users</a>
                            </li>
                            <li <?php if(!empty($submenu) && $submenu=='user-role'){ echo 'class="active"';}?>>
                                <a href="<?php echo site_url('panel/user-role/list');?>">User Role</a>
                            </li>
                        </ul>
                    </li>

                    <li <?php if(!empty($menu) && $menu=='social-media'){ echo 'class="active"';}?>>
                        <a href="<?php echo site_url('panel/account/list');?>" class="waves-effect waves-block">
                            <i class="material-icons">public</i>
                            <span>Social Media</span>
                        </a>
                    </li>
                    
                    <li <?php if(!empty($menu) && $menu=='biograph'){ echo 'class="active"';}?>>
                        <a href="<?php echo site_url('panel/biograph');?>" class="waves-effect waves-block">
                            <i class="material-icons">person_pin</i>
                            <span>Biography</span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 www.indrawijaya.com <br>
                <a href="https://github.com/gurayyarar/AdminBSBMaterialDesign">Template by Material Design</a>.
            </div>
            <div class="version">
                <b>Version: </b> 2017.7.8.1
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
