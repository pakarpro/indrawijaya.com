<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/dashboard';
    private $admin_data = array();
    private $validation_msg = array(
        'required' => '%s is required',
        'min_length' => '%s at least %d characters',
        'max_length' => '%s maximum length is %d characters'
    );
    private $validation_config = array(
        array(
            'field'   => 'username',
            'label'   => 'Username',
            'rules'   => 'trim|required|min_length[5]|max_length[30]'
        ),
        array(
            'field'   => 'password',
            'label'   => 'Password',
            'rules'   => 'trim|required|min_length[8]|max_length[30]'
        )
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }

    // as login page
    public function index() {
        if( !empty($this->admin_data) ) {
            $this->dashboard();
        } else {
            $this->login();
        }
    }
    
    // check session login
    private function _check_session_login() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('login'.$this->redirect);
        }
    }
    
    // getting user info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    public function login() {
        // redirect
        if( isset($_GET['rdr']) ) {
            $this->redirect = $_GET['rdr'];
        }
        $data['rdr'] = '?rdr='.urlencode($this->redirect);
        
        if( !empty($_POST) ) {
            // get post data
            $post_data = $this->input->post();

            // validation
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $this->load->helper('user/user');
                
                $check_param['where'] = array(
                    'user_login' => strtolower($post_data['username']),
                    'user_pass' => enc_password($post_data['password'])
                );
                $this->load->model('user/User_model');
                $count = $this->User_model->countUser('user', $check_param);
                if( $count == 1 ) {
                    $login_data = $this->User_model->getUser('user', $check_param);
                    $login_data = array_shift($login_data);

                    if( $login_data['user_status'] == 0 ) {
                        $error_login = 'Account is not active.';
                        $data['error_login'] = error_msg_generator($error_login);
                    } else {
                        // set session
                        $this->session->set_userdata('is_login', $login_data['user_id']);

                        redirect($this->redirect);
                    }
                } else {
                    $error_login = 'Invalid username or password.';
                    $data['error_login'] = error_msg_generator($error_login);
                }
            }
        }
        
        $this->load->view('sign-in', $data);
    }
    
    public function logout() {
        $this->session->unset_userdata('is_login');
        
        redirect('panel');
    }

    public function dashboard() {
        // check session login
        $this->_check_session_login();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);

        if( $data['admin_data']['role'] == 1 ) {
            // get total users
            $this->load->helper('user/user');
            $data['active_user'] = count_user('active');
        }
        
        // get unread mail
        $this->load->helper('mail/mail');
        $data['unread_msg'] = count_mail('unread');
        
        // get total active article per user
        $this->load->model('article/Article_model');
        $post_param['where'] = array('post_author' => $id_user);
        $data['posted_article'] = $this->Article_model->countArticle('post', $post_param);
        
        $this->load->view('dashboard', $data);
    }
}
