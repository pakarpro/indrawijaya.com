<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('count_mail')){
    function count_mail($type='all'){
        $CI = &get_instance();
        $CI->load->model('mail/Mail_model');
        
        $param = array();
        switch($type) {
            case 'read':
                $param['where'] = array('is_read' => 1);
                break;
            case 'unread':
                $param['where'] = array('is_read' => 0);
                break;
        }
        $count = $CI->Mail_model->countMail($param);

        return $count;
    }
}
