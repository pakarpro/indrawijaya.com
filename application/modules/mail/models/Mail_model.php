<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_model extends CI_model {
    
    private $data;
    private $return = array();
    private $mail = array(
        'mail_id',
        'date_sent',
        'date_read',
        'sender_ip',
        'sender_name',
        'sender_email',
        'sender_phone',
        'subject',
        'message',
        'is_read'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewMail($data) {
        $params['table'] = MAIL;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getMail($params=array()) {
        // fields
        $fields = $this->mail;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = MAIL;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getMailById($id) {
        $params['fields'] = $this->mail;
        $params['table'] = MAIL;
        $params['where'] = array(
            'mail_id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countMail($params=array()) {
        $params['table'] = MAIL;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateMail($data, $id) {
        $params['table'] = MAIL;
        $params['data'] = $data;
        $params['where'] = array(
            'mail_id' => $id
        );
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteMail($id) {
        $params['table'] = MAIL;
        $params['where'] = array(
            'mail_id' => $id
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}