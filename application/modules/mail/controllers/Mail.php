<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel';
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'sender_name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z\s\'\-\,\.]+$/]'
        ),
        array(
            'field'   => 'sender_email',
            'label'   => 'Email',
            'rules'   => 'trim|required|valid_email'
        ),
        array(
            'field'   => 'sender_phone',
            'label'   => 'Phone',
            'rules'   => 'trim|numeric|min_length[7]'
        ),
        array(
            'field'   => 'subject',
            'label'   => 'Subject',
            'rules'   => 'trim|required|min_length[10]'
        ),
        array(
            'field'   => 'message',
            'label'   => 'Message',
            'rules'   => 'trim|required|min_length[30]'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'regex_match' => '%s does not follow approriate format',
        'valid_email' => '%s is not valid',
        'numeric' => 'Use number only',
        'min_length' => '%s should be at least %d characters'
    );

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('mail');
        $this->load->model('Mail_model');

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
    }
    
    // getting user info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    // save new message
    private function _save($data) {
        // date send
        $data['date_sent'] = DATETIME;
        // client ip
        $data['sender_ip'] = IP_ADDR;

        // save to database
        $save = $this->Mail_model->setNewMail($data);
        if( isset($save['id']) ) {
            return true;
        } else {
            return false;
        }
    }
    
    private function _set_read_status($id, $status='read') {
        $is_read = 1;
        if( $status == 'unread' ) {
            $is_read = 0;
        }
        $update_param = array(
            'date_read' => DATETIME,
            'is_read' => $is_read
        );
        $update = $this->Mail_model->updateMail($update_param, $id);
    }

    // for ajax use, send mail through contact page
    public function send() {
        $post_data = $this->input->post();
        
        $data['status'] = 'failed';
        if( !empty($post_data) ) {
            // validation
            $this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $save = $this->_save($post_data);
                if( $save == true ) {
                    $data['status'] = 'success';
                }
            } else {
                foreach($post_data as $k => $val) {
                    $error_msg[$k] = form_error($k);
                }
                $data['status'] = 'failed';
                $data['message'] = $error_msg;
            }
        }

        echo json_encode($data);
    }
    
    public function index() {
        $this->lists();
    }
    
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        $count_mail = count_mail();
        if( $count_mail > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            $where_param['limit'] = array($limit_per_page, $start_row);
            $where_param['order_by'] = array('date_sent' => 'DESC');
                
            if( $count_mail > $limit_per_page ) {
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/mail'),
                    'total_rows'    =>  $count_mail,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            $data['data'] = $this->Mail_model->getMail($where_param);
        }

        $this->load->view('list-mail', $data);
    }
    
    // show detail of message
    public function view($id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $mail = $this->Mail_model->getMailById($id);
            if( !empty($mail) ) {
                $data['data'] = $mail;

                // set read status to 1 and update the date_read
                $this->_set_read_status($id);
            }
        }

        $this->load->view('view-mail', $data);
    }
    
    // delete mail
    public function delete($id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting mail. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 || is_numeric($id) ) {
            // check if given ID is existing in database
            $mail = $this->Mail_model->getMailById($id);
            // if data is found
            if( !empty($mail) ) {
                // run delete action
                $delete = $this->Mail_model->deleteMail($id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = 'Mail from <b>'.$mail['sender_name'].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/mail/list');
    }
}