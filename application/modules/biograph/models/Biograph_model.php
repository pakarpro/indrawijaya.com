<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Biograph_model extends CI_model {
    
    public $data;
    public $return = array();
    private $biographs = array(
        'biograph_id',
        'biograph_metakey',
        'biograph_value',
        'sorter'
    );
    private $biograph_terms = array(
        'biograph_term_id',
        'biograph_term_gid',
        'biograph_term_name'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewBiograph($type, $data) {
        switch($type) {
            case 'biograph': $table = BIOGRAPHS; break;
            case 'term': $table = BIOGRAPH_TERMS; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getBiograph($type, $params=array()) {
        switch($type) {
            case 'biograph':
                $fields = $this->biographs;
                $table = BIOGRAPHS;
                break;
            case 'term':
                $fields = $this->biograph_terms;
                $table = BIOGRAPH_TERMS;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
//        print_r($params); exit;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getBiographById($type, $id) {
        switch($type) {
            case 'biograph':
                $field_id = 'biograph_id';
                $fields = $this->biographs;
                $table = BIOGRAPHS;
                break;
            case 'term':
                $field_id = 'biograph_term_id';
                $fields = $this->biographs;
                $table = BIOGRAPHS;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getBiographByGid($gid) {
        $params['fields'] = $this->biograph_terms;
        $params['table'] = BIOGRAPH_TERMS;
        $params['where'] = array(
            'biograph_term_gid' => $gid
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countBiograph($type, $params=array()) {
        switch($type) {
            case 'biograph':
                $field_id = 'biograph_id';
                $table = BIOGRAPHS;
                break;
            case 'term':
                $field_id = 'biograph_term_id';
                $table = BIOGRAPH_TERMS;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateBiograph($type, $data, $param) {
        switch($type) {
            case 'biograph':
                $field_id = 'biograph_id';
                $table = BIOGRAPHS;
                break;
            case 'term':
                $field_id = 'biograph_term_id';
                $table = BIOGRAPH_TERMS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteBiograph($type, $param) {
        switch($type) {
            case 'biograph':
                $field_id = 'biograph_id';
                $table = BIOGRAPHS;
                break;
            case 'term':
                $field_id = 'biograph_term_id';
                $table = BIOGRAPH_TERMS;
                break;
        }
        $where = array($field_id => $param);
        if( is_array($where) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['where'] = $where;
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}