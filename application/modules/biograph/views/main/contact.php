<div class="form-group">
    <div class="row">
        <div class="col-xs-12">
            <label>Address</label>
            <?php echo form_error('address'); ?>
            <div class="form-line">
                <input type="text" name="address" class="form-control" placeholder="e.g. Jl. Bungur Besar 17 No. 6" value="<?php echo $biograph['address']; ?>">
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-4">
            <label class="form-label">Province</label>
            <?php echo form_error('province'); ?>
            <select id="province" name="province" class="form-control show-tick" data-live-search="true">
                <option value="0">- Choose -</option>
                <?php
                    foreach($province as $k => $item) {
                        $selected = '';
                        if( $item['area_id'] == $biograph['province'] ) {
                            $selected = 'selected';
                        }
                        echo '<option value="'.$item['area_id'].'" '.$selected.'>'.$item['area_name'].'</option>';
                    }
                ?>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">City</label>
            <?php echo form_error('city'); ?>
            <select id="city" name="city" class="form-control show-tick" data-live-search="true">
                <option value="0">- Choose -</option>
                <?php
                    foreach($city as $k => $item) {
                        $selected = '';
                        if( $item['area_id'] == $biograph['city'] ) {
                            $selected = 'selected';
                        }
                        echo '<option value="'.$item['area_id'].'" '.$selected.'>'.$item['area_name'].'</option>';
                    }
                ?>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">District</label>
            <?php echo form_error('district'); ?>
            <select id="district" name="district" class="form-control show-tick" data-live-search="true">
                <option value="0">- Choose -</option>
                <?php
                    foreach($district as $k => $item) {
                        $selected = '';
                        if( $item['area_id'] == $biograph['district'] ) {
                            $selected = 'selected';
                        }
                        echo '<option value="'.$item['area_id'].'" '.$selected.'>'.$item['area_name'].'</option>';
                    }
                ?>
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <label>Email</label>
    <?php echo form_error('email'); ?>
    <div class="form-line">
        <input name="email" type="email" class="form-control" value="<?php echo $biograph['email']; ?>">
    </div>
</div>

<div class="form-group">
    <label>Phone</label>
    <?php echo form_error('phone'); ?>
    <div class="form-line">
        <input name="phone" type="number" class="form-control" value="<?php echo $biograph['phone']; ?>">
    </div>
</div>

<div class="form-group">
    <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15">UPDATE</button>
</div>
