<div class="form-group">
    <label>Name</label>
    <?php echo form_error('name'); ?>
    <div class="form-line">
        <input name="name" type="text" class="form-control" value="<?php echo $biograph['name']; ?>" required="required" autofocus>
    </div>
</div>

<div class="form-group">
    <label>Place of Birth</label>
    <?php echo form_error('place_of_birth'); ?>
    <div class="form-line">
        <input name="place_of_birth" type="text" class="form-control" value="<?php echo $biograph['place_of_birth']; ?>">
    </div>
</div>

<div class="form-group">
    <label>Date of Birth</label>
    <?php echo form_error('date_of_birth'); ?>
    <div class="form-line">
        <input name="date_of_birth" id="date_of_birth" type="text" class="form-control" value="<?php echo $biograph['date_of_birth']; ?>">
    </div>
</div>

<div class="form-group">
    <label>Spouse</label>
    <?php echo form_error('spouse'); ?>
    <div class="form-line">
        <input name="spouse" type="text" class="form-control" value="<?php echo $biograph['spouse']; ?>">
    </div>
</div>

<div class="form-group">
    <label>Children</label>
    <?php echo form_error('children'); ?>
    <div class="form-line">
        <input name="children" type="number" min="0" class="form-control" value="<?php echo $biograph['children']; ?>">
    </div>
</div>

<div class="form-group">
    <label>Biographical Info</label>
    <div class="form-line">
        <textarea class="form-control" id="biographical_info" name="biographical_info"><?php echo $biograph['biographical_info']; ?></textarea>
    </div>
</div>

<div class="form-group">
    <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15">UPDATE</button>
</div>
