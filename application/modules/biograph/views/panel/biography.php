<?php echo call_header('panel', 'Biography'); ?>

<?php echo call_sidebar($admin_data, 'biograph', 'user'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">BIOGRAPHY</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <?php echo $this->session->flashdata('notif'); ?>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <?php
                                    foreach($terms as $k => $term) {
                                        // gid
                                        $gid = $term['biograph_term_gid'];
                                        // name
                                        $name = strtoupper($term['biograph_term_name']);
                                        // active
                                        $active = '';
                                        if( $term['biograph_term_gid'] == $type ) {
                                            $active = 'class="active"';
                                            $name = '<b>'.$name.'</b>';
                                        }
                                ?>
                                    <li role="presentation" <?php echo $active; ?>>
                                        <a href="<?php echo site_url('panel/biograph/'.$term['biograph_term_gid']); ?>"><?php echo $name; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>

                            <?php if( isset($biograph) ) { ?>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="<?php echo $gid; ?>">
                                        <form action="<?php echo site_url('panel/biograph/'.$type); ?>" method="post">
                                            <?php echo $biograph; ?>
                                        </form>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <p class="m-t-15">An error accured while seeing this page. Please try again later.</p>
                            <?php } ?>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<?php if( $type == 'profile' ) { ?>
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'autosize/autosize.js'); ?>"></script>
    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'momentjs/moment.js'); ?>"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
    <!-- TinyMCE -->
    <script src="<?php echo base_url(PLG_PATH.'tinymce/tinymce.js'); ?>"></script>
<?php } ?>
<script>
    // updating user role
    updateUserRole();
    // updating status of selected data
    $(document).on('click', '._cs', function() {
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post('/user/change-status', data);
    });

    function updateUserRole() {
        $(document).on('click', '._cur', function() {
            var id = $(this).parent().parent().attr('data-id');
            var role = $('#user_role-'+id).val();

            var data = {
                "id": id,
                "user_role": role
            }

            $.post('/user/change-role', data);
        });
    }
    
    <?php if($type == 'profile') { ?>
        // for editor, use tinymce for better
        tinymce.init({
            selector: "textarea#biographical_info",
            theme: "modern",
            height: 200,
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
            image_advtab: true
        });
        tinymce.suffix = ".min";
        tinymce.relative_urls = false;

        //Datetimepicker plugin
        $('#date_of_birth').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 0,
            time: false
        });
    <?php } ?>
        
    <?php if($type == 'contact') { ?>
        // ajax for location [province, city, district]
        // change city data
        $(document).on('change', '#province', function() {
            var i = 0;
            var id = $('#province').val();
            var data = {id: id}
            console.log(data);

            $.post('/area/get-city', data, function(response) {
                if( response != '' || response != '[]' ) {
                    var obj = JSON.parse(response);
                    var count = obj.length;

                    $('#city').html('<option value="0">- Choose -</option>');
                    $('#district').html('<option value="0">- Choose -</option>');
                    for(i; i<count; i++) {
                        $('#city').append(
                            '<option value="'+ obj[i].area_id +'">'+ obj[i].area_name +'</option>'
                        );
                    }
                    $('#city').selectpicker('refresh');
                    $('#district').selectpicker('refresh');
                }
            });
        });

        // change district data
        $(document).on('change', '#city', function() {
            var i = 0;
            var id = $('#city').val();
            var data = {id: id}

            $.post('/area/get-district', data, function(response) {
                if( response != '' || response != '[]' ) {
                    var obj = JSON.parse(response);
                    var count = obj.length;

                    $('#district').html('<option value="0">- Choose -</option>');
                    for(i; i<count; i++) {
                        $('#district').append(
                            '<option value="'+ obj[i].area_id +'">'+ obj[i].area_name +'</option>'
                        );
                    }
                    $('#district').selectpicker('refresh');
                }
            });
        });
    <?php } ?>

    <?php if($type == 'education' || $type == 'career') { ?>
        $(document).on('click', '._l', function() {
            var id = $(this).attr('data-id');
            var yearfrom = $(this).attr('data-from');
            var yearto = $(this).attr('data-to');
            var title = $(this).attr('data-title');
            var description = $(this).attr('data-desc');
            console.log(id);
            console.log(yearfrom);
            console.log(yearto);
            console.log(title);
            console.log(description);
            
            // get all data and store in form input
            
        });
    <?php } ?>
</script>