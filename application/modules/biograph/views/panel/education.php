<div class="form-group">
    <label>Year</label>
    <div class="row">
        <div class="col-md-3">
            <div class="form-line">
                <input id="year_from" name="year_from" type="number" class="form-control" placeholder="From" value="<?php if( isset($detail['year_from']) ) echo $detail['year_from']; ?>" required autofocus>
            </div>
            <?php
                echo form_error('year_from');
                if( isset($detail['error_year_from']) ) { echo $detail['error_year_from']; }
            ?>
        </div>
        <div class="col-md-1 text-center">-</div>
        <div class="col-md-3">
            <div class="form-line">
                <input id="year_to" name="year_to" type="number" min="0" class="form-control" value="<?php if( isset($detail['year_to']) ) echo $detail['year_to']; ?>" placeholder="To">
            </div>
            <?php
                echo form_error('year_to');
                if( isset($detail['error_year_to']) ) { echo $detail['error_year_to']; }
            ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label>School/University/Course</label>
    <?php echo form_error('title'); ?>
    <div class="form-line">
        <input id="title" name="title" type="text" class="form-control" value="<?php if( isset($detail['title']) ) echo $detail['title']; ?>" required>
    </div>
</div>

<div class="form-group">
    <label>Description</label>
    <div class="form-line">
        <textarea class="form-control" id="description" name="description" rows="5"><?php if( isset($detail['description']) ) echo $detail['description']; ?></textarea>
        <?php if( isset($detail['sorter']) ) { ?>
            <input name="sorter" type="hidden" value="<?php echo $detail['sorter']; ?>">
        <?php } ?>
    </div>
</div>

<div class="form-group">
    <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15">SUBMIT</button>
</div>

<?php if( !empty($biograph) ) { ?>
    <hr>
    <h2 class="card-inside-title">LIST</h2>
    <div class="body table-responsive">
        <table class="table table-hover table-list">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="100">Year</th>
                    <th>Title - Description</th>
                    <th width="30"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    foreach($biograph as $k => $item) {
                        // year
                        $year_from = $item['year_from'];
                        $year_to = $item['year_to'];
                        if( empty($year_to) ) {
                            $year_to = 'Present';
                            $year = $year_from.' - '.$year_to;
                        } elseif( !empty($year_to) && $year_to == $year_from ) {
                            $year = $year_from;
                        } else {
                            $year = $year_from.' - '.$year_to;
                        }
                ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $year; ?></td>
                        <td>
                            <span class="text-bold" id="t_title"><?php echo $item['title']; ?></span> <br>
                            <span id="t_description"><?php echo $item['description']; ?></span>
                        </td>
                        <td>
                            <a href="<?php echo site_url('panel/biograph/education/'.$k); ?>" class="waves-effect">
                                <i class="material-icons">edit</i>
                            </a>
                            <a href="<?php echo site_url('panel/biograph/delete/education/'.$k); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this row ?');">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                <?php $no++; } ?>
            </tbody>
        </table>

        <?php if( isset($pagination) ) { ?>
            <!-- pagination -->
            <div class="row clearfix">
                <div class="col-xs-12 text-center">
                    <?php print_r($pagination); ?>
                </div>
            </div>
            <!-- /pagination -->
        <?php } ?>
    </div>
<?php } ?>