<?php
/*
 * Function name    :   get_latest_sorter
 * Usage            :   formatting password to be encrypted
 * Type             :   string
 * Value            :   
 */
if( !function_exists('get_latest_sorter') ) {
    function get_latest_sorter($term_id) {
        $CI = &get_instance();
        $CI->load->model('biograph/Biograph_model');
        
        $where['fields'] = 'sorter';
        $where['where'] = array(
            'biograph_term_id' => $term_id
        );
        $where['order_by'] = array('sorter' => 'DESC');
        $where['limit'] = 1;
        $sorter = $CI->Biograph_model->getBiograph('biograph', $where);
        if( empty($sorter) ) {
            $sorter = 1;
        } else {
            $sorter = array_shift($sorter);
            $sorter = $sorter['sorter']+1;
        }
        
        return $sorter;
    }
}

// formatting Profile Data
if( !function_exists('_profile') ) {
    function _profile($data, $action) {
        $biograph = array();
        
        switch($action) {
            case 'in':
                foreach($data as $field => $value) {
                    switch($field) {
                        case 'name':
                        case 'place_of_birth':
                        case 'spouse':
                            $value = ucwords($value);
                            break;
                    }
                    $biograph[$field] = $value;
                }
                break;
            case 'out':
                foreach($data as $k => $row) {
                    $biograph[$row['biograph_metakey']] = $row['biograph_value'];
                }
                break;
        }
        
        return $biograph;
    }
}

// formatting Contact Data
if( !function_exists('_contact') ) {
    function _contact($data, $action) {
        $biograph = array();
        
        switch($action) {
            case 'in':
                foreach($data as $field => $value) {
                    switch($field) {
                        case 'email':
                            $value = strtolower($value);
                            break;
                    }
                    $biograph[$field] = $value;
                }
                break;
            case 'out':
                foreach($data as $k => $row) {
                    $biograph[$row['biograph_metakey']] = $row['biograph_value'];
                }
                break;
        }
        
        return $biograph;
    }
}

// formatting Education Data
if( !function_exists('_education') ) {
    function _education($data, $action) {
        $biograph = array();
        
        switch($action) {
            case 'in':
                foreach($data as $field => $value) {
                    switch($field) {
                        case 'title':
                            $value = ucwords($value);
                            break;
                        case 'description':
                            $value = ucfirst($value);
                            break;
                    }
                    $biograph[$field] = $value;
                }
                break;
            case 'out':
                foreach($data as $k => $row) {
                    $biograph[$row['sorter']][$row['biograph_metakey']] = $row['biograph_value'];
                }
                break;
        }
        
        return $biograph;
    }
}

// formatting Career Data
if( !function_exists('_career') ) {
    function _career($data, $action) {
        $biograph = array();

        switch($action) {
            case 'in':
                foreach($data as $field => $value) {
                    switch($field) {
                        case 'title':
                            $value = ucwords($value);
                            break;
                        case 'description':
                            $value = ucfirst($value);
                            break;
                    }
                    $biograph[$field] = $value;
                }
                break;
            case 'out':
                foreach($data as $k => $row) {
                    $biograph[$row['sorter']][$row['biograph_metakey']] = $row['biograph_value'];
                }
                break;
        }
        
        return $biograph;
    }
}
