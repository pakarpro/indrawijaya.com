<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biograph extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/login';
    private $admin_data = array();
    private $profile_validation_config = array(
        array(
            'field'   => 'name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z\s\.\,\']+$/]'
        ),
        array(
            'field'   => 'place_of_birth',
            'label'   => 'Place of Birth',
            'rules'   => 'trim|regex_match[/^[A-Za-z\s\.\']+$/]'
        ),
        array(
            'field'   => 'date_of_birth',
            'label'   => 'Date of Birth',
            'rules'   => 'trim|callback_checkDateFormat'
        ),
        array(
            'field'   => 'spouse',
            'label'   => 'Spouse',
            'rules'   => 'trim|regex_match[/^[A-Za-z\s\.\,\']+$/]'
        ),
        array(
            'field'   => 'children',
            'label'   => 'Children',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'biographical_info',
            'label'   => 'Biographical Info',
            'rules'   => 'trim'
        )
    );
    private $contact_validation_config = array(
        array(
            'field'   => 'address',
            'label'   => 'Address',
            'rules'   => 'trim'
        ),
        array(
            'field'   => 'province',
            'label'   => 'Province',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'city',
            'label'   => 'City',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'district',
            'label'   => 'District',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'email',
            'label'   => 'Email',
            'rules'   => 'trim|valid_email'
        ),
        array(
            'field'   => 'phone',
            'label'   => 'Phone',
            'rules'   => 'trim|numeric|min_length[7]'
        )
    );
    private $education_validation_config = array(
        array(
            'field'   => 'year_from',
            'label'   => 'Year',
            'rules'   => 'trim|required|numeric|exact_length[4]'
        ),
        array(
            'field'   => 'year_to',
            'label'   => 'Year',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'title',
            'label'   => 'School/University/Course',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'description',
            'label'   => 'Description',
            'rules'   => 'trim'
        ),
    );
    private $career_validation_config = array(
        array(
            'field'   => 'year_from',
            'label'   => 'Year',
            'rules'   => 'trim|required|numeric|exact_length[4]'
        ),
        array(
            'field'   => 'year_to',
            'label'   => 'Year',
            'rules'   => 'trim|numeric'
        ),
        array(
            'field'   => 'title',
            'label'   => 'Job/Title/Experience',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'description',
            'label'   => 'Description',
            'rules'   => 'trim'
        ),
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'min_length' => '%s at least %d characters',
        'regex_match' => '%s does not follow approriate format',
        'exact_length' => '%s must be 4 characters, consist of number',
        'greater_than' => '%s must not greater than %d',
        'valid_email' => '%s is invalid',
        'numeric' => 'Use number only',
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('biograph');
        $this->load->model('Biograph_model');

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
   
    // getting biograph info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
    
#------------------------------------------------------------------------------------------------------------------#|>
#   PANEL SECTION
#------------------------------------------------------------------------------------------------------------------#|>
   
    public function index() {
        $this->lists();
    }
    
    // update data [profile, contact, education, career]
    public function edit($type='profile', $sorter='') {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        // function
        $func_name = '_'.strtolower($type);
        
        // type of biograph
        $data['type'] = $type;

        // get biography terms for menu
        $data['terms'] = $this->Biograph_model->getBiograph('term');

        // get biograph term id
        $biograph_term = $this->Biograph_model->getBiographByGid($type);
        $term_id = $biograph_term['biograph_term_id'];
        
        // get biograph based on term id
        $where['where'] = array('biograph_term_id' => $term_id);
        if( $type == 'education' || $type == 'career' ) {
            $where['order_by'] = array('sorter' => 'DESC');
        }
        $biograph = $this->Biograph_model->getBiograph('biograph', $where);
        $biograph = $func_name($biograph, 'out');
        
        // for education and career when sorter is not empty
        if( ($type == 'education' || $type == 'career') && !empty($sorter) ) {
            if( !empty($sorter) && $sorter != 0 ) {
                $where['where']['sorter'] = $sorter;
            }
            $detail = $this->Biograph_model->getBiograph('biograph', $where);
            $detail = $func_name($detail, 'out');
            foreach($detail as $k => $item) {
                $item['sorter'] = $k;
                $detail[$k] = $item;
            }
            $detail = array_shift($detail);
            $bio['detail'] = $detail;
        }

        // processing input data
        if( !empty($_POST) ) {
            $post_data = $this->input->post();

            if( $type == 'education' || $type == 'career' ) {
                $detail = $func_name($post_data, 'in');
            } else {
                $biograph = $func_name($post_data, 'in');
            }

            $config_name = $type.'_validation_config';
            $validation_config = $this->$config_name;

            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                // for education and career
                if( $type == 'education' || $type == 'career' ) {
                    $error = 0;
                    // if year from is greater than year now
                    if( $detail['year_from'] > date('Y') ) {
                        $detail['error_year_from'] = error_msg_generator('Year from is greater than '.date('Y'));
                        $error++;
                    }
                    // if year to is less than year from
                    if( $detail['year_from'] > $detail['year_to'] ) {
                        $detail['error_year_to'] = error_msg_generator('Year to should be greater than year from');
                        $error++;
                    }

                    if( $error == 0 ) {
                        if( isset($detail['sorter']) ) {      // update
                            $sorter = $detail['sorter'];
                            foreach($detail as $field => $value) {
                                // where update param
                                $update_param = array('biograph_value' => $value);
                                $where_update_param = array(
                                    'biograph_term_id' => $term_id,
                                    'biograph_metakey' => $field,
                                    'sorter' => $sorter
                                );
                                $update = $this->Biograph_model->updateBiograph('biograph', $update_param, $where_update_param);
                            }

                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Data has been updated.'.
                                          '</div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to biograph > profile page
                                redirect('panel/biograph/'.$type);
                            }
                        } else {                                // insert new
                            // get latest sorter
                            $sorter = get_latest_sorter($term_id);
                            foreach($detail as $field => $value) {
                                // save param
                                $save_param = array(
                                    'biograph_term_id' => $term_id,
                                    'biograph_metakey' => $field,
                                    'biograph_value' => $value,
                                    'sorter' => $sorter
                                );
                                $save = $this->Biograph_model->setNewBiograph('biograph', $save_param);
                            }

                            if( isset($save['id']) ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            New data has been added.'.
                                          '</div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to biograph > profile page
                                redirect('panel/biograph/'.$type);
                            }
                        }
                    } else {
                        $bio['detail'] = $detail;
                    }
                } else {
                    foreach($biograph as $field => $value) {
                        // where update param
                        $update_param = array('biograph_value' => $value);
                        $where_update_param = array(
                            'biograph_term_id' => $term_id,
                            'biograph_metakey' => $field,
                        );
                        $update = $this->Biograph_model->updateBiograph('biograph', $update_param, $where_update_param);
                    }

                    if( $update == 'SUCCESSFUL' ) {
                        // set notification
                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    '.ucfirst($type).' has been updated.'.
                                  '</div>';
                        $this->session->set_flashdata('notif', $notif);

                        // redirect to biograph > profile page
                        redirect('panel/biograph/'.$type);
                    }
                }
            } # end of running validation
        } # end post

        if( $type == 'contact' ) {
            $this->load->model('area/Area_model');
            $order_by = array('area_name' => 'ASC');

            // get province
            $where_province['order_by'] = $order_by;
            $province = $this->Area_model->getArea('province');
            $bio['province'] = $province;

            // get city
            $where_city['where'] = array(
                'parent' => $biograph['province']
            );
            $where_city['order_by'] = $order_by;
            $city = $this->Area_model->getArea('city', $where_city);
            $bio['city'] = $city;

            // get district
            $where_district['where'] = array(
                'parent' => $biograph['city']
            );
            $where_district['order_by'] = $order_by;
            $district = $this->Area_model->getArea('district', $where_district);
            $bio['district'] = $district;
        }

        // view on another page, set as variable to be displayed in main page
        $bio['biograph'] = $biograph;
        $data['biograph'] = $this->load->view('panel/'.$type, $bio, TRUE);

        // main page
        $this->load->view('panel/biography', $data);
    }
    
    // delete action for education and career
    public function delete($type='education', $sorter) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        $msg = 'An error occured while deleting article. Please try again later.';
        $alertClass = 'alert-danger';
        switch($type) {
            case 'education':
            case 'career':
                // provide error message when given ID is empty or 0
                if( !empty($sorter) && is_numeric($sorter) && $sorter != 0 ) {
                    // get biograph term id
                    $biograph_term = $this->Biograph_model->getBiographByGid($type);
                    $term_id = $biograph_term['biograph_term_id'];

                    // check if given ID is existing in database
                    $where = array('sorter' => $sorter);
                    $biograph = $this->Biograph_model->getBiograph('biograph', array('where' => $where));
                    // if not empty delete from database
                    if( !empty($biograph) ) {
                        $delete_param = $where;
                        $delete_param['biograph_term_id'] = $term_id;
                        $delete = $this->Biograph_model->deleteBiograph('biograph', $delete_param);
                        if( $delete == 'SUCCESSFUL') {
                            // provide success message
                            $msg = 'Data with ID <b>'.$sorter.'</b> has been deleted.';
                            $alertClass = 'alert-success';
                        }
                    }
                }
                break;
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
            
        // redirect to list page
        redirect('panel/biograph/'.$type);
    }
    
    // check date format
    public function checkDateFormat($date) {
        $match_date = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date);

        if( $match_date ) {
            return true;
        } else {
            return false;
        }
    }
}