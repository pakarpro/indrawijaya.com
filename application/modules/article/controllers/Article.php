<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/login';
    private $img_path;
    private $temp_path;
    private $post_date;
    private $post_modified;
    private $category;
    private $tag;
    private $rel_tag = array();
    private $rel_tag_id = array();
    private $rel_category = array();
    private $uploaded_data = array();
    private $media = array();
    private $page = 1;
    private $file = 'list';
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'post_title',
            'label'   => 'Title',
            'rules'   => 'trim|required'
        ),
//        array(
//            'field'   => 'post_gid',
//            'label'   => 'Keyword',
//            'rules'   => 'trim|regex_match[/^[a-z0-9\s\-]+$/]'
//        ),
        array(
            'field'   => 'post_content',
            'label'   => 'Content',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'post_date',
            'label'   => 'Date scheduling',
            'rules'   => 'trim'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'is_unique' => '%s is already taken',
        'edit_unique' => '%s is already taken',
        'checkDateFormat' => 'Invalid date and time format',
//        'regex_match' => '%s does not follow approriate format'
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();

        $this->load->helper('article');
        $this->load->model('Article_model');
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'post/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/';
        
        // for input
        $this->post_date = DATETIME;
        $this->post_modified = DEFAULT_DATETIME;
        $this->category = array(1);
        $this->tag = array();
         
        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // check session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // getting user info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    // count article activity, both like and view
    private function _count_article_activity($activity='view', $post_id) {
        $param['where'] = array(       // set param
            'activity_post_id' => $post_id,
            'activity_type' => $activity,
            'activity_ip' => IP_ADDR
        );
        $param['like'] = array(
            'activity_date' => DATE
        );
//        print_r($param); exit;
        $count = $this->Article_model->countArticle('activity', $param);
        
        return $count;
    }
    
    // create directory recursively
    private function _create_directory($date, $id) {
        $dir = $date.'-'.$id;
        $dir = explode('-', $dir);
        foreach($dir as $key) {
            $this->img_path .= $key.'/';
            if( !file_exists($this->img_path) ) {
                mkdir($this->img_path);                               // create directory if not exist
                chmod($this->img_path, 0777);                         // set permission of directory
            }
        }
        
        return $this->img_path;
    }
    
    // get prev and next article
    private function _set_article_pager($type='next', $post_id) {
        $order_value = 'ASC';
        $where_field = 'post_id >';
        if( $type == 'prev' ) {
            $where_field = 'post_id <';
            $order_value = 'DESC';
        }
        
        $param['fields'] = array(
            'post_gid',
            'post_title',
        );
        $param['where'] = array(
            'post_status' => 'published',
            "(NOW() >= post_date)" => NULL,
            $where_field => $post_id
        );
        $param['order_by'] = array(
            'post_id' => $order_value
        );
        $param['limit'] = 1;
        $article = $this->Article_model->getArticle('post', $param);
        if( !empty($article) ) {
            $article = array_shift($article);
        }
        
        return $article;
    }
    
    // counter for like and view
    public function set_post_activity($activity='view', $post_id='') {
        $alert = '';
        $post = $this->input->post('post_id');
        if( !empty($post) && $post != 0 ) {
            $post_id = $post;
            $alert == 'success';
        }

        $count = $this->_count_article_activity($activity, $post_id);
        if( $count == 0 ) {                                                // if client has not seen this article today
            // soon will using API                                              // save to article_by_view_table
            $view_counter_param = array(
                'activity_post_id' => $post_id,
                'activity_date' => DATETIME,
                'activity_type' => $activity,
                'activity_ip' => IP_ADDR
            );
            $save_activity = $this->Article_model->setNewArticle('activity', $view_counter_param);   // save to article_view
            if( isset($save_activity['id']) ) {
                // get total views or likes
                $count_activity_param['where'] = array(
                    'activity_post_id' => $post_id,
                    'activity_type' => $activity
                );
                $count_activity = $this->Article_model->countArticle('activity', $count_activity_param);

                // update like or view count
                $total_param = array(
                    $activity.'_count' => $count_activity
                );
                $update_count = $this->Article_model->updateArticle('post', $total_param, $post_id);    // update total views in table article
                if( $update_count == 'SUCCESSFUL' ) {
                    echo $alert;
                }
            }
        }
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for main page
     * -------------------------------------------------------------------------
     */
    // for detail article
    public function posts($param='') {
        $this->load->helper('term/term');
        $article = array();
        if( empty($param) ) {
            $param = 1;
        }
        
        if( is_numeric($param) ) {      // show list
            $this->page = $param;

            $article_param['where'] = array(
                'post_status' => 'published',
                '(NOW() >= post_date)' => NULL
            );
            $count = $this->Article_model->countArticle('post', $article_param);
            if( $count > 0 ) {
                // set pagination
                $limit_per_page = 5;
                $start_row = ($this->page-1) * $limit_per_page;
                if( $count > $limit_per_page ) {
                    // additional where param
                    $article_param['limit'] = array($limit_per_page, $start_row);
                    $paging_config = array(
                        'base_url'      =>  site_url('posts'),
                        'total_rows'    =>  $count,
                        'per_page'      =>  $limit_per_page
                    );
                    $config = paging_config($paging_config);
                    $this->load->library('pagination');
                    $this->pagination->initialize($config);
                    $data['pagination'] = $this->pagination->create_links();
                }

                // get article
                $this->load->model('user/User_model');
                $this->load->model('media/Media_model');
                $article_param['fields'] = array(
                    'post_id',
                    'post_gid',
                    'post_author',
                    'post_date',
                    'post_title',
                    'post_content',
                    'like_count',
                    'view_count',
                );
                $article_param['order_by'] = array(
                    'post_date' => 'DESC'
                );
                $article = $this->Article_model->getArticle('post', $article_param);
                foreach($article as $k => $item) {
                    $post_id = $item['post_id'];

                    // get author name
                    $post_author = $this->User_model->getUserById('user', $item['post_author']);
                    $item['author_name'] = $post_author['name'];

                    // get related tag
                    $tags = get_term_relationship('tag', $post_id);
                    if( !empty($tags) ) {
                        foreach($tags as $key => $val) {
                            $rel_tags[] = '<a href="'.site_url('tag/'.$val['term_gid']).'">'.$val['term_name'].'</a>';
                        }
                        $item['rel_tags'] = implode(', ', $rel_tags);
                    }

                    // get media
                    $relationship_param['where'] = array(
                        'object_id' => $post_id
                    );
                    $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                    if( !empty($relationship) ){
                        $relationship = array_shift($relationship);

                        // get the media by id
                        $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                        $item['media'] = $media;
                    }

                    $article[$k] = $item;
                }
            }
        } else {                        // show detail
            $gid = $param;
            $this->file = 'detail';
            $article = $this->Article_model->getArticleByGid($gid);
            if( !empty($article) ) {
                $post_id = $article['post_id'];

                // get author name
                $this->load->model('user/User_model');
                $post_author = $this->User_model->getUserById('user', $article['post_author']);
                $article['author_name'] = $post_author['name'];
                
                // get related tag
                $tags = get_term_relationship('tag', $post_id);
                if( !empty($tags) ) {
                    foreach($tags as $k => $item) {
                        $article['rel_tags'][$k] = '<a href="'.site_url('tag/'.$item['term_gid']).'">'.$item['term_name'].'</a>';
                    }
                }

                // get related category
                $categories = get_term_relationship('category', $post_id);
                if( !empty($categories) ) {
                    foreach($categories as $k => $item) {
                        $article['rel_categories'][$k] = '<a href="'.site_url('category/'.$item['term_gid']).'">'.$item['term_name'].'</a>';
                    }
                }

                // get next and previous article
                $prev_article = $this->_set_article_pager('prev', $post_id);
                if( !empty($prev_article) ) {
                    $data['pager']['previous'] = $prev_article;
                }
                $next_article = $this->_set_article_pager('next', $post_id);
                if( !empty($next_article) ) {
                    $data['pager']['next'] = $next_article;
                }
                
                // get media
                $this->load->model('media/Media_model');
                $relationship_param['where'] = array(
                    'object_id' => $post_id
                );
                $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                if( !empty($relationship) ){
                    $relationship = array_shift($relationship);
                    
                    // get the media by id
                    $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                    $article['media'] = $media;
                }
                
                // count view for checking if user has been visited this article or no
                $this->set_post_activity('view', $post_id);
            }
        }
        
        $data['article'] = $article;

        // get all category
        $data['categories'] = get_term_list('category');
        
        // get popular article
        $data['popular_post'] = get_article('popular', 3);
        
        $this->load->view('main/'.$this->file.'-post', $data);
    }
    
    public function terms($type='', $gid='', $page='1') {
        $article = array();
        
        switch($type) {
            case 'category':
            case 'tag':
                if( empty($gid) ) {
                    $article_param['where'] = array(
                        'post_status' => 'published',
                        '(NOW() >= post_date)' => NULL
                    );
                    $count = $this->Article_model->countArticle('post', $article_param);
                    if( $count > 0 ) {
                        // set pagination
                        $limit_per_page = 5;
                        $start_row = ($page-1) * $limit_per_page;
                        if( $count > $limit_per_page ) {
                            // additional where param
                            $article_param['limit'] = array($limit_per_page, $start_row);
                            $paging_config = array(
                                'base_url'      =>  site_url('terms'),
                                'total_rows'    =>  $count,
                                'per_page'      =>  $limit_per_page
                            );
                            $config = paging_config($paging_config);
                            $this->load->library('pagination');
                            $this->pagination->initialize($config);
                            $data['pagination'] = $this->pagination->create_links();
                        }

                        // get article
                        $this->load->helper('term/term');
                        $this->load->model('user/User_model');
                        $this->load->model('media/Media_model');
                        $article_param['fields'] = array(
                            'post_id',
                            'post_gid',
                            'post_author',
                            'post_date',
                            'post_title',
                            'post_content',
                            'like_count',
                            'view_count',
                        );
                        $article_param['order_by'] = array(
                            'post_date' => 'DESC'
                        );
                        $article = $this->Article_model->getArticle('post', $article_param);
                        foreach($article as $k => $item) {
                            $post_id = $item['post_id'];

                            // get author name
                            $post_author = $this->User_model->getUserById('user', $item['post_author']);
                            $item['author_name'] = $post_author['name'];

                            // get related tag
                            $tags = get_term_relationship('tag', $post_id);
                            if( !empty($tags) ) {
                                foreach($tags as $key => $val) {
                                    $rel_tags[] = '<a href="'.site_url('tag/'.$val['term_gid']).'">'.$val['term_name'].'</a>';
                                }
                                $item['rel_tags'] = implode(', ', $rel_tags);
                            }

                            // get media
                            $relationship_param['where'] = array(
                                'object_id' => $post_id
                            );
                            $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                            if( !empty($relationship) ){
                                $relationship = array_shift($relationship);

                                // get the media by id
                                $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                                $item['media'] = $media;
                            }

                            $article[$k] = $item;
                        }
                    }
                } else {
                    $this->load->model('term/Term_model');
                    
                    if( !is_numeric($gid) ) {
                        $taxonomy_param['fields'] = TERM_TAXONOMY.'.term_taxonomy_id';
                        $taxonomy_param['where'] = array(
                            TERMS.'.term_gid' => $gid,
                            TERM_TAXONOMY.'.taxonomy' => $type,
                        );
                        $taxonomy_param['join'] = array(
                            TERMS => TERMS.'.term_id = '.TERM_TAXONOMY.'.term_id'
                        );
                        $taxonomy = $this->Term_model->getTerm('taxonomy', $taxonomy_param);
                        if( !empty($taxonomy) ) {
                            $taxonomy = array_shift($taxonomy);
                            
                            $taxonomy_id = $taxonomy['term_taxonomy_id'];

                            // get post id
                            $article_param['where'] = array(
                                'post_status' => 'published',
                                '(NOW() >= post_date)' => NULL,
                                'term_taxonomy_id' => $taxonomy_id
                            );
                            $article_param['join'] = array(
                                TERM_RELATIONSHIPS => TERM_RELATIONSHIPS.'.object_id = '.POSTS.'.post_id'
                            );
                            $count_article = $this->Article_model->countArticle('post', $article_param);
                            if( $count_article > 0 ) {
                                // set pagination
                                $limit_per_page = 5;
                                $start_row = ($page-1) * $limit_per_page;
                                if( $count_article > $limit_per_page ) {
                                    // additional where param
                                    $article_param['limit'] = array($limit_per_page, $start_row);
                                    $paging_config = array(
                                        'base_url'      =>  site_url('terms/'.$type.'/'.$gid),
                                        'total_rows'    =>  $count_article,
                                        'per_page'      =>  $limit_per_page
                                    );
                                    $config = paging_config($paging_config);
                                    $this->load->library('pagination');
                                    $this->pagination->initialize($config);
                                    $data['pagination'] = $this->pagination->create_links();
                                }
                            }

                            // get post
                            $this->load->helper('term/term');
                            $this->load->model('user/User_model');
                            $this->load->model('media/Media_model');
                            $article_param['fields'] = array(
                                'post_id',
                                'post_gid',
                                'post_author',
                                'post_date',
                                'post_title',
                                'post_content',
                                'like_count',
                                'view_count',
                            );
                            $article_param['order_by'] = array(
                                'post_date' => 'DESC'
                            );
                            $article = $this->Article_model->getArticle('post', $article_param);
                            foreach($article as $k => $item) {
                                $article_id = $item['post_id'];

                                // get author name
                                $article_author = $this->User_model->getUserById('user', $item['post_author']);
                                $item['author_name'] = $article_author['name'];

                                // get related tag
                                $tags = get_term_relationship('tag', $article_id);
                                if( !empty($tags) ) {
                                    foreach($tags as $key => $val) {
                                        $rel_tags[] = '<a href="'.site_url('tag/'.$val['term_gid']).'">'.$val['term_name'].'</a>';
                                    }
                                    $item['rel_tags'] = implode(', ', $rel_tags);
                                }

                                // get media
                                $relationship_param['where'] = array(
                                    'object_id' => $article_id
                                );
                                $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                                if( !empty($relationship) ){
                                    $relationship = array_shift($relationship);

                                    // get the media by id
                                    $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                                    $item['media'] = $media;
                                }

                                $article[$k] = $item;
                            }
                        }
                    }
                }
                break;
            default:
                $article = array();
        }

        $data['article'] = $article;

        // get all category
        $data['categories'] = get_term_list('category');

        // get popular article
        $data['popular_post'] = get_article('popular', 3);

        $this->load->view('main/list-post', $data);
    }
    
    public function search($page=1) {
        $this->load->helper('term/term');
        $where = array(
            'post_status' => 'published',
            "(NOW() >= post_date)" => NULL
        );
        
        $get_data = $this->input->get();
//        print_r($get_data); exit;
        foreach($get_data as $k => $val) {
            switch($k) {
                case 'q':
                    // for URI string purpose
                    $q = str_replace(' ', '+', $val);
                    $q = '?q='.$q;
                    
                    // search parameter
                    if(str_word_count($val) > 1) {
                        unset($or_like);
                        $val = explode(' ', $val);
                        foreach($val as $index) {
                            $like[] = "(post_gid LIKE '%".$index."%' OR post_title LIKE '%".$index."%' OR post_content LIKE '%".$index."%')";
                        }
                        $like = implode(' OR ', $like);
                        $where[$like] = NULL;
                    } else {
                        $where["(post_gid LIKE '%".$val."%' OR post_title LIKE '%".$val."%' OR post_content LIKE '%".$val."%')"] = NULL;
                    }
                    
                    // as variable in view file
                    $data['query'] = $val;
                    break;
                case 'page':
                    $this->page = $val;
                    break;
            }
        }

        $article_param['where'] = $where;
//        print_r($article_param); exit;
        $count_article = $this->Article_model->countArticle('post', $article_param);
        if( $count_article > 0 ) {
            // set pagination
            $limit_per_page = 5;
            $start_row = ($this->page-1) * $limit_per_page;
            if( $count_article > $limit_per_page ) {
                // additional where param
                $article_param['limit'] = array($limit_per_page, $start_row);
                $paging_config = array(
                    'base_url' =>  site_url('article/search'.$q),
                    'total_rows' =>  $count_article,
                    'per_page' =>  $limit_per_page,
                    'query_string_segment' => 'page',
                    'page_query_string' => TRUE,
                );
                $config = paging_config($paging_config);
                $this->load->library('pagination');
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }

            // get article
            $this->load->model('user/User_model');
            $article_param['fields'] = array(
                'post_id',
                'post_gid',
                'post_author',
                'post_date',
                'post_title',
                'post_content',
                'like_count',
                'view_count',
            );
            $article_param['order_by'] = array(
                'post_date' => 'DESC'
            );
            $article = $this->Article_model->getArticle('post', $article_param);
            foreach($article as $k => $item) {
                $post_id = $item['post_id'];

                // get author name
                $post_author = $this->User_model->getUserById('user', $item['post_author']);
                $item['author_name'] = $post_author['name'];

                // get related tag
                $tags = get_term_relationship('tag', $post_id);
                if( !empty($tags) ) {
                    foreach($tags as $key => $val) {
                        $rel_tags[] = '<a href="'.site_url('tag/'.$val['term_gid']).'">'.$val['term_name'].'</a>';
                    }
                    $item['rel_tags'] = implode(', ', $rel_tags);
                }

                $article[$k] = $item;
            }
            
            $data['article'] = $article;
        }
        
        // get all category
        $data['categories'] = get_term_list('category');
        
        // get popular article
        $data['popular_post'] = get_article('popular', 3);
        
        $this->load->view('main/search-result', $data);
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for panel page
     * -------------------------------------------------------------------------
     */
    
    // list all article data
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);

        $count_param = array();
        if( $data['admin_data']['role'] != 1 ) {
            $count_param = array('post_author' => $id_user);
        }
        $where_post['where'] = $count_param;
        $count_data = $this->Article_model->countArticle('post', $where_post);
        if( $count_data > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;
                
            if( $count_data > $limit_per_page ) {
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/article/list/'),
                    'total_rows'    =>  $count_data,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            // where post param
            $where_post['fields'] = array(
                'post_id',
                'post_author',
                'post_date',
                'post_modified',
                'post_title',
                'post_status',
            );
            $where_post['limit'] = array($limit_per_page, $start_row);
            $posts = $this->Article_model->getArticle('post', $where_post);
            $this->load->helper('term/term');
            $this->load->model('user/User_model');
            foreach($posts as $key => $row) {
                $post_id = $row['post_id'];
                
                // get author name
                $author = $this->User_model->getUserById('user', $row['post_author']);
                $row['post_author'] = $author['name'];
                
                // get category
                $categories = get_term_relationship('category', $post_id);
                if( !empty($categories) ) {
                    foreach($categories as $k => $val) {
                        $row['categories'][] = $val['term_name'];
                    }
                }
                
                // get tag
                $tags = get_term_relationship('tag', $post_id);
                if( !empty($tags) ) {
                    foreach($tags as $k => $val) {
                        $row['tags'][] = $val['term_name'];
                    }
                }
                
                $posts[$key] = $row;
            }

            $data['data'] = $posts;
        }
        
        $this->load->view('panel/list-post', $data);
    }
    
    // save new article data
    public function create() {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        if(!empty($_POST)) {
            $post_data = $this->input->post();

            // gid
            $post_gid = generate_gid($post_data['post_title']);
            // scheduling
            if( isset($post_data['scheduling']) ) {
                $this->post_date = $post_data['post_date'];
                if( $post_data['post_status'] == 'draft' ) {
                    $this->post_modified = $this->post_date;
                }
            }
            // category
            if( isset($post_data['category']) ) {
                $this->category = $post_data['category'];
            }
            // tag
            if( isset($post_data['tag']) ) {
                $this->tag = $post_data['tag'];
            }

            // validation
#---------------------------------------------------------------------------------------------------------------------#
            if( isset($post_data['scheduling']) ) {
                foreach($this->validation_config as $key => $val) {
                    if( $val['field'] == 'post_date' ) {
                        $val['rules'] = $val['rules'].'|required|callback_checkDateFormat';
                    }
                    $this->validation_config[$key] = $val;
                }

                $this->validation_config;
            }

            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }
#---------------------------------------------------------------------------------------------------------------------#

            if ($this->form_validation->run() == TRUE ) {
                // save param
                $save_param = array(
                    'post_gid' => $post_gid,
                    'post_author' => $id_user,
                    'post_date' => $this->post_date,
                    'post_title' => $post_data['post_title'],
                    'post_content' => $post_data['post_content'],
                    'post_status' => $post_data['post_status']
                );

                if( !empty($_FILES['image_name']['tmp_name']) ) {
                    $this->load->helper('uploads/uploads');

                    $source = $_FILES;
                    $this->uploaded_data = upload_image_to_temp('article', $source, $this->temp_path);
                }

                // if error occured
                if( !empty($this->uploaded_data) ) {
                    foreach($this->uploaded_data as $k => $value) {
                        switch($k) {
                            case 'uploaded_data':
                                $save = $this->Article_model->setNewArticle('post', $save_param);
                                if( isset($save['id']) ) {
                                    $post_id = $save['id'];

                                    // create directory
                                    $img_path = $this->_create_directory(DATE, $post_id);

                                    // crop image, store in directory, update image_name in database
                                    $image_name = cropping_image('article', $value, $img_path, $post_data['post_title']);
                                    
                                    // save media into database
                                    $this->load->model('media/Media_model');
                                    $save_media_param = array(
                                        'media_type' => 'image',
                                        'media_date' => DATETIME,
                                        'media_title' => $image_name['image_name']
                                    );
                                    $save_media = $this->Media_model->setNewMedia('media', $save_media_param);
                                    if( isset($save_media['id']) ) {
                                        $media_id = $save_media['id'];
                                        
                                        // set relationship between media and post
                                        $media_rel_param = array(
                                            'object_id' => $post_id,
                                            'media_id' => $media_id
                                        );
                                        $this->Media_model->setNewMedia('relationship', $media_rel_param);
                                    }

                                    $this->load->model('term/Term_model');
                                    // set category
                                    foreach($this->category as $item) {
                                        $category_param = array(
                                            'object_id' => $post_id,
                                            'term_taxonomy_id' => $item,
                                        );
                                        $this->Term_model->setNewTerm('relationship', $category_param);
                                    }

                                    // set tag
                                    if( !empty($this->tag) ) {
                                        $tags = explode(',', $this->tag);
                                        foreach($tags as $item) {
                                            $term_gid = generate_gid($item);

                                            // check existing tag
                                            $check_param = array(
                                                TERM_TAXONOMY.'.taxonomy' => 'tag',
                                                TERMS.'.term_gid' => $term_gid
                                            );
                                            $where_gid['join'] = array(
                                                TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
                                            );
                                            $where_gid['where'] = $check_param;
                                            $count = $this->Term_model->countTerm('term', $where_gid);
                                            if( $count == 0 ) {
                                                // save param
                                                $save_tag_param = array(
                                                    'term_gid' => $term_gid,
                                                    'term_name' => $item
                                                );
                                                $save_tag = $this->Term_model->setNewTerm('term', $save_tag_param);
                                                if( isset($save_tag['id']) ) {
                                                    $term_id = $save_tag['id'];

                                                    // save term taxonomy
                                                    $save_taxonomy_param = array(
                                                        'term_id' => $term_id,
                                                        'taxonomy' => 'tag'
                                                    );
                                                    $save_taxonomy = $this->Term_model->setNewTerm('taxonomy', $save_taxonomy_param);
                                                    if( isset($save_taxonomy['id']) ) {
                                                        $term_taxonomy_id = $save_taxonomy['id'];
                                                    }
                                                }
                                            } else {
                                                $term_param['fields'] = array(
                                                    TERM_TAXONOMY.'.term_taxonomy_id',
                                                );
                                                $term_param['where'] = $check_param;
                                                $term_param['join'] = $where_gid['join'];
                                                $term = $this->Term_model->getTerm('term', $term_param);
                                                $term = array_shift($term);
                                                $term_taxonomy_id = $term['term_taxonomy_id'];
                                            }

                                            // save to relationship
                                            $rel_param = array(
                                                'object_id' => $post_id,
                                                'term_taxonomy_id' => $term_taxonomy_id
                                            );
                                            $save_relationship = $this->Term_model->setNewTerm('relationship', $rel_param);
                                            if( isset($save_relationship['id']) ) {
                                                // count every tag
                                                $count_taxonomy_param['where'] = array(
                                                    'term_taxonomy_id' => $term_taxonomy_id
                                                );
                                                $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);

                                                // update term count
                                                $update_count_taxonomy = array(
                                                    'count' => $count_taxonomy
                                                );
                                                $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $term_taxonomy_id);
                                            }
                                        }
                                    }

                                    // set notification
                                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                New article has been created.'.
                                              '</div>';
                                    $this->session->set_flashdata('notif', $notif);

                                    // redirect to list general info page
                                    redirect('panel/article/edit/'.$post_id);
                                }
                                break;
                            case 'error_image':
                                $data[$k] = array_shift($value);
                                break;
                        }
                    }
                } else {
                    $save = $this->Article_model->setNewArticle('post', $save_param);

                    if( isset($save['id']) ) {
                        $post_id = $save['id'];
                        
                        $this->load->model('term/Term_model');
                        // set category
                        foreach($this->category as $item) {
                            $category_param = array(
                                'object_id' => $post_id,
                                'term_taxonomy_id' => $item,
                            );
                            $this->Term_model->setNewTerm('relationship', $category_param);
                        }
                        
                        // set tag
                        if( !empty($this->tag) ) {
                            $tags = explode(',', $this->tag);
                            foreach($tags as $item) {
                                $term_gid = generate_gid($item);
                                
                                // check existing tag
                                $check_param = array(
                                    TERM_TAXONOMY.'.taxonomy' => 'tag',
                                    TERMS.'.term_gid' => $term_gid
                                );
                                $where_gid['join'] = array(
                                    TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
                                );
                                $where_gid['where'] = $check_param;
                                $count = $this->Term_model->countTerm('term', $where_gid);
                                if( $count == 0 ) {
                                    // save param
                                    $save_tag_param = array(
                                        'term_gid' => $term_gid,
                                        'term_name' => $item
                                    );
                                    $save_tag = $this->Term_model->setNewTerm('term', $save_tag_param);
                                    if( isset($save_tag['id']) ) {
                                        $term_id = $save_tag['id'];
                                        
                                        // save term taxonomy
                                        $save_taxonomy_param = array(
                                            'term_id' => $term_id,
                                            'taxonomy' => 'tag'
                                        );
                                        $save_taxonomy = $this->Term_model->setNewTerm('taxonomy', $save_taxonomy_param);
                                        if( isset($save_taxonomy['id']) ) {
                                            $term_taxonomy_id = $save_taxonomy['id'];
                                        }
                                    }
                                } else {
                                    $term_param['fields'] = array(
                                        TERM_TAXONOMY.'.term_taxonomy_id',
                                    );
                                    $term_param['where'] = $check_param;
                                    $term_param['join'] = $where_gid['join'];
                                    $term = $this->Term_model->getTerm('term', $term_param);
                                    $term = array_shift($term);
                                    $term_taxonomy_id = $term['term_taxonomy_id'];
                                }
                                
                                // save to relationship
                                $rel_param = array(
                                    'object_id' => $post_id,
                                    'term_taxonomy_id' => $term_taxonomy_id
                                );
                                $save_relationship = $this->Term_model->setNewTerm('relationship', $rel_param);
                                if( isset($save_relationship['id']) ) {
                                    // count every tag
                                    $count_taxonomy_param['where'] = array(
                                        'term_taxonomy_id' => $term_taxonomy_id
                                    );
                                    $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);
                                    
                                    // update term count
                                    $update_count_taxonomy = array(
                                        'count' => $count_taxonomy
                                    );
                                    $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $term_taxonomy_id);
                                }
                            }
                        }
                        
                        // set notification
                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    New article has been created.
                                  </div>';
                        $this->session->set_flashdata('notif', $notif);

                        // redirect to list general info page
                        redirect('panel/article/edit/'.$post_id);
                    }
                } # end of checking uploaded data
            } # end of validation
        } # end of post

        $this->load->helper('term/term');
        $data['categories'] = get_term_list('category');
        $data['tags'] = get_term_list('tag');
        $data['post_status'] = get_post_status();
        
        $this->load->view('panel/create-post', $data);
    }
    
    // update article data, operated by admin
    public function edit($id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        // check if article is belongs to the owner of user, except administrator
        $check_owner_param['where'] = array(
            'post_id' => $id,
            'post_author' => $id_user,
        );
        $data['post_owner'] = $this->Article_model->countArticle('post', $check_owner_param);
        
        // get article
        $article = $this->Article_model->getArticleById('post', $id);
        if( !empty($article) ) {
            $id = $article['post_id'];

            // post status
            $article['scheduling'] = 1;
            // get category
            $this->load->helper('term/term');
            $categories = get_term_relationship('category', $id);
            if( !empty($categories) ) {
                foreach($categories as $k => $val) {
                    $this->rel_category[] = $val['term_taxonomy_id'];
                }
            }
            // get tag
            $tags = get_term_relationship('tag', $id);
            if( !empty($tags) ) {
                foreach($tags as $k => $val) {
                    $this->rel_tag_id[] = $val['term_taxonomy_id'];
                    $this->rel_tag[] = $val['term_name'];
                }
            }
            $article['category'] = $this->rel_category;
            $article['tag'] = implode(',', $this->rel_tag);
            
            // get media
            $this->load->model('media/Media_model');
            $media_param['fields'] = array(
                MEDIAS.'.media_id',
                MEDIAS.'.media_date',
                MEDIAS.'.media_title'
            );
            $media_param['where'] = array(
                MEDIA_RELATIONSHIPS.'.object_id' => $id
            );
            $media_param['join'] = array(
                MEDIA_RELATIONSHIPS => MEDIA_RELATIONSHIPS.'.media_id = '.MEDIAS.'.media_id'
            );
            $media = $this->Media_model->getMedia('media', $media_param);
            if( !empty($media) ) {
                $media = array_shift($media);
                $this->media = $media;
            }
            $article['media'] = $this->media;
            if(!empty($_POST)) {
                $post_data = $this->input->post();

                // gid
                $gid = generate_gid($post_data['post_title']);
                // image name
                $image_name = $post_data['image_name'];
                // scheduling
                if( isset($post_data['scheduling']) ) {
                    $this->post_date = $post_data['post_date'];
                }
                // category
                if( isset($post_data['category']) ) {
                    $this->category = $post_data['category'];
                }
                // tag
                if( isset($post_data['tag']) ) {
                    $this->tag = $post_data['tag'];
                }

                $article = array_merge($article, $post_data);

                // validation
#---------------------------------------------------------------------------------------------------------------------#
                if( isset($post_data['scheduling']) ) {
                    foreach($this->validation_config as $key => $val) {
                        if( $val['field'] == 'post_date' ) {
                            $val['rules'] = $val['rules'].'|required';
                        }
                        $this->validation_config[$key] = $val;
                    }

                    $this->validation_config;
                }

                $this->form_validation->set_rules($this->validation_config);
                foreach($this->validation_msg as $key => $msg) {
                    $this->form_validation->set_message($key, $msg);
                }
#---------------------------------------------------------------------------------------------------------------------#

                if ($this->form_validation->run() == TRUE ) {
                    // update param
                    $update_param = array(
                        'post_gid' => $gid,
                        'post_date' => $this->post_date,
                        'post_title' => $post_data['post_title'],
                        'post_modified' => DATETIME,
                        'post_content' => $post_data['post_content'],
                        'post_status' => $post_data['post_status']
                    );

                    if( !empty($_FILES['image_name']['tmp_name']) ) {
                        $this->load->helper('uploads/uploads');

                        $source = $_FILES;
                        $this->uploaded_data = upload_image_to_temp('article', $source, $this->temp_path);
                    }

                    // if error occured
                    if( !empty($this->uploaded_data) ) {
                        foreach($this->uploaded_data as $k => $value) {
                            switch($k) {
                                case 'uploaded_data':
                                    $update = $this->Article_model->updateArticle('post', $update_param, $id);
                                    if( $update == 'SUCCESSFUL' ) {
                                        // create directory
                                        $date_directory = DATE;
                                        if( isset($this->media['media_date']) ) {
                                            $exp_date = explode(' ', $this->media['media_date']);
                                            $date_directory = $exp_date[0];
                                        }
//                                        $exp_date = explode(' ', $this->media['media_date']);
//                                        $date_directory = $exp_date[0];
                                        $img_path = $this->_create_directory($date_directory, $id);

                                        // unlink old photo
                                        if( !empty($image_name) ) {
                                            $glob = glob($img_path.'*'.$image_name);
                                            if( !empty($glob) ) {
                                                foreach($glob as $item) {
                                                    unlink($item);
                                                }
                                            }
                                        }

                                        // crop image, store in directory, update image_name in database
                                        $image_name = cropping_image('article', $value, $img_path, $post_data['post_title']);
                                        
                                        // check media relationship first
                                        $relationship_param['where'] = array(
                                            'object_id' => $id
                                        );
                                        $count_relationship = $this->Media_model->countMedia('relationship', $relationship_param);
                                        if( $count_relationship == 0 ) {
                                            $save_media_param = array(
                                                'media_type' => 'image',
                                                'media_date' => DATETIME,
                                                'media_title' => $image_name['image_name']
                                            );
                                            $save_media = $this->Media_model->setNewMedia('media', $save_media_param);
                                            if( isset($save_media['id']) ) {
                                                $media_id = $save_media['id'];

                                                // set relationship between media and post
                                                $media_rel_param = array(
                                                    'object_id' => $id,
                                                    'media_id' => $media_id
                                                );
                                                $this->Media_model->setNewMedia('relationship', $media_rel_param);
                                            }
                                        } else {
                                            // update media into database
                                            $update_media_param = array(
                                                'media_modified' => DATETIME,
                                                'media_title' => $image_name['image_name']
                                            );
                                            $this->Media_model->updateMedia('media', $update_media_param, $this->media['media_id']);
                                        }
                                        
                                        // update related category
                                        $merge_category = array_merge($this->category, $this->rel_category);
                                        $merge_category = array_unique($merge_category);
                                        $rel_category_param['object_id'] = $id;
                                        foreach($merge_category as $index) {
                                            $rel_category_param['term_taxonomy_id'] = $index;
                                            if( in_array($index, $this->category) && !in_array($index, $this->rel_category) ) {
                                                $this->Term_model->setNewTerm('relationship', $rel_category_param);
                                            } elseif( !in_array($index, $this->category) && in_array($index, $this->rel_category) ) {
                                                $this->Term_model->deleteTerm('relationship', $rel_category_param);
                                            }

                                            // count every category
                                            $count_taxonomy_param['where'] = array(
                                                'term_taxonomy_id' => $index
                                            );
                                            $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);

                                            // update term count
                                            $update_count_taxonomy = array(
                                                'count' => $count_taxonomy
                                            );
                                            $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $index);
                                        }

                                        // update related tag
                                        $tags = explode(',', $this->tag);
                                        foreach($tags as $index) {
                                            $term_gid = generate_gid($index);

                                            // check existing tag
                                            $check_param = array(
                                                TERM_TAXONOMY.'.taxonomy' => 'tag',
                                                TERMS.'.term_gid' => $term_gid
                                            );
                                            $where_gid['join'] = array(
                                                TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
                                            );
                                            $where_gid['where'] = $check_param;
                                            $count = $this->Term_model->countTerm('term', $where_gid);
                                            if( $count == 0 ) {
                                                // save param
                                                $save_tag_param = array(
                                                    'term_gid' => $term_gid,
                                                    'term_name' => $index
                                                );
                                                $save_tag = $this->Term_model->setNewTerm('term', $save_tag_param);
                                                if( isset($save_tag['id']) ) {
                                                    $term_id = $save_tag['id'];

                                                    // save term taxonomy
                                                    $save_taxonomy_param = array(
                                                        'term_id' => $term_id,
                                                        'taxonomy' => 'tag'
                                                    );
                                                    $save_taxonomy = $this->Term_model->setNewTerm('taxonomy', $save_taxonomy_param);
                                                    if( isset($save_taxonomy['id']) ) {
                                                        $term_taxonomy_id = $save_taxonomy['id'];
                                                    }
                                                }
                                            } else {
                                                $term_param['fields'] = array(
                                                    TERM_TAXONOMY.'.term_taxonomy_id',
                                                );
                                                $term_param['where'] = $check_param;
                                                $term_param['join'] = $where_gid['join'];
                                                $term = $this->Term_model->getTerm('term', $term_param);
                                                $term = array_shift($term);
                                                $term_taxonomy_id = $term['term_taxonomy_id'];
                                            }

                                            $rel_tag_ids[] = $term_taxonomy_id;
                                        }

                                        $merge_tag = array_merge($rel_tag_ids, $this->rel_tag_id);
                                        $merge_tag = array_unique($merge_tag);
                                        $rel_tag_param['object_id'] = $id;
                                        foreach($merge_tag as $index) {
                                            $rel_tag_param['term_taxonomy_id'] = $index;
                                            if( in_array($index, $rel_tag_ids) && !in_array($index, $this->rel_tag_id) ) {
                                                $this->Term_model->setNewTerm('relationship', $rel_tag_param);
                                            } elseif( !in_array($index, $rel_tag_ids) && in_array($index, $this->rel_tag_id) ) {
                                                $this->Term_model->deleteTerm('relationship', $rel_tag_param);
                                            }

                                            // count every category
                                            $count_taxonomy_param['where'] = array(
                                                'term_taxonomy_id' => $index
                                            );
                                            $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);

                                            // update term count
                                            $update_count_taxonomy = array(
                                                'count' => $count_taxonomy
                                            );
                                            $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $index);
                                        }

                                        // set notification
                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    Article has been updated.'.
                                                  '</div>';
                                        $this->session->set_flashdata('notif', $notif);

                                        // redirect to list general info page
                                        redirect('panel/article/edit/'.$id);
                                    }
                                break;
                                case 'error_image':
                                    $data[$k] = array_shift($value);
                                    break;
                            }
                        }
                    } else {
                        $update = $this->Article_model->updateArticle('post', $update_param, $id);
                        if( $update == 'SUCCESSFUL' ) {
                            // update related category
                            $merge_category = array_merge($this->category, $this->rel_category);
                            $merge_category = array_unique($merge_category);
                            $rel_category_param['object_id'] = $id;
                            foreach($merge_category as $index) {
                                $rel_category_param['term_taxonomy_id'] = $index;
                                if( in_array($index, $this->category) && !in_array($index, $this->rel_category) ) {
                                    $this->Term_model->setNewTerm('relationship', $rel_category_param);
                                } elseif( !in_array($index, $this->category) && in_array($index, $this->rel_category) ) {
                                    $this->Term_model->deleteTerm('relationship', $rel_category_param);
                                }

                                // count every category
                                $count_taxonomy_param['where'] = array(
                                    'term_taxonomy_id' => $index
                                );
                                $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);

                                // update term count
                                $update_count_taxonomy = array(
                                    'count' => $count_taxonomy
                                );
                                $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $index);
                            }

                            // update related tag
                            $tags = explode(',', $this->tag);
                            foreach($tags as $index) {
                                $term_gid = generate_gid($index);

                                // check existing tag
                                $check_param = array(
                                    TERM_TAXONOMY.'.taxonomy' => 'tag',
                                    TERMS.'.term_gid' => $term_gid
                                );
                                $where_gid['join'] = array(
                                    TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
                                );
                                $where_gid['where'] = $check_param;
                                $count = $this->Term_model->countTerm('term', $where_gid);
                                if( $count == 0 ) {
                                    // save param
                                    $save_tag_param = array(
                                        'term_gid' => $term_gid,
                                        'term_name' => $index
                                    );
                                    $save_tag = $this->Term_model->setNewTerm('term', $save_tag_param);
                                    if( isset($save_tag['id']) ) {
                                        $term_id = $save_tag['id'];

                                        // save term taxonomy
                                        $save_taxonomy_param = array(
                                            'term_id' => $term_id,
                                            'taxonomy' => 'tag'
                                        );
                                        $save_taxonomy = $this->Term_model->setNewTerm('taxonomy', $save_taxonomy_param);
                                        if( isset($save_taxonomy['id']) ) {
                                            $term_taxonomy_id = $save_taxonomy['id'];
                                        }
                                    }
                                } else {
                                    $term_param['fields'] = array(
                                        TERM_TAXONOMY.'.term_taxonomy_id',
                                    );
                                    $term_param['where'] = $check_param;
                                    $term_param['join'] = $where_gid['join'];
                                    $term = $this->Term_model->getTerm('term', $term_param);
                                    $term = array_shift($term);
                                    $term_taxonomy_id = $term['term_taxonomy_id'];
                                }

                                $rel_tag_ids[] = $term_taxonomy_id;
                            }

                            $merge_tag = array_merge($rel_tag_ids, $this->rel_tag_id);
                            $merge_tag = array_unique($merge_tag);
                            $rel_tag_param['object_id'] = $id;
                            foreach($merge_tag as $index) {
                                $rel_tag_param['term_taxonomy_id'] = $index;
                                if( in_array($index, $rel_tag_ids) && !in_array($index, $this->rel_tag_id) ) {
                                    $this->Term_model->setNewTerm('relationship', $rel_tag_param);
                                } elseif( !in_array($index, $rel_tag_ids) && in_array($index, $this->rel_tag_id) ) {
                                    $this->Term_model->deleteTerm('relationship', $rel_tag_param);
                                }

                                // count every category
                                $count_taxonomy_param['where'] = array(
                                    'term_taxonomy_id' => $index
                                );
                                $count_taxonomy = $this->Term_model->countTerm('relationship', $count_taxonomy_param);

                                // update term count
                                $update_count_taxonomy = array(
                                    'count' => $count_taxonomy
                                );
                                $this->Term_model->updateTerm('taxonomy', $update_count_taxonomy, $index);
                            }

                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Article has been updated.'.
                                      '</div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/article/edit/'.$id);
                        }
                    }
                } # end of validation
            } # end of post

            $this->load->helper('term/term');
            $data['data'] = $article;
            $data['categories'] = get_term_list('category');
            $data['tags'] = get_term_list('tag');
            $data['post_status'] = get_post_status();
        }

        $this->load->view('panel/edit-post', $data);
    }

    // delete article data, include of image
    public function delete($id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);

        // check if article is belongs to the owner of user, except administrator
        $check_owner_param['where'] = array(
            'post_id' => $id,
            'post_author' => $id_user,
        );
        $check_owner = $this->Article_model->countArticle('post', $check_owner_param);
        if( $check_owner == 1 || $data['admin_data']['role'] == 1 ) {
            // provide error message when given ID is empty or 0
            $msg = 'An error occured while deleting article. Please try again later.';
            $alertClass = 'alert-warning';
            if( !empty($id) && is_numeric($id) && $id != 0 ) {
                // check if given ID is existing in database
                $article = $this->Article_model->getArticleById('post', $id);

                // if data is found
                if( !empty($article) ) {
                    $global_param = array('object_id' => $id);
                    
                    // check image
                    $this->load->model('media/Media_model');
                    $media_param['fields'] = array(
                        MEDIA_RELATIONSHIPS.'.*',
                        MEDIAS.'.media_date'
                    );
                    $media_param['where'] = $global_param;
                    $media_param['join'] = array(
                        MEDIAS => MEDIAS.'.media_id = '.MEDIA_RELATIONSHIPS.'.media_id'
                    );
                    $media = $this->Media_model->getMedia('relationship', $media_param);
                    if( !empty($media) ) {
                        $media = array_shift($media);
                        // delete relationship
                        $this->Media_model->deleteMedia('relationship', $global_param);
                        // delete media
                        $this->Media_model->deleteMedia('media', $media['media_id']);
                        // delete image
                        $exp_date = explode(' ', $media['media_date']);
                        $date_directory = $exp_date[0];
                        $img_path = $this->_create_directory($date_directory, $id);

                        $glob = glob($img_path.'*');
                        if( !empty($glob) ) {
                            foreach($glob as $k => $item) {
                                unlink($item);          // remove images
                            }
                            rmdir($img_path);          // remove directory
                        }
                    }

                    // check like and view
                    $activity_param['where'] = array('activity_post_id' => $id);
                    $activity = $this->Article_model->getArticle('activity', $activity_param);
                    if( !empty($activity) ) {
                        $this->Article_model->deleteArticle('activity', $activity_param);
                    }

                    // check term [both category and tag]
                    $this->load->model('term/Term_model');
                    $term_param['fields'] = 'term_taxonomy_id';
                    $term_param['where'] = $global_param;
                    $term = $this->Term_model->getTerm('relationship', $term_param);
                    if( !empty($term) ) {
                        // delete term relationship
                        $delete_term = $this->Term_model->deleteTerm('relationship', $global_param);
                        if( $delete_term == 'SUCCESSFUL' ) {
                            // count term and update
                            foreach($term as $k => $item) {
                                $term_taxonomy_id = $item['term_taxonomy_id'];

                                $count_term_param['where'] = array(
                                    'term_taxonomy_id' => $term_taxonomy_id
                                );
                                $count_term = $this->Term_model->countTerm('relationship', $count_term_param);
                                
                                $update_term_count = array(
                                    'count' => $count_term
                                );
                                $this->Term_model->updateTerm('taxonomy', $update_term_count, $term_taxonomy_id);
                            }
                        }
                    }

                    // delete post
                    $delete = $this->Article_model->deleteArticle('post', $id);
                    // if delete was succeed
                    if( $delete == 'SUCCESSFUL' ) {
                        // provide success message
                        $msg = 'Article with title <b>'.$article['post_title'].'</b> has been deleted.';
                        $alertClass = 'alert-success';
                    }
                }
            }
        } else {
            // provide error message when given ID is empty or 0
            $msg = 'You can not delete an article with <b>ID #'.$id.'</b>, because you are not the owner of the article. Thank you.';
            $alertClass = 'alert-danger';
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
            
        // redirect to list page
        redirect('panel/article/list');
    }
    
    // check datetime format
    public function checkDateFormat($datetime) {
        $data = explode(' ', $datetime);

        $match_date = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data[0]);
        $match_time = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])/", $data[1]);
        if( $match_date && $match_time ) {
            return true;
        } else {
            return false;
        }
    }
}
