<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Article_model extends CI_model {
    
    public $data;
    public $return = array();
    private $activities = array(
        'activity_id',
        'activity_post_id',
        'activity_type',
        'activity_date',
        'activity_ip'
    );
    private $comments = array(
        'comment_id',
        'comment_post_id',
        'comment_date',
        'comment_ip',
        'comment_content',
        'comment_approved',
        'comment_parent',
        'user_id'
    );
    private $posts = array(
        'post_id',
        'post_gid',
        'post_author',
        'post_date',
        'post_modified',
        'post_title',
        'post_content',
        'post_status',
        'comment_status',
        'comment_count',
        'view_count',
        'like_count'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewArticle($type, $data) {
        switch($type) {
            case 'activity': $table = ACTIVITIES; break;
            case 'comment': $table = COMMENTS; break;
            case 'post': $table = POSTS; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getArticle($type, $params=array()) {
        switch($type) {
            case 'activity':
                $fields = $this->activities;
                $table = ACTIVITIES;
                break;
            case 'comment':
                $fields = $this->comments;
                $table = COMMENTS;
                break;
            case 'post':
                $fields = $this->posts;
                $table = POSTS;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getArticleById($type, $id) {
        switch($type) {
            case 'activity':
                $field_id = 'activity_id';
                $fields = $this->activities;
                $table = ACTIVITIES;
                break;
            case 'comment':
                $field_id = 'comment_id';
                $fields = $this->comments;
                $table = COMMENTS;
                break;
            case 'post':
                $field_id = 'post_id';
                $fields = $this->posts;
                $table = POSTS;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getArticleByGid($gid) {
        $params['fields'] = $this->posts;
        $params['table'] = POSTS;
        $params['where'] = array(
            'post_gid' => $gid
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countArticle($type, $params=array()) {
        switch($type) {
            case 'activity':
                $field_id = 'activity_id';
                $table = ACTIVITIES;
                break;
            case 'comment':
                $field_id = 'comment_id';
                $table = COMMENTS;
                break;
            case 'post':
                $field_id = 'post_id';
                $table = POSTS;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateArticle($type, $data, $param) {
        switch($type) {
            case 'activity':
                $field_id = 'activity_id';
                $fields = $this->activities;
                $table = ACTIVITIES;
                break;
            case 'comment':
                $field_id = 'comment_id';
                $fields = $this->comments;
                $table = COMMENTS;
                break;
            case 'post':
                $field_id = 'post_id';
                $fields = $this->posts;
                $table = POSTS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteArticle($type, $param) {
        switch($type) {
            case 'activity':
                $field_id = 'activity_id';
                $table = ACTIVITIES;
                break;
            case 'comment':
                $field_id = 'comment_id';
                $table = COMMENTS;
                break;
            case 'post':
                $field_id = 'post_id';
                $table = POSTS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['where'] = $where;
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}