<?php echo call_header('panel', 'Edit Post'); ?>

<?php echo call_sidebar($admin_data, 'article', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">EDIT POST</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-12">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <?php if( $post_owner == 1 || $admin_data['role'] == 1 ) { ?>
                                    <?php if( isset($data) ) { ?>
                                        <form action="<?php echo site_url('panel/article/edit/'.$data['post_id']); ?>" method="post" enctype="multipart/form-data" novalidate>
                                            <div class="col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <?php echo form_error('post_title'); ?>
                                                    <div class="form-line">
                                                        <input name="post_title" id="post_title" type="text" class="form-control" value="<?php echo $data['post_title']; ?>" autofocus required> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Content</label>
                                                    <?php echo form_error('post_content'); ?>
                                                    <div class="form-line">
                                                        <textarea id="post_content" name="post_content" class="form-control" required="required"><?php echo $data['post_content']; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <?php if(isset($error_image)) echo $error_image; ?>
                                                    <div class="form-line">
                                                        <?php
                                                            $img_name = '';
                                                            $img_url = base_url(IMG_PATH.'default/default_medium-article.png');
                                                            if( !empty($data['media']) ) {
                                                                $media = $data['media'];
                                                                $img_name = $media['media_title'];
                                                                if( !empty($img_name) ) {
                                                                    $date = explode(' ', $media['media_date']);
                                                                    $date = str_replace('-','/',$date[0]).'/'.$data['post_id'].'/';
                                                                    $img_url = base_url(IMG_PATH.'post/'.$date.'medium-'.$img_name);
                                                                }
                                                            }
                                                        ?>
                                                        <input type="file" name="image_name" id="imageInput" class="form-control">
                                                        <input type="hidden" name="image_name" value="<?php echo $img_name; ?>">
                                                    </div>
                                                    <small class="rules">File size should be equal or less than 2 MB, with minimum size 750 x 400 px</small>
                                                    <br><br>
                                                    <img class="img-responsive img-thumbnail" src="<?php echo $img_url; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="post_status" class="form-control show-tick">
                                                        <?php
                                                            foreach($post_status as $k => $item) {
                                                                $selected = '';
                                                                if( $data['post_status'] == $item['status_gid'] ) {
                                                                    $selected = 'selected';
                                                                }

                                                                echo '<option value="'.$item['status_gid'].'" '.$selected.'>'.$item['status_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Date Published</label>
                                                    <?php echo form_error('post_date'); ?>
                                                    <div class="form-line">
                                                        <?php
                                                            // for date
                                                            $status = $data['post_status'];

                                                            $date = $data['post_date'];
                                                            if( $status == 'draft' ) {
                                                                $date = $data['post_modified'];
                                                            }

                                                            // for scheduling
                                                            $checked_scheduling = '';
                                                            $disabled_scheduling = 'disabled';
                                                            $required_scheduling = '';
                                                            if( !empty($data['scheduling']) && $data['scheduling'] == 1 ) {
                                                                $checked_scheduling = 'checked';
                                                                $disabled_scheduling = '';
                                                                $required_scheduling = 'required';
                                                            }
                                                        ?>
                                                        <input type="checkbox" class="filled-in chk-col-red" name="scheduling" id="scheduling" value="1" <?php echo $checked_scheduling; ?>>
                                                        <label for="scheduling">Use scheduling</label>
                                                        <br>
                                                        <input type="text" id="post_date" name="post_date" class="form-control" placeholder="Date for scheduling" value="<?php echo $date; ?>" <?php echo $disabled_scheduling.' '.$required_scheduling; ?>>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <div class="checkbox">
                                                        <?php
                                                            foreach($categories as $k => $item) {
                                                                $check = '';
                                                                if( !empty($data['category']) && in_array($item['term_taxonomy_id'], $data['category']) ) {
                                                                    $check = 'checked';
                                                                }
                                                        ?>
                                                            <input type="checkbox" class="filled-in chk-col-red category" name="category[]" id="<?php echo $item['term_taxonomy_id']; ?>" value="<?php echo $item['term_taxonomy_id']; ?>" <?php echo $check; ?>>
                                                            <label for="<?php echo $item['term_taxonomy_id']; ?>"><?php echo $item['term_name']; ?></label>
                                                            <br>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Tag</label>
                                                    <div class="form-line">
                                                        <input name="tag" type="text" data-role="tagsinput" class="form-control" value="<?php echo $data['tag']; ?>">
    <!--                                                    <select name="tag[]" id="tag" class="form-control show-tick" data-live-search="true" multiple>
                                                            <?php
                                                                foreach($tags as $k => $item) {
                                                                    $selected = '';
                                                                    if( !empty($data['tags']) && in_array($item['term_taxonomy_id'], $data['tags']) ) {
                                                                        $selected = 'selected';
                                                                    }
                                                                    echo '<option value="'.$item['term_taxonomy_id'].'">'.$item['term_name'].'</option>';
                                                                }
                                                            ?>
                                                        </select>-->
                                                    </div>
                                                </div>

                                                <div class="form-group">                                        
                                                    <?php
                                                        $back_url = site_url('panel/article/list');
                                                        if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                                            $back_url = $_SERVER['HTTP_REFERER'];
                                                        }
                                                    ?>
                                                    <a href="<?php echo $back_url; ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                                    <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right" style="margin-right: 10px;" value="UPDATE">
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                        } else {
                                            echo 'Data not found';
                                        }
                                    ?>
                                <?php } else { ?>
                                    <p>You are not allowed to enter this page, because you are not the owner of this article. Thank you.</p>
                                    <a href="<?php echo site_url('panel/article/list'); ?>" class="btn btn-default m-t-15 waves-effect">BACK TO LIST</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<!-- Bootstrap Tagsinput Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'autosize/autosize.js'); ?>"></script>
<!-- Moment Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'momentjs/moment.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
<!-- TinyMCE -->
<script src="<?php echo base_url(PLG_PATH.'tinymce/tinymce.js'); ?>"></script>
<script>
    // for editor, use tinymce for better
    tinymce.init({
        selector: "textarea#post_content",
        theme: "modern",
        height: 400,
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinymce.relative_urls = false;
    
    //Datetimepicker plugin
    function datetime_picker() {
        $('#post_date').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm:00',
            minDate: new Date(),
            clearButton: true,
            weekStart: 0
        })
    }
    
    // Scheduling
    datetime_picker();
    $(document).on('click', '#scheduling', function() {
        if( $(this).is(':checked') ) {
            datetime_picker();
            $('#post_date').attr('required', 'required');
            $('#post_date').attr('disabled', false);
        } else {
            $('#post_date').attr('disabled', 'disabled');
            $('#post_date').attr('required', false);
            $('#post_date').val('');
        }
    });
</script>
