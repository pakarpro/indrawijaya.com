<?php echo call_header('panel', 'All Posts'); ?>

<?php echo call_sidebar($admin_data, 'article', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">ALL POSTS</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                        
                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/article/create'); ?>">ADD NEW</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th>Title</th>
                                    <th width="100">Author</th>
                                    <th width="150">Categories</th>
                                    <th width="150">Date</th>
                                    <th width="20"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($data) ) {
                                        foreach($data as $k => $item) {
                                            // categories
                                            $categories = '--';
                                            if( !empty($item['categories']) ) {
                                                $categories = implode(', ', $item['categories']);
                                            }
                                            // status
                                            $post_date = $item['post_date'];
                                            if( $item['post_status'] == 'draft' ) {
                                                $post_date = $item['post_modified'];
                                            }
                                            $status = format_post_status($item['post_status'], $post_date);
                                ?>
                                    <tr data-id="<?php echo $item['post_id']; ?>">
                                        <td><?php echo $item['post_id']; ?></td>
                                        <td class="text-bold">
                                            <a href="<?php echo site_url('panel/article/edit/'.$item['post_id']); ?>">
                                                <?php echo $item['post_title']; ?>
                                            </a>
                                            <?php
                                                if($item['post_status'] == 'draft') {
                                                    $stts = ucfirst($item['post_status']);
                                            ?>
                                                <span class="text-grey text-bold clearfix">-- <?php echo $stts; ?></span>
                                            <?php } ?>
                                        </td>
                                        <td class="text-bold"><?php echo $item['post_author']; ?></td>
                                        <td><?php echo $categories; ?></td>
                                        <td><?php echo $status; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('panel/article/delete/'.$item['post_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this article ?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="6">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>
<script>
    // updating status of selected data
    $(document).on('click', '._cs', function() {
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post('../changestatus/all', data, function(response) {
            console.log(response);
            // do something here, for custom
        });
    });
</script>