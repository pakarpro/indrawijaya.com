<?php echo call_header('panel', 'Add New Post'); ?>

<?php echo call_sidebar($admin_data, 'article', 'create'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CREATE NEW POST</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <form action="<?php echo site_url('panel/article/create'); ?>" method="post" enctype="multipart/form-data" novalidate>
                                <div class="col-md-8 col-xs-12">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <?php echo form_error('post_title'); ?>
                                        <div class="form-line">
                                            <input name="post_title" id="post_title" type="text" class="form-control" value="<?php echo set_value('post_title'); ?>" autofocus required> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Content</label>
                                        <?php echo form_error('post_content'); ?>
                                        <div class="form-line">
                                            <textarea id="post_content" name="post_content" class="form-control" required="required"><?php echo set_value('post_content'); ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <?php if(isset($error_image)) echo $error_image; ?>
                                        <div class="form-line">
                                            <input type="file" name="image_name" id="imageInput" class="form-control">
                                        </div>
                                        <small class="rules">File size should be equal or less than 2 MB, with minimum size 750 x 400 px</small>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="post_status" class="form-control show-tick">
                                            <?php
                                                $status = 'publish';
                                                if( !empty(set_value('post_status')) ) {
                                                    $status = set_value('post_status');
                                                }
                                                
                                                foreach($post_status as $k => $item) {
                                                    $selected = '';
                                                    if( $status == $item['status_gid'] ) {
                                                        $selected = 'selected';
                                                    }
                                                    echo '<option value="'.$item['status_gid'].'" '.$selected.'>'.$item['status_name'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Date Published</label>
                                        <?php echo form_error('post_date'); ?>
                                        <div class="form-line">
                                            <?php
                                                $checked_scheduling = '';
                                                $disabled_scheduling = 'disabled';
                                                $required_scheduling = '';
                                                if( !empty(set_value('scheduling')) && set_value('scheduling') == 1 ) {
                                                    $checked_scheduling = 'checked';
                                                    $disabled_scheduling = '';
                                                    $required_scheduling = 'required';
                                                }
                                            ?>
                                            <input type="checkbox" class="filled-in chk-col-red" name="scheduling" id="scheduling" value="1" <?php echo $checked_scheduling; ?>>
                                            <label for="scheduling">Use scheduling</label>
                                            <br>
                                            <input type="text" id="post_date" name="post_date" class="form-control" placeholder="Date for scheduling" value="<?php echo set_value('post_date'); ?>" <?php echo $disabled_scheduling.' '.$required_scheduling; ?>>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Category</label>
                                        <div class="checkbox">
                                            <?php
                                                foreach($categories as $k => $item) {
                                                    $check = '';
                                                    if( !empty(set_value('category')) && in_array($item['term_taxonomy_id'], set_value('category')) ) {
                                                        $check = 'checked';
                                                    }
                                            ?>
                                                <input type="checkbox" class="filled-in chk-col-red category" name="category[]" id="<?php echo $item['term_taxonomy_id']; ?>" value="<?php echo $item['term_taxonomy_id']; ?>" <?php echo $check; ?>>
                                                <label for="<?php echo $item['term_taxonomy_id']; ?>"><?php echo $item['term_name']; ?></label>
                                                <br>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Tag</label>
                                        <div class="form-line">
                                            <input name="tag" type="text" data-role="tagsinput" class="form-control" value="<?php echo set_value('tag'); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <?php
                                            $back_url = site_url('panel/article/list');
                                            if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                                $back_url = $_SERVER['HTTP_REFERER'];
                                            }
                                        ?>
                                        <a href="<?php echo $back_url; ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-15" value="CREATE">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<!-- Bootstrap Tagsinput Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'autosize/autosize.js'); ?>"></script>
<!-- Moment Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'momentjs/moment.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
<!-- TinyMCE -->
<script src="<?php echo base_url(PLG_PATH.'tinymce/tinymce.js'); ?>"></script>
<script>
    // for editor, use tinymce for better
    tinymce.init({
        selector: "textarea#post_content",
        theme: "modern",
        height: 400,
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinymce.relative_urls = false;
    
    //Datetimepicker plugin
    function datetime_picker() {
        $('#post_date').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm:00',
            minDate: new Date(),
            clearButton: true,
            weekStart: 0
        })
    }
    
    // Scheduling
    $(document).on('click', '#scheduling', function() {
        if( $(this).is(':checked') ) {
            datetime_picker();
            $('#post_date').attr('required', 'required');
            $('#post_date').attr('disabled', false);
        } else {
            $('#post_date').attr('disabled', 'disabled');
            $('#post_date').attr('required', false);
            $('#post_date').val('');
        }
    });
</script>
