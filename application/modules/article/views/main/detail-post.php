<?php echo call_header('main', 'Article', 'post'); ?>

    <div class="row">
        <div class="span8 blog">
            <?php
                if( !empty($article) ) {
                    // image URL
                    $img_url = base_url(IMG_PATH.'default/default_article.png');
                    if( isset($article['media']) ) {
                        $media = $article['media'];
                        $exp_date = explode(' ', $media['media_date']);
                        $date_directory = str_replace('-','/',$exp_date[0]);
                        $img_url = base_url(IMG_PATH.'post/'.$date_directory.'/'.$article['post_id'].'/'.$media['media_title']);;
                    }
            ?>
                <article>
                    <h3 class="title-bg"><?php echo $article['post_title']; ?></h3>
                    <div class="post-content">
                        <img class="img-responsive" src="<?php echo $img_url; ?>" alt="Post Thumb" width="100%">

                        <div class="post-body"><?php echo $article['post_content']; ?></div>

                        <div class="post-summary-footer">
                            <ul class="post-data">
                                <li><i class="icon-calendar"></i> <?php echo format_datetime($article['post_date'], 'd', true); ?></li>
                                <li><i class="icon-user"></i> <?php echo $article['author_name']; ?></li>
                                <li class="_lk" data-id="<?php echo $article['post_id']; ?>">
                                    <i class="icon-heart"></i> 
                                    <a href="#"><span id="total_<?php echo $article['post_id']; ?>"><?php echo $article['like_count']; ?></span></a>
                                </li>
                                <li><i class="icon-eye-open"></i> <a href="#"><?php echo $article['view_count']; ?></a></li>
                                <?php /* <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li> */ ?>
                            </ul>
                            <br><br>
                            <ul class="post-data">
                                <?php if( !empty($article['rel_categories']) ) { ?>
                                    <li><i class="icon-folder-open"></i> <?php echo implode(', ', $article['rel_categories']); ?></li>
                                <?php } ?>
                                <?php if( !empty($article['rel_tags']) ) { ?>
                                    <li><i class="icon-tags"></i> <?php echo implode(', ', $article['rel_tags']); ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <?php if( isset($pager) ) { ?>
                        <div class="clearfix">
                            <div class="col-xs-12">
                                <ul class="pager">
                                    <?php
                                        foreach($pager as $k => $item) {
                                            switch($k) {
                                                case 'previous';
                                                    $text = '<i class="fa fa-long-arrow-left"></i> Previous';
                                                    break;
                                                case 'next';
                                                    $text = 'Next <i class="fa fa-long-arrow-right"></i>';
                                                    break;
                                            }
                                            $url = site_url('posts/'.$item['post_gid']);
                                    ?>
                                        <li class="<?php echo $k; ?>">
                                            <a href="<?php echo $url; ?>" title="<?php echo $item['post_title']; ?>">
                                                <?php echo $text; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </article>
            <?php } else { ?>
                <h4>Article is not found or maybe has been deleted. Back to <a href="<?php echo site_url('posts'); ?>">List</a>.</h4>
            <?php }?>
        </div><!--Close container row-->

        <!-- Blog Sidebar -->
        <div class="span4 sidebar">
            <!--Search-->
            <section>
                <div class="input-append">
                    <form action="<?php echo site_url('article/search'); ?>" method="get">
                        <input name="q" class="search_box form-control" size="16" type="text" placeholder="Search here">
                        <button class="btn" type="submit"><i class="icon-search"></i></button>
                    </form>
                </div>
            </section>

            <?php if( !empty($categories) ) { ?>
                <!--Categories-->
                <h5 class="title-bg">Categories</h5>
                <ul class="post-category-list">
                    <?php
                        foreach($categories as $k => $item) {
                            // detail post URL
                            $term_url = site_url('category/'.$item['term_gid']);
                    ?>
                        <li>
                            <a href="<?php echo $term_url; ?>"><i class="icon-plus-sign"></i><?php echo $item['term_name']; ?></a>
                            <span class="label label-info pull-right"><?php echo $item['count']; ?></span>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

            <?php if( !empty($popular_post) ) { ?>
                <!--Popular Posts-->
                <h5 class="title-bg">Popular Posts</h5>
                <ul class="popular-posts">
                    <?php
                        foreach($popular_post as $k => $item) {
                            // detail post URL
                            $post_url = site_url('posts/'.$item['post_gid']);
                    ?>
                        <li>
                            <a href="<?php echo $post_url; ?>"><img src="img/gallery/gallery-img-2-thumb.jpg"></a>
                            <h6><a href="#"><?php echo $item['post_title']; ?></a></h6>
                            <em>Posted on <?php echo format_datetime($item['post_date'], 'd', true); ?></em>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._lk', function(e) {
        var id = $(this).attr('data-id');
        
        $.post('/article/set_post_activity/like/', {'post_id':id}, function(res) {
            if( res == 'success' ) {
                var current_total = $('span#total_'+id).text();
                var total = parseInt(current_total) + 1;
                $('span#total_'+id).text(total);
//                $('.fa.fa-heart').addClass('liked');
                alert('Anda menyukai artikel ini');
            } else {
                alert('Anda telah menyukai artikel ini');
            }
        });
    });
</script>