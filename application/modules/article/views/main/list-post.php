<?php echo call_header('main', 'Article', 'post'); ?>

    <!-- Blog Content --> 
    <div class="row">
        <!-- Blog Posts --> 
        <div class="span8 blog blog-list">
            <?php
                if( !empty($article) ) {
                    foreach($article as $k => $item) {
                        // detail post URL
                        $post_url = site_url('posts/'.$item['post_gid']);
                        // image URL
                        $img_url = base_url(IMG_PATH.'default/default_article.png');
                        if( isset($item['media']) ) {
                            $media = $item['media'];
                            $exp_date = explode(' ', $media['media_date']);
                            $date_directory = str_replace('-','/',$exp_date[0]);
                            $img_url = base_url(IMG_PATH.'post/'.$date_directory.'/'.$item['post_id'].'/'.$media['media_title']);;
                        }
                        // annotation
                        $annotation = strip_tags($item['post_content']);
                        $annotation = summarize_sentence($annotation, 20);
            ?> 
                <article>
                    <div class="row">
                        <div class="span2 blog-style-2">
                            <h4 class="title-bg"><?php echo format_datetime($item['post_date'], 'd', true); ?></h4>
                            <ul class="post-data">
                                <li><i class="icon-user"></i> <a href="#"><?php echo $item['author_name']; ?></a></li>
                                <li class="_lk" data-id="<?php echo $item['post_id']; ?>">
                                    <i class="icon-heart"></i> 
                                    <a href="#"><span id="total_<?php echo $item['post_id']; ?>"><?php echo $item['like_count']; ?></span></a>
                                </li>
                                <li><i class="icon-eye-open"></i> <a href="#"><?php echo $item['view_count']; ?></a></li>
                                <?php /*<li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>*/ ?>
                                <?php if( isset($item['rel_tags']) ) { ?>
                                    <li><i class="icon-tags"></i> <?php echo $item['rel_tags']; ?></li>
                                <?php } ?>
                            </ul>
                            <a class="btn btn-small btn-inverse" href="<?php echo $post_url; ?>">Read more</a>
                        </div>
                        <div class="span6">       
                            <a href="<?php echo $post_url; ?>"><img src="<?php echo $img_url; ?>" alt="Post Thumb"></a>
                            <h3 class="title-bg"><a href="<?php echo $post_url; ?>"><?php echo $item['post_title']; ?></a></h3>
                            <p><?php echo $annotation; ?></p>
                        </div>
                    </div>
                </article>
            <?php
                    }
                } else {
            ?>
                <h4>Article is not found or maybe has been deleted. Back to <a href="<?php echo site_url(); ?>">Homepage</a>.</h4>
            <?php } ?>
            
            <?php if( isset($pagination) ) { ?>
                <!-- Pagination -->
                <div class="pagination">
                    <?php print_r($pagination); ?>
                </div>
            <?php } ?>
        </div>

        <!-- Blog Sidebar -->
        <div class="span4 sidebar">
            <!--Search-->
            <section>
                <div class="input-append">
                    <form action="<?php echo site_url('article/search'); ?>" method="get">
                        <input name="q" class="search_box form-control" size="16" type="text" placeholder="Search here">
                        <button class="btn" type="submit"><i class="icon-search"></i></button>
                    </form>
                </div>
            </section>

            <?php if( !empty($categories) ) { ?>
                <!--Categories-->
                <h5 class="title-bg">Categories</h5>
                <ul class="post-category-list">
                    <?php
                        foreach($categories as $k => $item) {
                            // detail post URL
                            $term_url = site_url('category/'.$item['term_gid']);
                    ?>
                        <li>
                            <a href="<?php echo $term_url; ?>"><i class="icon-plus-sign"></i><?php echo $item['term_name']; ?></a>
                            <span class="label label-info pull-right"><?php echo $item['count']; ?></span>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

            <?php if( !empty($popular_post) ) { ?>
                <!--Popular Posts-->
                <h5 class="title-bg">Popular Posts</h5>
                <ul class="popular-posts">
                    <?php
                        foreach($popular_post as $k => $item) {
                            // detail post URL
                            $post_url = site_url('posts/'.$item['post_gid']);
                    ?>
                        <li>
                            <a href="<?php echo $post_url; ?>"></a>
                            <h6><a href="#"><?php echo $item['post_title']; ?></a></h6>
                            <em>Posted on <?php echo format_datetime($item['post_date'], 'd', true); ?></em>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
            
            <?php /*
            <!--Tabbed Content-->
            <h5 class="title-bg">More Info</h5>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#comments" data-toggle="tab">Comments</a></li>
                <li><a href="#tweets" data-toggle="tab">Tweets</a></li>
                <li><a href="#about" data-toggle="tab">About</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="comments">
                     <ul>
                        <li><i class="icon-comment"></i>admin on <a href="#">Lorem ipsum dolor sit amet</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Consectetur adipiscing elit</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Ipsum dolor sit amet consectetur</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Aadipiscing elit varius elementum</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">ulla iaculis mattis lorem</a></li>
                    </ul>
                </div>
                <div class="tab-pane" id="tweets">
                    <ul>
                        <li><a href="#"><i class="icon-share-alt"></i>@room122</a> Vivamus tincidunt sem eu magna varius elementum. Maecenas felis tellus, fermentum vitae laoreet vitae, volutpat et urna.</li>
                        <li><a href="#"> <i class="icon-share-alt"></i>@room122</a> Nulla faucibus ligula eget ante varius ac euismod odio placerat.</li>
                        <li><a href="#"> <i class="icon-share-alt"></i>@room122</a> Pellentesque iaculis lacinia leo. Donec suscipit, lectus et hendrerit posuere, dui nisi porta risus, eget adipiscing</li>
                        <li><a href="#"> <i class="icon-share-alt"></i>@room122</a> Vivamus augue nulla, vestibulum ac ultrices posuere, vehicula ac arcu.</li>
                        <li><a href="#"> <i class="icon-share-alt"></i>@room122</a> Sed ac neque nec leo condimentum rhoncus. Nunc dapibus odio et lacus.</li>
                    </ul>
                </div>
                <div class="tab-pane" id="about">
                    <p>Enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.</p>

                    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                </div>
            </div>
            */ ?>
        </div>
    </div>

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._lk', function(e) {
        var id = $(this).attr('data-id');
        
        $.post('/article/set_post_activity/like/', {'post_id':id}, function(res) {
            if( res == 'success' ) {
                var current_total = $('span#total_'+id).text();
                var total = parseInt(current_total) + 1;
                $('span#total_'+id).text(total);
//                $('.fa.fa-heart').addClass('liked');
                alert('Anda menyukai artikel ini');
            } else {
                alert('Anda telah menyukai artikel ini');
            }
        });
    });
</script>