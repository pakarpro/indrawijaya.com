<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('count_article')){
    function count_article($type='all', $additional_param=array()){
        $CI = &get_instance();
        $CI->load->model('article/Article_model');
        
        $where_param = array('post_status' => $type);
        if( !empty($additional_param) ) {
            $where_param = array_merge($where_param, $additional_param);
        }
        $where['where'] = $where_param;
        $article = $CI->Article_model->countArticle('post', $where_param);

        return $article;
    }
}

if(!function_exists('get_post_status')){
    function get_post_status(){
        $status = array(
            array(
                'status_gid' => 'draft',
                'status_name' => 'Draft',
            ),
            array(
                'status_gid' => 'published',
                'status_name' => 'Published',
            ),
            array(
                'status_gid' => 'closed',
                'status_name' => 'Closed',
            )
        );

        return $status;
    }
}

if(!function_exists('format_post_status')){
    function format_post_status($post_status, $datetime, $usage=''){
        // status
        switch($post_status) {
            case 'draft':
                $label = 'Last Modified';
                break;
            case 'published':
                $label = 'Published on';
                if( DATETIME < $datetime ) {
                    $label = 'Scheduled for';
                }
                break;
            case 'closed':
                $label = 'Closed';
                break;
        }
        
        $date = format_datetime($datetime, 'dt', true);
        $status = '<span>'.$label.'</span><br>';
        $status .= '<span class="text-bold text-grey">'.$date.'</span>';

        return $status;
    }
}

if( !function_exists('get_article') ) {
    function get_article($type='', $limit=5) {
        $CI = &get_instance();
        $CI->load->model('article/Article_model');
        $CI->load->model('media/media_model');
        
        $param['fields'] = array(
            POSTS.'.post_id',
            POSTS.'.post_gid',
            POSTS.'.post_date',
            POSTS.'.post_title',
            POSTS.'.post_content'
        );
        $param['where'] = array(
            'post_status' => 'published',
            '(NOW() >= post_date)' => NULL
        );
        $param['limit'] = $limit;
        if( !empty($type) ) {
            switch($type) {
                case 'recent':
                    $param['order_by'] = array(
                        'post_date' => 'DESC'
                    );
                    break;
                case 'popular':
                    $param['order_by'] = array(
                        'like_count' => 'DESC'
                    );
                    break;
                case 'most_viewed':
                    $param['order_by'] = array(
                        'view_count' => 'DESC'
                    );
                    break;
            }
        }
        $article = $CI->Article_model->getArticle('post', $param);
        
        return $article;
    }
}

