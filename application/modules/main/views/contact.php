<?php echo call_header('main', 'Contact', 'contact'); ?>

    <!-- Page Content --> 
    <div class="row"><!--Container row-->
        <div class="span8 contact"><!--Begin page content column-->
            <h3>Contact Me</h3>

            <div class="status alert alert-success" style="display: none;"></div>
            <form id="contact-form" class="contact-form" name="contact-form" method="post" action="<?php echo site_url('mail/send'); ?>">
                <div class="form-group">
                    <input type="text" name="sender_name" id="name" class="form-control span4" placeholder="Name *">
                </div>
                <div class="form-group">
                    <input type="email" name="sender_email" id="email" class="form-control span4" placeholder="Email *">
                </div>
                <div class="form-group">
                    <input type="number" name="sender_phone" id="phone" class="form-control span4" placeholder="Phone Number">
                </div>
                <div class="form-group">
                    <input type="text" name="subject" id="subject" class="form-control span4" placeholder="Subject *">
                </div>
                <div class="form-group">
                    <textarea name="message" id="message" class="form-control span6" rows="8" placeholder="Message *"></textarea>
                </div>                        
                <div class="form-group">
                    <button id="btn-contact" class="btn btn-primary btn-lg">Submit Message</button>
                    <p style="margin-top: 5px;"><i>*) required</i></p>
                </div>
            </form> 
        </div> <!--End page content column-->

        <!-- Sidebar
        ================================================== --> 
        <div class="span4 sidebar page-sidebar"><!-- Begin sidebar column -->
            <h5 class="title-bg">Contact Info</h5>
            <address>
                <strong>Indra Wijaya</strong><br>
                <abbr title="Phone">Phone:</abbr> (123) 456-7890 <br>
                <abbr title="Email">Email:</abbr> <a href="mailto:#">indra.w.wong@gmail.com</a> <br>
            </address>

            <?php if( !empty($social_account) ) { ?>
                <h5 class="title-bg">Follow Me</h5>
                <address>
                    <ul class="social-icons" style="margin:0; padding:0;">
                        <?php
                            foreach($social_account as $k => $account) {
                                $social_url = $account['social_media_url'].'/'.$account['account_name'];
                                $fa_icon = 'fa-'.$account['social_media_gid'];
                        ?>
                            <li style="margin-right:10px;">
                                <a href="<?php echo $social_url; ?>" target="_blank">
                                    <i class="fa fa-2x <?php echo $fa_icon; ?>"></i>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </address>
            <?php } ?>

        </div><!-- End sidebar column -->

    </div><!-- End container row -->
    
<?php echo call_footer('main'); ?>
<script>
    // Contact form
    var form = $('#contact-form');
    form.submit(function(event){ 
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: form.serialize(),
            success: function(data) {
                var msg = JSON.parse(data);

                switch( msg.status ) {
                    case 'success':
                        $('span.text-error').remove();
                        $('.form-control').val('');
                        $('div.alert').show().html(
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+
                            'Pesan berhasil dikirim'
                        ).delay(5000).fadeOut();
                        break;
                    case 'failed':
                        $('span.text-error').remove();
                        
                        var error = msg.message;
                        $(error.sender_name).insertAfter('input#name');
                        $(error.sender_email).insertAfter('input#email');
                        $(error.sender_phone).insertAfter('input#phone');
                        $(error.subject).insertAfter('input#subject');
                        $(error.message).insertAfter('textarea#message');
                        break;
                }
            }
        });
        event.preventDefault();
    });
</script>
    