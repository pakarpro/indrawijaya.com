<?php echo call_header('main', 'Home', 'home'); ?>

    <?php
        if( !empty($most_viewed_post) ) {
            // detail post URL
            $post_url = site_url('posts/'.$most_viewed_post['post_gid']);
            // image
            $img_url = base_url(IMG_PATH.'default/default_article.png');
            if( isset($most_viewed_post['media']) ) {
                $media = $most_viewed_post['media'];
                $exp_date = explode(' ', $media['media_date']);
                $date_directory = str_replace('-','/',$exp_date[0]);
                $img_url = base_url(IMG_PATH.'post/'.$date_directory.'/'.$most_viewed_post['post_id'].'/'.$media['media_title']);;
            }
            // annotation
            $annotation = strip_tags($most_viewed_post['post_content']);
            $annotation = summarize_sentence($annotation, 10);
    ?>
        <div class="row headline">
            <div class="span8">
                <a href="<?php echo $post_url; ?>">
                    <img class="img-responsive" src="<?php echo $img_url; ?>" alt="slider" width="100%">
                </a>
            </div>
            <div class="span4">
                <h3><?php echo $most_viewed_post['post_title']; ?></h3>
                <p><?php echo $annotation; ?></p>
                <a href="<?php echo $post_url; ?>"><i class="icon-plus-sign"></i>Read More</a> 
            </div>
        </div><!-- End Headline -->
    <?php } ?>

    <?php if( !empty($recent_post) ) { ?>
        <div class="row blog"><!-- Begin Recent Post -->
            <div class="span12">
                <h5 class="title-bg">Recent Post</h5>
            </div>
            <?php
                $no = 1;
                foreach($recent_post as $k => $item) {
                    // detail post URL
                    $post_url = site_url('posts/'.$item['post_gid']);
                    // image
                    $img_url = base_url(IMG_PATH.'default/default_medium-article.png');
                    if( isset($item['media']) ) {
                        $media = $item['media'];
                        $exp_date = explode(' ', $media['media_date']);
                        $date_directory = str_replace('-','/',$exp_date[0]);
                        $img_url = base_url(IMG_PATH.'post/'.$date_directory.'/'.$item['post_id'].'/'.$media['media_title']);;
                    }
                    // annotation
                    $annotation = strip_tags($item['post_content']);
                    $annotation = summarize_sentence($annotation, 10);
            ?>
                <div class="span3">
                    <a href="<?php echo $post_url; ?>">
                        <img class="img-responsive" src="<?php echo $img_url; ?>" alt="" width="100%">
                    </a>
                    <h5 class="title-bg">
                        <a href="<?php echo $post_url; ?>"><?php echo $item['post_title']; ?></a>
                    </h5>
                    <p><?php echo $annotation; ?></p>
                </div>
                <?php
//                        if( $no % 4 == 0 ) {
//                            echo '</div>';
//                            echo '<div class="clearfix">';
//                        }
//                        
//                        $no++;
                    }
                ?>
        </div><!-- End Recent Post -->
    <?php } ?>
        
    <?php if( !empty($popular_post) ) { ?>
        <div class="row blog"><!-- Begin Popular -->
            <div class="span12">
                <h5 class="title-bg">Popular</h5>
            </div>
            <div class="clearfix">
                <?php
                    $no = 1;
                    foreach($popular_post as $k => $item) {
                        // detail post URL
                        $post_url = site_url('posts/'.$item['post_gid']);
                        // image
                        $img_url = base_url(IMG_PATH.'default/default_medium-article.png');
                        if( isset($item['media']) ) {
                            $media = $item['media'];
                            $exp_date = explode(' ', $media['media_date']);
                            $date_directory = str_replace('-','/',$exp_date[0]);
                            $img_url = base_url(IMG_PATH.'post/'.$date_directory.'/'.$item['post_id'].'/'.$media['media_title']);;
                        }
                        // annotation
                        $annotation = strip_tags($item['post_content']);
                        $annotation = summarize_sentence($annotation, 10);
                ?>
                    <div class="span3">
                        <a href="<?php echo $post_url; ?>">
                            <img class="img-responsive" src="<?php echo $img_url; ?>" alt="" width="100%">
                        </a>
                        <h5 class="title-bg">
                            <a href="<?php echo $post_url; ?>"><?php echo $item['post_title']; ?></a>
                        </h5>
                        <p><?php echo $annotation; ?></p>
                    </div>
                <?php
//                        if( $no % 4 == 0 ) {
//                            echo '</div>';
//                            echo '<div class="clearfix">';
//                        }
//                        
//                        $no++;
                    }
                ?>
            </div>
        </div><!-- End Popular -->
    <?php } ?>
        
<?php echo call_footer('main'); ?>