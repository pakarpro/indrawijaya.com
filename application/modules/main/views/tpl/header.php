<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $page; ?> | Indra Wijaya</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url(IMG_PATH.'favicon.ico'); ?>">

    <link href="<?php echo base_url(MAIN_PATH.'css/main.fonts.css'); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(MAIN_PATH.'css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url(MAIN_PATH.'css/bootstrap-responsive.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url(MAIN_PATH.'css/custom-styles.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url(PLG_PATH.'font-awesome/css/font-awesome.min.css'); ?>">
    <style type="text/css">
        :root #header + #content > #left > #rlblock_left
        { display: none !important; }
    </style>
</head>

<body class="home">
    <!-- Color Bars (above header)-->
    <div class="color-bar-1"></div>
    <div class="color-bar-2 color-bg"></div>

    <div class="container">
        <div class="row header"><!-- Begin Header -->
            <!-- Logo -->
            <div class="span5 logo">
                <a href="<?php echo site_url(); ?>">
                    <h2>Indra Wijaya</h2>
                </a>
            </div>

            <!-- Main Navigation -->
            <div class="span7 navigation">
                <div class="navbar hidden-phone">
                    <ul class="nav">
                        <li class="dropdown <?php if($gid == 'home') { echo 'active'; } ?>">
                            <a href="<?php echo site_url(); ?>">Home</a>
                        </li>
                        <li class="<?php if($gid == 'biograph') { echo 'active'; } ?>">
                            <a href="<?php echo site_url('biography'); ?>">Personal</a>
                        </li>
                        <li class="dropdown <?php if($gid == 'post') { echo 'active'; } ?>">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url('posts'); ?>">Blog <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('posts'); ?>">Semua</a>
                                </li>
                                <?php foreach($menu as $k => $item) { ?>
                                    <li>
                                        <a href="<?php echo site_url('category/'.$item['term_gid']); ?>"><?php echo $item['term_name']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="<?php if($gid == 'contact') { echo 'active'; } ?>">
                            <a href="<?php echo site_url('contact'); ?>">Contact</a>
                        </li>
                    </ul>
                </div>

                <!-- Mobile Nav -->
                <form action="#" id="mobile-nav" class="visible-phone">
                    <div class="mobile-nav-select">
                        <select onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                            <option value="">- MENU -</option>
                            <option value="<?php echo site_url(); ?>">Home</option>
                            <option value="<?php echo site_url('posts'); ?>">All Post</option>
                            <option value="">Categories</option>
                                <option value="<?php echo site_url('posts'); ?>">- Semua</option>
                                <?php foreach($menu as $k => $item) { ?>
                                    <option value="<?php echo site_url('category/'.$item['term_gid']); ?>">- <?php echo $item['term_name']; ?></option>
                                <?php } ?>
                            <option value="<?php echo site_url('contact'); ?>">Contact</option>
                        </select>
                    </div>
                </form>
            </div>
        </div><!-- End Header -->
