    </div> <!-- End Container -->

    <!-- Footer -->
    <div class="footer-container">
        <div class="container">
            <div class="row footer-row">
                <div class="span4 footer-col">
                    <h4>INDRA WIJAYA</h4>
                    <?php /*
                    <br><br>
                    <address>
                        <strong>Design Team</strong><br>
                        123 Main St, Suite 500<br>
                        New York, NY 12345<br>
                    </address>
                    */ ?>
                    <h5 style="margin-bottom:5px;">Follow on</h5>
                    <?php if( !empty($social_account) ) { ?>
                        <ul class="social-icons">
                            <?php
                                foreach($social_account as $k => $account) {
                                    $social_url = $account['social_media_url'].'/'.$account['account_name'];
                                    $fa_icon = 'fa-'.$account['social_media_gid'];
                            ?>
                                <li style="margin-right:10px;">
                                    <a href="<?php echo $social_url; ?>" target="_blank">
                                        <i class="fa fa-3x <?php echo $fa_icon; ?>"></i>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="span4 footer-col">
                    <h4>Categories</h4>
                    <ul>
                        <?php foreach($menu as $k => $item) { ?>
                            <li style="padding-bottom:10px;">
                                <a href="<?php echo site_url('category/'.$item['term_gid']); ?>"><?php echo $item['term_name']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="span4 footer-col">
                    <h4>Latest Posts</h4>
                    <?php if( !empty($latest_post) ) { ?>
                        <ul class="post-list">
                            <?php foreach($latest_post as $k => $item) { ?>
                                <li>
                                    <a href="<?php echo site_url('posts/'.$item['post_gid']); ?>"><?php echo $item['post_title']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="span12 footer-col footer-sub">
                    <div class="row no-margin">
                        <div class="span6"><span class="left">Copyright 2017 Indra Wijaya. All rights reserved | Template By Piccolo Theme</span></div>
                        <div class="span6">
                            <span class="right">
                                <a href="<?php echo site_url(); ?>">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url('biography'); ?>">Personal</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url('posts'); ?>">Blog</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo site_url('contact'); ?>">Contact</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div><!-- End Sub Footer -->
        </div>
    </div><!-- End Footer --> 

    <!-- Scroll to Top -->  
    <div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

    <!-- Javascript -->
    <script src="<?php echo base_url(MAIN_PATH.'js/jquery-1.8.3.min.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/jquery.custom.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/bootstrap.js'); ?>"></script>
</body>
</html>
