<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    private $CI;
    
    public function __construct() {
        parent::__construct();
        
        $this->CI = &get_instance();
    }
    
    public function index() {
        $this->homepage();
    }
    
    /*
     * Homepage of the website
     */
    public function homepage() {
        $data = array();
        
        $this->load->helper('article/article');
        $this->load->model('media/Media_model');
        
        // most_viewed post
        $most_viewed_post = get_article('most_viewed', 1);
        if( !empty($most_viewed_post) ) {
            $most_viewed_post = array_shift($most_viewed_post);
            
            // check relationship param
            $relationship_param['where'] = array(
                'object_id' => $most_viewed_post['post_id']
            );
            $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
            if( !empty($relationship) ){
                $relationship = array_shift($relationship);

                // get the media by id
                $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                $most_viewed_post['media'] = $media;
            }
        }
        $data['most_viewed_post'] = $most_viewed_post;
        
        // recent post
        $recent_post = get_article('recent', 4);
        if( !empty($recent_post) ) {
            foreach($recent_post as $k => $item) {
                // check relationship param
                $relationship_param['where'] = array(
                    'object_id' => $item['post_id']
                );
                $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                if( !empty($relationship) ){
                    $relationship = array_shift($relationship);
                    
                    // get the media by id
                    $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                    $item['media'] = $media;
                    
                    $popular_post[$k] = $item;
                }
            }
        }
        $data['recent_post'] = $recent_post;
        
        // popular post
        $popular_post = get_article('popular', 4);
        if( !empty($popular_post) ) {
            foreach($popular_post as $k => $item) {
                // check relationship param
                $relationship_param['where'] = array(
                    'object_id' => $item['post_id']
                );
                $relationship = $this->Media_model->getMedia('relationship', $relationship_param);
                if( !empty($relationship) ){
                    $relationship = array_shift($relationship);
                    
                    // get the media by id
                    $media = $this->Media_model->getMediaById('media', $relationship['media_id']);
                    $item['media'] = $media;
                    
                    $popular_post[$k] = $item;
                }
            }
        }
        $data['popular_post'] = $popular_post;
        
        $this->load->view('homepage', $data);
    }
    
    /*
     * Biography
     */
    public function biography() {
        // get biography
        $this->load->helper('biograph/biograph');
        $this->load->model('biograph/Biograph_model');

        $biographs = array('profile','contact','education','career');
        foreach($biographs as $type) {
            // function name
            $func_name = '_'.strtolower($type);
            
            // get biograph term id
            $biograph_term = $this->Biograph_model->getBiographByGid($type);
            $term_id = $biograph_term['biograph_term_id'];

            // get biograph based on term id, this is main variable for where condition
            $where['where'] = array('biograph_term_id' => $term_id);
            if( $type == 'education' || $type == 'career' ) {
                $where['order_by'] = array('sorter' => 'DESC');
            }
            $biograph = $this->Biograph_model->getBiograph('biograph', $where);
            $biograph = $func_name($biograph, 'out');
            
            $data[$type] = $biograph;
        }
        
        $this->load->view('biograph/main/biography', $data);
    }
    
    /*
     * Contact Us
     */
    public function contact() {
        // get active sosmed account
        $this->load->helper('social_media/social_media');
        $data['social_account'] = get_social_account('active');
        
        $this->load->view('contact', $data);
    }
}
