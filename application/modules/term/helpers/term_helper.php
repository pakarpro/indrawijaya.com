<?php
    if( !function_exists('get_term_relationship') ){
    function get_term_relationship($type='category', $post_id) {
        $CI = &get_instance();
        $CI->load->model('term/Term_model');
        
        $where['fields'] = array(
            TERMS.'.term_gid',
            TERMS.'.term_name',
            TERM_TAXONOMY.'.term_taxonomy_id'
        );
        $where['join'] = array(
            TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
            TERM_RELATIONSHIPS => TERM_RELATIONSHIPS.'.term_taxonomy_id = '.TERM_TAXONOMY.'.term_taxonomy_id',
        );
        $where['where'] = array(
            TERM_RELATIONSHIPS.'.object_id' => $post_id,
            TERM_TAXONOMY.'.taxonomy' => $type
        );
        $terms = $CI->Term_model->getTerm('term', $where);
        
        return $terms;
    }
}

if( !function_exists('get_term_list') ){
    function get_term_list($type='category') {
        $CI = &get_instance();
        $CI->load->model('term/Term_model');
        
        $where['fields'] = array(
            TERMS.'.term_gid',
            TERMS.'.term_name',
            TERM_TAXONOMY.'.term_taxonomy_id',
            TERM_TAXONOMY.'.count'
        );
        $where['join'] = array(
            TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id',
        );
        $where['where'] = array(
            TERM_TAXONOMY.'.taxonomy' => $type
        );
        $terms = $CI->Term_model->getTerm('term', $where);
        
        return $terms;
    }
}

if( !function_exists('get_term_menu') ) {
    function get_term_menu() {
        $CI = &get_instance();
        $CI->load->model('term/Term_model');
        
        $where['fields'] = array(
            TERMS.'.term_gid',
            TERMS.'.term_name'
        );
        $where['where'] = array(
            TERM_TAXONOMY.'.taxonomy' => 'category'
        );
        $where['join'] = array(
            TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id'
        );
//        $where['order_by'] = array(
//            TERM_TAXONOMY.'.count' => 'DESC'
//        );
//        $where['limit'] = 5;
        $menu = $CI->Term_model->getTerm('term', $where);
//        print_r($menu); exit;
        
        return $menu;
    }
}
