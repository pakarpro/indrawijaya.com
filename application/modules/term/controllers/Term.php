<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Term extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/login';
    private $admin_data = array();
    private $term_validation_config = array(
        array(
            'field'   => 'term_name',
            'label'   => 'Term name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z0-9\s\/\_]+$/]'
        ),
        array(
            'field'   => 'term_gid',
            'label'   => 'Keyword',
            'rules'   => 'trim|regex_match[/^[a-z0-9\s\-]+$/]'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'regex_match' => '%s does not follow approriate format'
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->model('Term_model');

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
   
    // getting term info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    public function index() {
        $this->lists();
    }
    
    // show data [term, taxonomy, relationships]
    public function lists($type='category', $page=1) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);

        $where_param['where'] = array(
            'taxonomy' => $type
        );
        $count_term = $this->Term_model->countTerm('taxonomy', $where_param);
        if( $count_term > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            // where param
            $where_param['fields'] = array(
                TERMS.'.*',
                TERM_TAXONOMY.'.description',
                TERM_TAXONOMY.'.count'
            );
            $where_param['join'] = array(
                TERMS => TERMS.'.term_id = '.TERM_TAXONOMY.'.term_id'
            );
            $where_param['limit'] = array($limit_per_page, $start_row);
            if( $count_term > $limit_per_page ) {
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/'.$type.'/list/'),
                    'total_rows'    =>  $count_term,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            $data['data'] = $this->Term_model->getTerm('taxonomy', $where_param);
        }

        $this->load->view('list-'.$type, $data);
    }
    
    // create new data [term, taxonomy, relationships]
    public function create($type='category') {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();

            $gid = generate_gid($post_data['term_gid']);
            if( empty($post_data['term_gid']) ) {
                $gid = generate_gid($post_data['term_name']);
            }
            $data['term_gid'] = $gid;

            $this->form_validation->set_rules($this->term_validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                // check existing gid
                $where_gid['where'] = array(
                    'taxonomy' => $type,
                    'term_gid' => $gid
                );
                $where_gid['join'] = array(
                    TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id'
                );
                $count = $this->Term_model->countTerm('term', $where_gid);
                if( $count == 0 ) {
                    // save param
                    $save_term_param = array(
                        'term_gid' => $gid,
                        'term_name' => $post_data['term_name']
                    );
                    $save_term = $this->Term_model->setNewTerm('term', $save_term_param);
                    if( isset($save_term['id']) ) {
                        $id = $save_term['id'];

                        // save term taxonomy
                        $save_taxonomy_param = array(
                            'term_id' => $id,
                            'taxonomy' => $type,
                            'description' => ucfirst($post_data['description']),
                        );
                        $save_taxonomy = $this->Term_model->setNewTerm('taxonomy', $save_taxonomy_param);
                        if( isset($save_taxonomy['id']) ) {
                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" term="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        New '.$type.' has been created.
                                      </div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/term/'.$type.'/edit/'.$id);
                        }
                    }
                } else {
                    $error_term_gid = 'Keyword is already exist';
                    $data['error_term_gid'] = error_msg_generator($error_term_gid);
                }
            }
        }

        // view page (create term or term)
        $this->load->view('create-'.$type, $data);
    }
    
    // update data [term, term-term]
    public function edit($type='category', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $where_param['fields'] = array(
                TERMS.'.*',
                TERM_TAXONOMY.'.description'
            );
            $where_param['where'] = array(
                TERMS.'.term_id' => $id,
                TERM_TAXONOMY.'.taxonomy' => $type
            );
            $where_param['join'] = array(
                TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id'
            );
            $term = $this->Term_model->getTerm('term', $where_param);
            if( !empty($term) ) {
                $term = array_shift($term);
                if(!empty($_POST)) {
                    // get post data
                    $post_data = $this->input->post();
    
                    // replacing data on variable $term using posted data
                    $gid = generate_gid($post_data['term_gid']);
                    if( empty($post_data['term_gid']) ) {
                        $gid = generate_gid($post_data['term_name']);
                    }
                    if( !empty($post_data['description']) ) {
                        $term['description'] = ucfirst($post_data['description']);
                    }
                    $term['term_name'] = $post_data['term_name'];
                    $term['term_gid'] = $gid;

                    $this->form_validation->set_rules($this->term_validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        // check existing gid
                        $where_gid['where'] = array(
                            'taxonomy' => $type,
                            'term_gid' => $term['term_gid'],
                            TERMS.'.term_id != ' => $id
                        );
                        $where_gid['join'] = array(
                            TERM_TAXONOMY => TERM_TAXONOMY.'.term_id = '.TERMS.'.term_id'
                        );
                        $count = $this->Term_model->countTerm('term', $where_gid);
                        if( $count == 0 ) {
                            // update term first
                            $update_term_param = array(
                                'term_gid' => $term['term_gid'],
                                'term_name' => $term['term_name']
                            );
                            $update_term = $this->Term_model->updateTerm('term', $update_term_param, $id);
                            if( $update_term == 'SUCCESSFUL' ) {
                                // update term taxonomy
                                $update_taxonomy_param = array(
                                    'description' => $term['description']
                                );
                                $where_taxonomy = array(
                                    'term_id' => $id,
                                    'taxonomy' => $type,
                                );
                                $update_taxonomy = $this->Term_model->updateTerm('taxonomy', $update_taxonomy_param, $where_taxonomy);
                                if( $update_taxonomy == 'SUCCESSFUL' ) {
                                    // set notification
                                    $subject = ucfirst($type);
                                    $notif = '<div class="alert alert-success alert-dismissible" term="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                '.$subject.' has been updated.'.
                                              '</div>';
                                    $this->session->set_flashdata('notif', $notif);

                                    // redirect to list general info page
                                    redirect('panel/term/'.$type.'/edit/'.$id);
                                }
                            }
                        } else {
                            $error_term_gid = 'Keyword is already exist';
                            $data['error_term_gid'] = error_msg_generator($error_term_gid);
                        }
                    } # end of validation
                } # end of post
                
                $data['data'] = $term;
            }
        }

        $this->load->view('edit-'.$type, $data);
    }
    
    // delete data [term, term-term]
    public function delete($type='category', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting term. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            // check if given ID is existing in database
            $term = $this->Term_model->getTermById('term', $id);
            // if data is found
            if( !empty($term) ) {
                $name = $term['term_name'];

                // delete taxonomy first
                $where_taxonomy_param = array(
                    'term_id' => $id,
                    'taxonomy' => $type,
                );
                $delete_taxonomy = $this->Term_model->deleteTerm('taxonomy', $where_taxonomy_param);
                if( $delete_taxonomy == 'SUCCESSFUL' ) {
                    $delete_term = $this->Term_model->deleteTerm('term', $id);
                    // if delete was succeed
                    if( $delete_term == 'SUCCESSFUL' ) {
                        // provide success message
                        $msg = 'Term of '.$type.' with name <b>'.$name.'</b> has been deleted.';
                        $alertClass = 'alert-success';
                    }
                }
            }
        }
        
        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" term="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/term/'.$type.'/list/');
    }
}