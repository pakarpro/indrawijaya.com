<?php echo call_header('panel', 'Create New Tag'); ?>

<?php echo call_sidebar($admin_data, 'article', 'tag'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CREATE NEW TAG</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>

                                <form action="<?php echo site_url('panel/term/tag/create'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Term name</label>
                                        <?php echo form_error('term_name'); ?>
                                        <div class="form-line">
                                            <input name="term_name" id="term_name" type="text" class="form-control" value="<?php echo set_value('term_name'); ?>" autofocus required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <?php if( isset($error_term_gid) ) { echo $error_term_gid; } ?>
                                        <div class="form-line">
                                            <?php
                                                $gid = set_value('term_gid');
                                                if(isset($term_gid)) {
                                                    $gid = $term_gid;
                                                }
                                            ?>
                                            <input name="term_gid" id="term_gid" type="text" class="form-control" value="<?php echo $gid; ?>">
                                        </div>
                                        <p>
                                            <small>Must contain only letters, numbers, and hyphen.
                                            If you leave this empty, keyword will automatically generated from the Term name.</small>
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label>Description</label>
                                        <div class="form-line">
                                            <textarea class="form-control" rows="5" name="description"><?php echo set_value('description'); ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">                                        
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect m-r-15" value="CREATE">
                                        <a href="<?php echo site_url('panel/term/tag/list'); ?>" class="btn btn-default m-t-15 waves-effect">BACK</a>
                                    </div>
                                </form>                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
