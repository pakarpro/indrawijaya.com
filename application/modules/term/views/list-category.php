<?php echo call_header('panel', 'Category'); ?>

<?php echo call_sidebar($admin_data, 'article', 'category'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CATEGORY</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>

                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/term/category/create'); ?>">CREATE NEW CATEGORY</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="30">#</th>
                                    <th width="150">Category Name</th>
                                    <th width="150">Keyword</th>
                                    <th>Description</th>
                                    <th width="30">Count</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($data) ) {
                                        foreach($data as $k => $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item['term_id']; ?></td>
                                        <td class="text-bold">
                                            <a href="<?php echo site_url('panel/term/category/edit/'.$item['term_id']); ?>">
                                                <?php echo $item['term_name']; ?>
                                            </a>
                                        </td>
                                        <td><?php echo $item['term_gid']; ?></td>
                                        <td><?php echo $item['description']; ?></td>
                                        <td><?php echo $item['count']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('panel/term/category/delete/'.$item['term_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this category ?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="6">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>

                        <?php if( isset($pagination) ) { ?>
                            <!-- pagination -->
                            <div class="row clearfix">
                                <div class="col-xs-12 text-center">
                                    <?php print_r($pagination); ?>
                                </div>
                            </div>
                            <!-- /pagination -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>