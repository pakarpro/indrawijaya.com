<?php echo call_header('panel', 'Edit Category'); ?>

<?php echo call_sidebar($admin_data, 'article', 'category'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">EDIT CATEGORY</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/term/category/edit/'.$data['term_id']); ?>" method="post">
                                        <div class="form-group">
                                            <label>Term name</label>
                                            <?php echo form_error('term_name'); ?>
                                            <div class="form-line">
                                                <input name="term_name" id="term_name" type="text" class="form-control" value="<?php echo $data['term_name']; ?>" autofocus required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Keyword</label>
                                            <?php if( isset($error_term_gid) ) { echo $error_term_gid; } ?>
                                            <div class="form-line">
                                                <input name="term_gid" id="term_gid" type="text" class="form-control" value="<?php echo $data['term_gid']; ?>">
                                            </div>
                                            <p>
                                                <small>Must contain only letters, numbers, and hyphen.
                                                If you leave this empty, keyword will automatically generated from the Term name.</small>
                                            </p>
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <div class="form-line">
                                                <textarea class="form-control" rows="5" name="description"><?php echo $data['description']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <?php
                                                $back_url = site_url('panel/term/category/list');
                                                if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                                    $back_url = $_SERVER['HTTP_REFERER'];
                                                }
                                            ?>
                                            <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-r-15">UPDATE</button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                        </div>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
