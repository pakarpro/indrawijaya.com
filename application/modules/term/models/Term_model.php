<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Term_model extends CI_model {
    
    public $data;
    public $return = array();
    private $terms = array(
        'term_id',
        'term_gid',
        'term_name'
    );
    private $term_relationships = array(
        'rel_id',
        'object_id',
        'term_taxonomy_id'
    );
    private $term_taxonomy = array(
        'term_taxonomy_id',
        'term_id',
        'taxonomy',
        'description',
        'parent',
        'count'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewTerm($type, $data) {
        switch($type) {
            case 'term': $table = TERMS; break;
            case 'relationship': $table = TERM_RELATIONSHIPS; break;
            case 'taxonomy': $table = TERM_TAXONOMY; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getTerm($type, $params=array()) {
        switch($type) {
            case 'term':
                $fields = $this->terms;
                $table = TERMS;
                break;
            case 'relationship':
                $fields = $this->term_relationships;
                $table = TERM_RELATIONSHIPS;
                break;
            case 'taxonomy':
                $fields = $this->term_taxonomy;
                $table = TERM_TAXONOMY;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getTermById($type, $id) {
        switch($type) {
            case 'term':
                $field_id = 'term_id';
                $fields = $this->terms;
                $table = TERMS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $fields = $this->term_relationships;
                $table = TERM_RELATIONSHIPS;
                break;
            case 'taxonomy':
                $field_id = 'term_taxonomy_id';
                $fields = $this->term_taxonomy;
                $table = TERM_TAXONOMY;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getTermByGid($gid) {
        $params['fields'] = $fields;
        $params['table'] = TERMS;
        $params['where'] = array(
            'term_gid' => $gid
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countTerm($type, $params=array()) {
        switch($type) {
            case 'term':
                $field_id = TERMS.'.term_id';
                $table = TERMS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $table = TERM_RELATIONSHIPS;
                break;
            case 'taxonomy':
                $field_id = 'term_taxonomy_id';
                $table = TERM_TAXONOMY;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateTerm($type, $data, $param) {
        switch($type) {
            case 'term':
                $field_id = 'term_id';
                $table = TERMS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $table = TERM_RELATIONSHIPS;
                break;
            case 'taxonomy':
                $field_id = 'term_taxonomy_id';
                $table = TERM_TAXONOMY;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteTerm($type, $param) {
        switch($type) {
            case 'term':
                $field_id = 'term_id';
                $table = TERMS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $table = TERM_RELATIONSHIPS;
                break;
            case 'taxonomy':
                $field_id = 'term_taxonomy_id';
                $table = TERM_TAXONOMY;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}