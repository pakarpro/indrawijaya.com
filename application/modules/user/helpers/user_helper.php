<?php
/*
 * Function name    :   enc_password
 * Usage            :   formatting password to be encrypted
 * Type             :   string
 * Value            :   
 */
if( !function_exists('enc_password') ) {
    function enc_password($password) {
        $CI = &get_instance();

        $key = read_key(256);
        $key = pack('H*', $key);
        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $iv = read_iv();

        $new_password = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $password, MCRYPT_MODE_CBC, $iv);
        $new_password = $iv . $new_password;
        $new_password = base64_encode($new_password);
        
        return $new_password;
    }
}

if( !function_exists('read_key') ){
    function read_key($length, $pos=0) {
        $return = '';
        
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/key.txt";
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $length);
        $data = unpack("h*", $data);
        $arr = str_split(current($data), 2);
        foreach($arr as $val) {
            $return .= chr(hexdec($val));
        }
        fclose($fp);
        
        return $return;
    }
}

if( !function_exists('read_iv') ){
    function read_iv($pos=0) {
        $return = '';
        
        $lenght = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/iv.txt";
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $lenght);
        fclose($fp);
        
        return $data;    
    }
}

if( !function_exists('count_user') ){
    function count_user($type='all') {
        $CI = &get_instance();
        $CI->load->model('user/User_model');
        
        $param = array();
        switch($type) {
            case 'active':
                $param['where'] = array('user_status' => 1);
                break;
            case 'inactive':
                $param['where'] = array('user_status' => 0);
                break;
        }
        $count = $CI->User_model->countUser('user', $param);
        
        return $count;
    }
}

