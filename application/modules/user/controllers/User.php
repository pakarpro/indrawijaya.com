<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/login';
    private $admin_data = array();
    private $user_validation_config = array(
        array(
            'field'   => 'username',
            'label'   => 'Username',
            'rules'   => 'trim|min_length[5]|max_length[30]'
        ),
        array(
            'field'   => 'email',
            'label'   => 'Email',
            'rules'   => 'trim|required|valid_email'
        ),
        array(
            'field'   => 'name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z0-9\s\.\,]+$/]'
        ),
        array(
            'field'   => 'password',
            'label'   => 'Password',
            'rules'   => 'trim|min_length[8]'
        ),
        array(
            'field'   => 'description',
            'label'   => 'Biographical info',
            'rules'   => 'trim'
        ),
    );
    private $role_validation_config = array(
        array(
            'field'   => 'role_name',
            'label'   => 'Role name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z0-9\s]+$/]'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'min_length' => '%s at least %d characters',
        'max_length' => '%s maximum length is %d characters',
        'regex_match' => '%s does not follow approriate format',
        'is_unique' => '%s is already used',
        'edit_unique' => '%s is already used',
        'valid_email' => '%s is invalid'
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('user');
        $this->load->model('User_model');

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
   
    // getting user info after logging in
    private function _user_info($id_user) {
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'user_status' => $status
            );
            $update = $this->User_model->updateUser('user', $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // updating user_type, for ajax use
    public function changerole() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $user_type = $data['user_role'];
            
            $update_param = array(
                'role' => $user_type
            );
            $update = $this->User_model->updateUser('user', $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // update profile [user], same as edit user
    public function profile() {
        // check session login
        $this->_check_login_session();
        
        //if given ID is not empty and not 0
        $id = $this->admin_data;
        $user = $this->User_model->getUserById('user', $id);
        if( !empty($user) ) {
            $data['admin_data'] = $user;
            if(!empty($_POST)) {
                // get post data
                $post_data = $this->input->post();

                // replacing data on variable $user using posted data
                if( !empty($post_data['description']) ) {
                    $user['description'] = ucfirst($post_data['description']);
                }
                $user['user_email'] = strtolower($post_data['email']);
                $user['name'] = ucwords($post_data['name']);
                $user['role'] = $post_data['user_role'];

                foreach($this->user_validation_config as $k => $item) {
                    switch($item['field']) {
                        case 'email':
                            $item['rules'] = $item['rules'].'|edit_unique['.USERS.'.user_email.user_id.'.$id.']';
                            break;
                    }

                    $this->user_validation_config[$k] = $item;
                }
                
                $this->form_validation->set_rules($this->user_validation_config);
                foreach($this->validation_msg as $key => $msg) {
                    $this->form_validation->set_message($key, $msg);
                }

                if ($this->form_validation->run() == TRUE ) {
                    // update param
                    $update_param = array(
                        'user_email' => $user['user_email'],
                        'name' => $user['name'],
                        'role' => $user['role'],
                        'description' => $user['description']
                    );
                    if( !empty($post_data['password']) ) {
                        $update_param['user_pass'] = enc_password($post_data['password']);
                    }
                    $update = $this->User_model->updateUser('user', $update_param, $id);
                    if( $update == 'SUCCESSFUL' ) {
                        // set notification
                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    Profile has been updated.'.
                                  '</div>';
                        $this->session->set_flashdata('notif', $notif);

                        // redirect to list general info page
                        redirect('panel/profile');
                    }
                } # end of validation
            } # end of post

            $data['data'] = $user;
            $data['user_role'] = $this->User_model->getUser('user-role');
        }

        $this->load->view('profile', $data);
    }
    
    public function index() {
        $this->lists();
    }
    
    // show data [user, user_role]
    public function lists($type='user', $page=1) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        $count_user = $this->User_model->countUser($type);
        if( $count_user > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            // where param
            $where_param['limit'] = array($limit_per_page, $start_row);
                
            if( $count_user > $limit_per_page ) {
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/'.$type.'/list/'),
                    'total_rows'    =>  $count_user,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            if( $type == 'user' ) {
                $data['user_role'] = $this->User_model->getUser('user-role');
            }
            $data['data'] = $this->User_model->getUser($type, $where_param);
        }

        $this->load->view('list-'.$type, $data);
    }
    
    // create new data [user, user_role]
    public function create($type='user') {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();

            // validation
            switch($type) {
                case 'user':
                    $validation_config = $this->user_validation_config;
                    foreach($validation_config as $k => $item) {
                        switch($item['field']) {
                            case 'email':
                                $item['rules'] = $item['rules'].'|is_unique['.USERS.'.user_email]';
                                break;
                            case 'username':
                                $item['rules'] = $item['rules'].'|required|is_unique['.USERS.'.user_login]';
                                break;
                            case 'password':
                                $item['rules'] = $item['rules'].'|required';
                        }

                        $validation_config[$k] = $item;
                    }
                    break;
                case 'user-role':
                    $validation_config = $this->role_validation_config;
                    break;
            }
            
            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                // save param
                switch($type) {
                    case 'user':
                        $save_param = array(
                            'user_login' => strtolower($post_data['username']),
                            'user_pass' => enc_password($post_data['password']),
                            'user_email' => strtolower($post_data['email']),
                            'user_registered' => DATETIME,
                            'name' => ucwords($post_data['name']),
                            'role' => $post_data['user_role']
                        );
                        break;
                    case 'user-role':
                        $save_param = array(
                            'user_role_gid' => generate_gid($post_data['role_name']),
                            'role_name' => ucwords($post_data['role_name'])
                        );
                        break;
                }
                $save = $this->User_model->setNewUser($type, $save_param);
                if( isset($save['id']) ) {
                    $id = $save['id'];

                    // set notification
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                New data has been created.
                              </div>';
                    $this->session->set_flashdata('notif', $notif);

                    // redirect to list general info page
                    redirect('panel/'.$type.'/edit/'.$id);
                }
            }
        }

        if( $type == 'user' ) {
            $data['user_role'] = $this->User_model->getUser('user-role');
        }
        
        // view page (create user or role)
        $this->load->view('create-'.$type, $data);
    }
    
    // update data [user, user-role]
    public function edit($type='user', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $user = $this->User_model->getUserById($type, $id);
            if( !empty($user) ) {
                if(!empty($_POST)) {
                    // get post data
                    $post_data = $this->input->post();
    
                    switch($type) {
                        case 'user':
                            // replacing data on variable $user using posted data
                            $status = 1;
                            if( !isset($post_data['status']) ) {
                                $status = 0;
                            }
                            if( !empty($post_data['description']) ) {
                                $user['description'] = ucfirst($post_data['description']);
                            }
                            $user['user_status'] = $status;
                            $user['user_email'] = strtolower($post_data['email']);
                            $user['name'] = ucwords($post_data['name']);
                            $user['role'] = $post_data['user_role'];
                            break;
                        case 'user-role':
                            $user['role_name'] = ucwords($post_data['role_name']);
                            break;
                    }

                    // validation
                    switch($type) {
                        case 'user':
                            $validation_config = $this->user_validation_config;
                            foreach($validation_config as $k => $item) {
                                switch($item['field']) {
                                    case 'email':
                                        $item['rules'] = $item['rules'].'|edit_unique['.USERS.'.user_email.user_id.'.$id.']';
                                        break;
                                }

                                $validation_config[$k] = $item;
                            }
                            break;
                        case 'user-role':
                            $validation_config = $this->role_validation_config;
                            foreach($validation_config as $k => $item) {
                                switch($item['field']) {
                                    case 'role_name':
                                        $item['rules'] = $item['rules'].'|edit_unique['.USER_ROLE.'.role_name.user_role_id.'.$id.']';
                                        break;
                                }

                                $validation_config[$k] = $item;
                            }
                            break;
                    }

                    $this->form_validation->set_rules($validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        // update param
                        switch($type) {
                            case 'user':
                                $update_param = array(
                                    'user_status' => $user['user_status'],
                                    'user_email' => $user['user_email'],
                                    'name' => $user['name'],
                                    'role' => $user['role'],
                                    'description' => $user['description']
                                );
                                if( !empty($post_data['password']) ) {
                                    $update_param['user_pass'] = enc_password($post_data['password']);
                                }
                                break;
                            case 'user-role':
                                $update_param = array(
                                    'user_role_gid' => generate_gid($user['role_name']),
                                    'role_name' => $user['role_name']
                                );
                                break;
                        }
                        $update = $this->User_model->updateUser($type, $update_param, $id);
                        if( $update == 'SUCCESSFUL' ) {
                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Data has been updated.'.
                                      '</div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/'.$type.'/edit/'.$id);
                        }
                    } # end of validation
                } # end of post
                
                $data['data'] = $user;
                
                if( $type == 'user' ) {
                    $data['user_role'] = $this->User_model->getUser('user-role');
                }
            }
        }

        $this->load->view('edit-'.$type, $data);
    }
    
    // delete data [user, user-role]
    public function delete($type='user', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        if( $data['admin_data']['role'] == 1 ) {
            // provide error message when given ID is empty or 0
            $msg = 'An error occured while deleting '.$type.'. Please try again later.';
            $alertClass = 'alert-warning';
            if( !empty($id) && is_numeric($id) && $id != 0 ) {
                // check if given ID is existing in database
                $user = $this->User_model->getUserById($type, $id);
                // if data is found
                if( !empty($user) ) {
                    $name = $user['name'];
                    if( $type == 'user-role' ) {
                        $name = $user['role_name'];
                    }

                    // run delete action
                    $delete = $this->User_model->deleteUser($type, $id);
                    // if delete was succeed
                    if( $delete == 'SUCCESSFUL' ) {
                        // provide success message
                        $msg = ucfirst(str_replace('-',' ',$type)).' with name <b>'.$name.'</b> has been deleted.';
                        $alertClass = 'alert-success';
                    }
                }
            }
        }
        
        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/'.$type.'/list/');
    }
}