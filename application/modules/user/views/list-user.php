<?php echo call_header('panel', 'All Users'); ?>

<?php echo call_sidebar($admin_data, 'user', 'user'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">USERS</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <?php echo $this->session->flashdata('notif'); ?>

                            <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/user/create'); ?>">CREATE NEW USER</a>
                            <table class="table table-hover table-list">
                                <thead>
                                    <tr>
                                        <th width="50">#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th width="180">Role</th>
                                        <th>Posts</th>
                                        <th width="80">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if( !empty($data) ) {
                                            foreach($data as $k => $item) {
                                                $checked = '';
                                                if( $item['user_status'] == 1 ) {
                                                    $checked = 'checked';
                                                }
                                    ?>
                                        <tr data-id="<?php echo $item['user_id']; ?>">
                                            <td><?php echo $item['user_id']; ?></td>
                                            <td class="text-bold">
                                                <a href="<?php echo site_url('panel/user/edit/'.$item['user_id']); ?>">
                                                    <?php echo $item['name']; ?>
                                                </a>
                                            </td>
                                            <td><?php echo $item['user_login']; ?></td>
                                            <td>
                                                <select name="user_role" id="user_role-<?php echo $item['user_id']; ?>" class="show-tick form-control _cur">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($user_role as $key => $row) {
                                                            $selected = '';
                                                            if( $row['user_role_id'] == $item['role'] ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$row['user_role_id'].'" '.$selected.'>'.$row['role_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </td>
                                            <td><?php echo $item['post_count']; ?></td>
                                            <td>
                                                <?php if($item['user_default'] == 0) { ?>
                                                    <?php if( $item['role'] != 1 ) { ?>
                                                        <div class="switch pull-left">
                                                            <label><input type="checkbox" name="status" class="_cs" data-id="<?php echo $item['user_id']; ?>" <?php echo $checked; ?>><span class="lever"></span></label>
                                                        </div>
                                                    <?php } ?>
                                                    <a href="<?php echo site_url('panel/user/delete/'.$item['user_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this user?');">
                                                        <i class="material-icons">delete</i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                            echo '<tr><td colspan="6">Data not found</td></tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>

                            <?php if( isset($pagination) ) { ?>
                                <!-- pagination -->
                                <div class="row clearfix">
                                    <div class="col-xs-12 text-center">
                                        <?php print_r($pagination); ?>
                                    </div>
                                </div>
                                <!-- /pagination -->
                            <?php } ?>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>
<script>
    // updating user role
    updateUserRole();
    // updating status of selected data
    $(document).on('click', '._cs', function() {
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post('/user/change-status', data);
    });

    function updateUserRole() {
        $(document).on('click', '._cur', function() {
            var id = $(this).parent().parent().attr('data-id');
            var role = $('#user_role-'+id).val();

            var data = {
                "id": id,
                "user_role": role
            }

            $.post('/user/change-role', data);
        });
    }
</script>