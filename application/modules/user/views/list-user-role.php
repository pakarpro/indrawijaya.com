<?php echo call_header('panel', 'User Role'); ?>

<?php echo call_sidebar($admin_data, 'user', 'user-role'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">USER ROLE</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <?php echo $this->session->flashdata('notif'); ?>

                            <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/user-role/create'); ?>">CREATE NEW USER ROLE</a>
                            <table class="table table-hover table-list">
                                <thead>
                                    <tr>
                                        <th width="30">#</th>
                                        <th>Role name</th>
                                        <th width="200">Keyword</th>
                                        <th width="30"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if( !empty($data) ) {
                                            foreach($data as $k => $item) {
                                    ?>
                                        <tr data-id="<?php echo $item['user_role_id']; ?>">
                                            <td><?php echo $k+1; ?></td>
                                            <td class="text-bold">
                                                <a href="<?php echo site_url('panel/user-role/edit/'.$item['user_role_id']); ?>">
                                                    <?php echo $item['role_name']; ?>
                                                </a>
                                            </td>
                                            <td><?php echo $item['user_role_gid']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('panel/user-role/delete/'.$item['user_role_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this user role ?');">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                            echo '<tr><td colspan="4">Data not found</td></tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>

                            <?php if( isset($pagination) ) { ?>
                                <!-- pagination -->
                                <div class="row clearfix">
                                    <div class="col-xs-12 text-center">
                                        <?php print_r($pagination); ?>
                                    </div>
                                </div>
                                <!-- /pagination -->
                            <?php } ?>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>