<?php echo call_header('panel', 'Edit User Role'); ?>

<?php echo call_sidebar($admin_data, 'user', 'user-role'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">EDIT USER ROLE</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <div class="row clearfix">
                                <div class="col-sm-12 col-md-6">
                                    <?php echo $this->session->flashdata('notif'); ?>

                                    <?php if( isset($data) ) { ?>
                                        <form action="<?php echo site_url('panel/user-role/edit/'.$data['user_role_id']); ?>" method="post">
                                            <div class="form-group">
                                                <label>Role name</label>
                                                <?php echo form_error('role_name'); ?>
                                                <div class="form-line">
                                                    <input name="role_name" id="role_name" type="text" class="form-control" value="<?php echo $data['role_name']; ?>" autofocus required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php
                                                    $back_url = site_url('panel/user-role/list');
                                                    if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                                        $back_url = $_SERVER['HTTP_REFERER'];
                                                    }
                                                ?>
                                                <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-r-15">UPDATE</button>
                                                <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                            </div>
                                        </form>                                            
                                    <?php
                                        } else {
                                            echo 'Data not found';
                                        }
                                    ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
