<?php echo call_header('panel', 'My Profile'); ?>

<?php echo call_sidebar($admin_data); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">PROFILE</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/profile'); ?>" method="post">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <div class="form-line">
                                               <?php echo $data['user_login']; ?> 
                                            </div>
                                            <p><small class="rules">Username cannot be changed.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <?php echo form_error('email'); ?>
                                            <div class="form-line">
                                                <input name="email" type="email" class="form-control" value="<?php echo $data['user_email']; ?>" required="required" autofocus>
                                            </div>
                                            <p><small class="rules">For recovery reason, email must be provide in order to recover your account easily.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Name</label>
                                            <?php echo form_error('name'); ?>
                                            <div class="form-line">
                                                <input name="name" type="text" class="form-control" value="<?php echo $data['name']; ?>" required="required">
                                            </div>
                                            <p><small class="rules">Must contain only standard letters, space, dot, or comma.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <?php echo form_error('password'); ?>
                                            <div class="input-group" style="margin-bottom: 0;">
                                                <div class="form-line">
                                                    <input name="password" id="password" type="password" class="form-control">
                                                </div>
                                                <span class="input-group-addon show-hide-pass _l">
                                                    <i class="material-icons">visibility</i>
                                                </span>
                                            </div>
                                            <p><small class="rules">Minimum 8 characters</small></p>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Role</label>
                                            <div class="form-line">
                                                <select name="user_role" class="form-control show-tick">
                                                    <?php
                                                        foreach($user_role as $k => $item) {
                                                            $selected = '';
                                                            if( $item['user_role_id'] == $data['role'] ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['user_role_id'].'" '.$selected.'>'.$item['role_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Biographical Info</label>
                                            <div class="form-line">
                                                <textarea class="form-control" rows="5" name="description"><?php echo $data['description']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-r-15 m-t-15">UPDATE</button>
                                            <a href="<?php echo site_url('panel/user/list'); ?>" class="btn btn-default waves-effect m-t-15">BACK</a>
                                        </div>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<script>
    // show and hide password
    $(document).on('click', '.show-hide-pass', function() {
        var type = $('#password').attr('type');
        console.log(type);
        if( type == 'password') {
            $('#password').attr('type', 'text');
            $(this).children('i').text('visibility_off');
        } else {
            $('#password').attr('type', 'password');
            $(this).children('i').text('visibility');
        }
    });
</script>