<?php echo call_header('panel', 'Create New User'); ?>

<?php echo call_sidebar($admin_data, 'user', 'user'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CREATE NEW USER</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <div class="row clearfix">
                                <div class="col-md-6 col-xs-12">
                                    <form action="<?php echo site_url('panel/user/create'); ?>" method="post">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <?php echo form_error('username'); ?>
                                            <div class="form-line">
                                                <input name="username" type="text" class="form-control" value="<?php echo set_value('username'); ?>" required="required" autofocus>
                                            </div>
                                            <p><small class="rules">Must contain only letters, underscore, or dot. Username cannot be change once you chose it.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <?php echo form_error('email'); ?>
                                            <div class="form-line">
                                                <input name="email" type="email" class="form-control" value="<?php echo set_value('email'); ?>" required="required">
                                            </div>
                                            <p><small class="rules">For recovery reason, email must be provide in order to recover your account easily.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Name</label>
                                            <?php echo form_error('name'); ?>
                                            <div class="form-line">
                                                <input name="name" type="text" class="form-control" value="<?php echo set_value('name'); ?>" required="required">
                                            </div>
                                            <p><small class="rules">Must contain only standard letters, space, dot, or comma.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <?php echo form_error('password'); ?>
                                            <div class="input-group" style="margin-bottom: 0;">
                                                <div class="form-line">
                                                    <input name="password" id="password" type="password" class="form-control" required="required">
                                                </div>
                                                <span class="input-group-addon show-hide-pass _l">
                                                    <i class="material-icons">visibility</i>
                                                </span>
                                            </div>
                                            <p><small class="rules">Minimum 8 characters</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Role</label>
                                            <div class="form-line">
                                                <select name="user_role" class="form-control show-tick">
                                                    <?php
                                                        foreach($user_role as $k => $item) {
                                                            $default = 1;
                                                            if( !empty(set_value('user_role')) ) {
                                                                $default = set_value('user_role');
                                                            }

                                                            $selected = '';
                                                            if( $item['user_role_id'] == $default ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['user_role_id'].'" '.$selected.'>'.$item['role_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">                                        
                                            <input type="submit" class="btn btn-primary m-t-15 waves-effect m-r-15" value="CREATE">
                                            <a href="<?php echo site_url('panel/user/list'); ?>" class="btn btn-default m-t-15 waves-effect">BACK</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<script>
    // show and hide password
    $(document).on('click', '.show-hide-pass', function() {
        var type = $('#password').attr('type');
        console.log(type);
        if( type == 'password') {
            $('#password').attr('type', 'text');
            $(this).children('i').text('visibility_off');
        } else {
            $('#password').attr('type', 'password');
            $(this).children('i').text('visibility');
        }
    });
</script>