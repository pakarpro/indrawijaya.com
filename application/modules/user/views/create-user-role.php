<?php echo call_header('panel', 'Create New User Role'); ?>

<?php echo call_sidebar($admin_data, 'user', 'user-role'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CREATE NEW USER ROLE</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <div class="row clearfix">
                                <div class="col-sm-12 col-md-6">
                                    <?php echo $this->session->flashdata('notif'); ?>

                                    <form action="<?php echo site_url('panel/user-role/create'); ?>" method="post">
                                        <div class="form-group">
                                            <label>Role name</label>
                                            <?php echo form_error('role_name'); ?>
                                            <div class="form-line">
                                                <input name="role_name" id="role_name" type="text" class="form-control" value="<?php echo set_value('role_name'); ?>" autofocus required>
                                            </div>
                                        </div>

                                        <div class="form-group">                                        
                                            <input type="submit" class="btn btn-primary m-t-15 waves-effect m-r-15" value="CREATE">
                                            <a href="<?php echo site_url('panel/user-role/list'); ?>" class="btn btn-default m-t-15 waves-effect">BACK</a>
                                        </div>
                                    </form>                                            
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
