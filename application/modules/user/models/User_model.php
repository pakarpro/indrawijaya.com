<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_model {
    
    public $data;
    public $return = array();
    private $users = array(
        'user_id',
        'name',              // as username
        'role',              // as username
        'description',       // as username
        'user_login',       // as username
        'user_pass',        // as login passpord
        'user_email',       // registered email of user
        'user_registered',  // date when the user created
        'user_status',      // active or inactive
        'user_default',        // total article had been posted
        'post_count'        // total article had been posted
    );
    private $user_role = array(
        'user_role_id',
        'user_role_gid',
        'role_name'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewUser($type, $data) {
        switch($type) {
            case 'user': $table = USERS; break;
            case 'user-role': $table = USER_ROLE; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getUser($type, $params=array()) {
        switch($type) {
            case 'user':
                $fields = $this->users;
                $table = USERS;
                break;
            case 'user-role':
                $fields = $this->user_role;
                $table = USER_ROLE;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getUserById($type, $id) {
        switch($type) {
            case 'user':
                $field_id = 'user_id';
                $fields = $this->users;
                $table = USERS;
                break;
            case 'user-role':
                $field_id = 'user_role_id';
                $fields = $this->user_role;
                $table = USER_ROLE;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countUser($type, $params=array()) {
        switch($type) {
            case 'user':
                $field_id = 'user_id';
                $table = USERS;
                break;
            case 'user-role':
                $field_id = 'user_role_id';
                $table = USER_ROLE;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateUser($type, $data, $id) {
        switch($type) {
            case 'user':
                $field_id = 'user_id';
                $table = USERS;
                break;
            case 'user-role':
                $field_id = 'user_role_id';
                $table = USER_ROLE;
                break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteUser($type, $id) {
        switch($type) {
            case 'user':
                $field_id = 'user_id';
                $table = USERS;
                break;
            case 'user-role':
                $field_id = 'user_role_id';
                $table = USER_ROLE;
                break;
        }
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}