<?php echo call_header('panel', 'Edit Account'); ?>

<?php echo call_sidebar($admin_data, 'social-media', 'account'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">EDIT ACCOUNT</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <div class="row clearfix">
                                <div class="col-sm-12 col-md-6">
                                    <?php echo $this->session->flashdata('notif'); ?>

                                    <?php if( isset($data) ) { ?>
                                        <form action="<?php echo site_url('panel/account/edit/'.$data['account_id']); ?>" method="post">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <div class="switch">
                                                    <?php
                                                        $checked = '';
                                                        if( $data['account_status'] == 1 ) {
                                                            $checked = 'checked';
                                                        }
                                                    ?>
                                                    <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Social Media</label>
                                                <?php echo form_error('social_media'); ?>
                                                <div class="form-line">
                                                    <input type="hidden" name="regex_rules" id="regex_rules" value="<?php echo $data['regex_rules']; ?>">
                                                    <select name="social_media" class="form-control show-tick" id="social_media" required="required">
                                                        <option value="">- Choose -</option>
                                                        <?php
                                                            foreach($social_media as $k => $item) {
                                                                $selected = '';
                                                                if( $item['social_media_id'] == $data['id_social_media'] ) {
                                                                    $selected = 'selected';
                                                                }
                                                                echo '<option value="'.$item['social_media_id'].'" '.$selected.'>'.$item['social_media_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Account Name</label>
                                                <?php
                                                    echo form_error('account_name');
                                                    // additional error message
                                                    if( isset($error_account_name) ) { echo $error_account_name; }
                                                ?>
                                                <div class="form-line">
                                                    <input type="hidden" name="info_rules" id="info_rules" value="<?php echo $data['info_rules']; ?>">
                                                    <input name="account_name" type="text" class="form-control" value="<?php echo $data['account_name']; ?>" autofocus required="required">
                                                </div>
                                                <p><small class="rules"><?php echo $data['info_rules']; ?></small></p>
                                            </div>

                                            <div class="form-group">
                                                <?php
                                                    $back_url = site_url('panel/account/list');
                                                    if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                                        $back_url = $_SERVER['HTTP_REFERER'];
                                                    }
                                                ?>
                                                <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 m-r-15">UPDATE</button>
                                                <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect m-t-15">BACK</a>
                                            </div>
                                        </form>                                            
                                    <?php
                                        } else {
                                            echo 'Data not found';
                                        }
                                    ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    var rules = [];
    var regex;
    var rule;
    
    $(document).on('change', '#social_media', function() {
        var id = $(this).val();

        clearRules();
        $.post('/social-media/get-rule', {social_media_id: id}, function(data){
            data = JSON.parse(data);

            $('small.rules').text(data.info_rules);
            $('#info_rules').val(data.info_rules);
            $('#regex_rules').val(data.regex_rules);
            $('input[name="account_name"]').focus();
        });
    });
    
    function clearRules() {
        rules = [];
    }
</script>