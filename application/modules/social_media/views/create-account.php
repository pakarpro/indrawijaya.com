<?php echo call_header('panel', 'Create New Account'); ?>

<?php echo call_sidebar($admin_data, 'social-media', 'account'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">CREATE NEW ACCOUNT</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <div class="row clearfix">
                                <div class="col-md-6 col-xs-12">
                                    <form action="<?php echo site_url('panel/account/create'); ?>" method="post">
                                        <div class="form-group">
                                            <label>Social Media</label>
                                            <?php echo form_error('social_media'); ?>
                                            <div class="form-line">
                                                <input type="hidden" name="regex_rules" id="regex_rules" value="<?php echo set_value('regex_rules'); ?>">
                                                <select name="social_media" class="form-control show-tick" id="social_media">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($social_media as $k => $item) {
                                                            $selected = '';
                                                            if( $item['social_media_id'] == set_value('social_media') ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['social_media_id'].'" '.$selected.'>'.$item['social_media_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Account Name</label>
                                            <?php
                                                echo form_error('account_name');
                                                // additional error message
                                                if( isset($error_account_name) ) { echo $error_account_name; }
                                            ?>
                                            <div class="form-line">
                                                <input type="hidden" name="info_rules" id="info_rules" value="<?php echo set_value('info_rules'); ?>">
                                                <input name="account_name" type="text" class="form-control" value="<?php echo set_value('account_name'); ?>" autofocus required="required">
                                            </div>
                                            <p><small class="rules"><?php echo set_value('info_rules'); ?></small></p>
                                        </div>

                                        <div class="form-group">                                        
                                            <input type="submit" class="btn btn-primary m-t-15 waves-effect m-r-15" value="CREATE">
                                            <a href="<?php echo site_url('panel/account/list'); ?>" class="btn btn-default m-t-15 waves-effect">BACK</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    var rules = [];
    var regex;
    var rule;
    
    $(document).on('change', '#social_media', function() {
        var id = $(this).val();

        clearRules();
        $.post('/social-media/get-rule', {social_media_id: id}, function(data){
            data = JSON.parse(data);

            $('small.rules').text(data.info_rules);
            $('#info_rules').val(data.info_rules);
            $('#regex_rules').val(data.regex_rules);
            $('input[name="account_name"]').focus();
//            console.log(data);
        });
    });
    
    function clearRules() {
        rules = [];
    }
</script>