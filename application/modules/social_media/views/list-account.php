<?php echo call_header('panel', 'Social Media'); ?>

<?php echo call_sidebar($admin_data, 'social-media', 'account'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="card-inside-title">SOCIAL MEDIA</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php if( $admin_data['role'] == 1 ) { ?>
                            <?php echo $this->session->flashdata('notif'); ?>

                            <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/account/create'); ?>">CREATE NEW ACCOUNT</a>
                            <table class="table table-hover table-list">
                                <thead>
                                    <tr>
                                        <th width="50">#</th>
                                        <th width="120">Social Media</th>
                                        <th>Account Name</th>
                                        <th width="120"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if( !empty($data) ) {
                                            foreach($data as $k => $item) {
                                                $checked = '';
                                                if( $item['account_status'] == 1 ) {
                                                    $checked = 'checked';
                                                }
                                    ?>
                                        <tr data-id="<?php echo $item['account_id']; ?>">
                                            <td><?php echo $item['account_id']; ?></td>
                                            <td><?php echo $item['social_media_name']; ?></td>
                                            <td class="text-bold">
                                                <a href="<?php echo site_url('panel/account/edit/'.$item['account_id']); ?>">
                                                    <?php echo $item['account_name']; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="switch pull-left">
                                                    <label><input type="checkbox" name="status" class="_cs" data-id="<?php echo $item['account_id']; ?>" <?php echo $checked; ?>><span class="lever"></span></label>
                                                </div>
                                                <a target="_blank" href="<?php echo $item['social_media_url'].'/'.$item['account_name']; ?>" class="waves-effect" title="<?php echo $item['account_name']; ?>">
                                                    <i class="material-icons">launch</i>
                                                </a>
                                                <a href="<?php echo site_url('panel/account/delete/'.$item['account_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this account ?');">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                            echo '<tr><td colspan="5">Data not found</td></tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>

                            <?php if( isset($pagination) ) { ?>
                                <!-- pagination -->
                                <div class="row clearfix">
                                    <div class="col-xs-12 text-center">
                                        <?php print_r($pagination); ?>
                                    </div>
                                </div>
                                <!-- /pagination -->
                            <?php } ?>
                        <?php } else { ?>
                            <p>You are not allowed to enter this page, because you are not an Administrator. Thank you.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>

<script>
    // updating status of selected data
    $(document).on('click', '._cs', function() {
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post('/account/change-status', data);
    });
</script>