<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social_media extends CI_Controller {
    
    private $CI;
    private $access_token = '';
    private $redirect = 'panel/login';
    private $admin_data = array();
    private $account_validation_config = array(
        array(
            'field'   => 'social_media',
            'label'   => 'Social media',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'account_name',
            'label'   => 'Account name',
            'rules'   => 'trim|required'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'regex_match' => '%s does not follow approriate format'
    );
//    private $instagram_credential = array(
//        'client_id' => 'd86d7dc2ae0d4867912f63fbf7e466e5',
//        'client_secret' => 'cec00df1da174168aa7522e1b478d4b4'
//    );

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('social_media');
        $this->load->model('Social_media_model');
        
        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // getting user info after logging in
    private function _user_info($id_user) {
        $this->load->model('user/User_model');
        $user_info = $this->User_model->getUserById('user', $id_user);
        
        return $user_info;
    }
   
    private function _get_credential($type) {
        $createitional_parameters['where'] = array(
            'source' => $type,
            'credential_status' => 1
        );
        $credential = $this->Social_media_model->getSocialMedia(SOCIAL_MEDIA_CREDENTIALS, $createitional_parameters);
        $credential = array_shift($credential);
        
        return $credential;
    }
    
    // for getting access token from instagram
    public function get_access_token($type='instagram') {
        $credential = $this->_get_credential($type);
        $credential_id = $credential['credential_id'];
        $client_id = $credential['client_id_key'];
        
        $redirect_url = strtolower(site_url(__CLASS__.'/'.__FUNCTION__));
        $authorization_url = 'https://api.instagram.com/oauth/authorize/?client_id='.$client_id.'&redirect_uri='.$redirect_url.'&response_type=code';
        $access_token_url = 'https://api.instagram.com/oauth/access_token';
        
        $code = $this->input->get('code');
        if( isset($code) && !empty($code) ) {
            $client_secret = $credential['client_secret_key'];
            $required_parameters = array(
                'client_id'     => $client_id,
                'client_secret' => $client_secret,
                'grant_type'    => 'authorization_code',
                'redirect_uri'  => $redirect_url,
                'code'          => $code
            );
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $access_token_url); // uri
            curl_setopt($ch, CURLOPT_POST, true); // POST
            curl_setopt($ch, CURLOPT_POSTFIELDS, $required_parameters); // POST DATA
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
            curl_setopt($ch, CURLOPT_HEADER, 0); // RETURN HEADER false
            curl_setopt($ch, CURLOPT_NOBODY, 0); // NO RETURN BODY false / we need the body to return
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // VERIFY SSL HOST false
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
            $result = json_decode(curl_exec($ch)); // execute curl
            
            // update credential by createing access token code
            if( isset($result->access_token) ) {
                $update_param = array(
                    'access_token' => $result->access_token
                );
                $where = array('credential_id' => $credential_id);
                $this->Social_media_model->updateSocialMedia(SOCIAL_MEDIA_CREDENTIALS, $update_param, $where);
            }
        } else {
            redirect($authorization_url);
        }
    }
    
    public function get_media($type='instagram') {
        if( $type == 'instagram' ) {
            $credential = $this->_get_credential($type);
            $access_token = $credential['access_token'];
            $user_endpoints_url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token='.$access_token.'&count=20';

            $media = file_get_contents($user_endpoints_url);
            $media = json_decode($media, true, 512);
            $data = $media['data'];

            if( !empty($data) ) {
//                print_r($data); exit;
                $this->load->helper('uploads/uploads');
                foreach($data as $key => $item) {
                    $date_activity = gmdate('Y-m-d H:i:s', $item['created_time']);
                    $image_url = $item['images']['standard_resolution']['url'];
                    $image_name = pathinfo($image_url, PATHINFO_BASENAME);
                    
                    // check if activities already exist or no
                    $count_activity_param['where'] = array(
                        'date_activity' => $date_activity,
                        'activity_image' => $image_name
                    );
                    $count_activity = $this->Social_media_model->countSocialMedia(ACTIVITIES, $count_activity_param);
                    if( $count_activity == 0 ) {
                        $image_text = $item['caption']['text'];
                        if( !isset($image_text) ) {
                            $image_text = '';
                        }

                        $activity_parameter = array(
                            'date_create' => DATETIME,
                            'date_activity' => $date_activity,
                            'source' => 'instagram',
                            'activity_caption' => $image_text,
                            'activity_status' => 1
                        );
                        $save = $this->Social_media_model->setNewSocialMedia(ACTIVITIES, $activity_parameter);
                        if( isset($save['id']) ) {
                            $activity_id = $save['id'];
                            $temp_image = $this->temp_path.$image_name;

                            if( !file_exists($this->temp_path) ) {
                                mkdir($this->temp_path);
                                chmod($this->temp_path, 0777);
                            }
                            copy($image_url, $temp_image);

                            // create directory for image
                            $img_path = $this->img_path.$activity_id.'/';
                            if( !file_exists($img_path) ) {
                                mkdir($img_path);
                                chmod($img_path, 0777);
                            }

                            // get image config
                            $config = get_uploads_config('activity');
                            $thumb_where_param['where'] = array(
                                'config_id' => $config['config_id']
                            );
                            $thumb_config = get_thumb_config($thumb_where_param);
                            crop_image($config['min_width'], $config['min_height'], $temp_image, $img_path, $image_name);
                            if( !empty($thumb_config) ) {
                                foreach($thumb_config as $key => $val) {
                                    crop_image($val['width'], $val['height'], $temp_image, $img_path, $val['prefix'].$image_name);
                                }
                            }

                            // update image name in database
                            $update_param = array('activity_image' => $image_name);
                            $where_param = array('activity_id' => $activity_id);
                            $this->Social_media_model->updateSocialMedia(ACTIVITIES, $update_param, $where_param);

                            unlink($temp_image);
                        }
                    }
                }
            }
        } else {
            echo 'null';
        }
    }
    
    // format account rules
    private function _format_account_rules($rules) {
        $rules = unserialize($rules);
        
        // regex
        $regex = implode("", $rules);
        $data['regex_rules'] = 'regex_match[/^['.$regex.']+$/]';
        
        // info rules
        foreach($rules as $rule) {
            switch($rule) {
                case "a-z": $rule = 'letters'; break;
                case "0-9": $rule = 'numbers'; break;
                case "_": $rule = 'underscore'; break;
                case ".": $rule = 'dot'; break;
            }

            $formatted_rules[] = ' '.$rule;
        }
        $data['info_rules'] = 'Must contain at least'.implode(',', $formatted_rules);
        
        return $data;
    }

    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'account_modified' => DATETIME,
                'account_status' => $status
            );
            $update = $this->Social_media_model->updateSocialMedia('account', $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // get rules of social_media account name, for ajax use
    public function get_rule() {
        $rule = array();
        
        $social_media_id = $this->input->post('social_media_id');
        if( !empty($social_media_id) && is_numeric($social_media_id) && $social_media_id != 0 ) {
            $social_media = $this->Social_media_model->getSocialMediaById('social-media', $social_media_id);
            if( !empty($social_media) ) {
                $rule = $social_media['social_media_rule'];
                $rule = $this->_format_account_rules($rule);
            }
        }
        
        echo json_encode($rule);
    }
    
    public function index() {
        $this->lists();
    }
    
    // list all social_media account
    public function lists($type='account', $page=1) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        $count_social_media_account = $this->Social_media_model->countSocialMedia($type);
        if( $count_social_media_account > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            // where param
            $where_param['fields'] = array(
                SOCIAL_MEDIA_ACCOUNT.'.account_id',
                SOCIAL_MEDIA_ACCOUNT.'.account_name',
                SOCIAL_MEDIA_ACCOUNT.'.account_status',
                SOCIAL_MEDIA.'.social_media_gid',
                SOCIAL_MEDIA.'.social_media_url',
                SOCIAL_MEDIA.'.social_media_name'
            );
            $where_param['join'] = array(
                SOCIAL_MEDIA => SOCIAL_MEDIA.'.social_media_id = '.SOCIAL_MEDIA_ACCOUNT.'.id_social_media'
            );
            $where_param['limit'] = array($limit_per_page, $start_row);

            if( $count_social_media_account > $limit_per_page ) {
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/socialmedia'),
                    'total_rows'    =>  $count_social_media_account,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            $data['data'] = $this->Social_media_model->getSocialMedia($type, $where_param);
        }
        
        $this->load->view('list-'.$type, $data);
    }
    
    // save new socialmedia data
    public function create($type='account') {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);

        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            
            switch($type) {
                case 'account':
                    $regex_rules = $post_data['regex_rules'];

                    // validation
                    $validation_config = $this->account_validation_config;
                    foreach($validation_config as $k => $item) {
                        if( $item['field'] == 'account_name' ) {
                            $new_rules = $item['rules'].'|'.$regex_rules;
                            $item['rules'] = $new_rules;
                        }
                        $validation_config[$k] = $item;
                    }
                    break;
                case 'credential':
                    break;
                case 'social-media':
                    break;
            }
            
            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                switch($type) {
                    case 'account':
                        $id_social_media = $post_data['social_media'];
                        $account_name = $post_data['account_name'];

                        // checking existing account based on selected social_media type
                        $check_param['where'] = array(
                            'id_social_media' => $id_social_media,
                            'account_name' => $account_name
                        );
                        $check = $this->Social_media_model->countSocialMedia($type, $check_param);
                        if( $check == 1 ) {
                            $error_account_name = 'Account name is already exist';
                            $data['error_account_name'] = error_msg_generator($error_account_name);
                        } else {
                            // save parameter
                            $save_param = array(
                                'id_social_media' => $id_social_media,
                                'account_registered' => DATETIME,
                                'account_name' => $account_name
                            );
                        }
                        break;
                    case 'credential':
                        break;
                    case 'social-media':
                        break;
                }
                if( isset($save_param) ) {
                    $save = $this->Social_media_model->setNewSocialMedia($type, $save_param);
                    if( isset($save['id']) ) {
                        $id = $save['id'];

                        // set notification
                        $subject = str_replace('-', ' ', $type);
                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    New '.$subject.' has been created.
                                  </div>';
                        $this->session->set_flashdata('notif', $notif);

                        // redirect to list general info page
                        redirect('panel/'.$type.'/edit/'.$id);
                    }
                }
            }
        }

        if( $type == 'account' ) {
            $where_param['fields'] = array('social_media_id','social_media_name');
            $where_param['where'] = array('social_media_status' => 1);
            $data['social_media'] = $this->Social_media_model->getSocialMedia('social-media', $where_param);
        }
        
        $this->load->view('create-'.$type, $data);
    }
    
    // update socialmedia data, with image or not
    public function edit($type='account', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            switch ($type) {
                case 'account':
                    $param = array(
                        'fields' => array(
                            SOCIAL_MEDIA_ACCOUNT.'.account_id',
                            SOCIAL_MEDIA_ACCOUNT.'.id_social_media',
                            SOCIAL_MEDIA_ACCOUNT.'.account_name',
                            SOCIAL_MEDIA_ACCOUNT.'.account_status',
                            SOCIAL_MEDIA.'.social_media_rule'
                        ),
                        'join' => array(
                            SOCIAL_MEDIA => SOCIAL_MEDIA.'.social_media_id = '.SOCIAL_MEDIA_ACCOUNT.'.id_social_media'
                        ),
                        'where' => array(
                            SOCIAL_MEDIA_ACCOUNT.'.account_id' => $id
                        )
                    );
                    break;
                case 'social-media':
                    break;
                case 'credential':
                    break;
            }
            $detail = $this->Social_media_model->getSocialMedia($type, $param);
            if( !empty($detail) ) {
                $detail = array_shift($detail);
                
                if( $type == 'account' ) {
                    $rules = $this->_format_account_rules($detail['social_media_rule']);
                    $detail = array_merge($detail, $rules);
                    
                    $where_param['fields'] = array('social_media_id','social_media_name');
                    $where_param['where'] = array('social_media_status' => 1);
                    $data['social_media'] = $this->Social_media_model->getSocialMedia('social-media', $where_param);
                }
                
                if(!empty($_POST)) {
                    $post_data = $this->input->post();
                    $regex_rules = $post_data['regex_rules'];
                    
                    switch($type) {
                        case 'account':
                            // replacing data on variable $user using posted data
                            $status = 1;
                            if( !isset($post_data['status']) ) {
                                $status = 0;
                            }
                            $detail['id_social_media'] = $post_data['social_media'];
                            $detail['account_status'] = $status;
                            $detail['account_name'] = $post_data['account_name'];
                            $detail['regex_rules'] = $post_data['regex_rules'];
                            $detail['info_rules'] = $post_data['info_rules'];

                            // validation
                            $validation_config = $this->account_validation_config;
                            foreach($validation_config as $k => $item) {
                                if( $item['field'] == 'account_name' ) {
                                    $new_rules = $item['rules'].'|'.$regex_rules;
                                    $item['rules'] = $new_rules;
                                }
                                $validation_config[$k] = $item;
                            }
                            break;
                        case 'credential':
                            break;
                        case 'social-media':
                            break;
                    }
                    $this->form_validation->set_rules($validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        switch($type) {
                            case 'account':
                                $id_social_media = $detail['id_social_media'];
                                $account_name = $detail['account_name'];

                                // checking existing account based on selected social_media type
                                $check_param['where'] = array(
                                    'id_social_media' => $id_social_media,
                                    'account_name' => $account_name,
                                    'account_id != ' => $id
                                );
                                $check = $this->Social_media_model->countSocialMedia('account', $check_param);
                                if( $check == 1 ) {
                                    $error_account_name = 'Account name is already exist';
                                    $data['error_account_name'] = error_msg_generator($error_account_name);
                                } else {
                                    // update parameter
                                    $update_param = array(
                                        'id_social_media' => $id_social_media,
                                        'account_modified' => DATETIME,
                                        'account_name' => $account_name,
                                        'account_status' => $status,
                                    );
                                }
                                break;
                            case 'credential':
                                break;
                            case 'social-media':
                                break;
                        }

                        if( isset($update_param) ) {
                            $update = $this->Social_media_model->updateSocialMedia($type, $update_param, $id);
                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $subject = ucfirst(str_replace('-', ' ', $type));
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                            .$subject.'has been updated.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/'.$type.'/edit/'.$id);
                            }
                        }
                    } # end of validation
                } # end of post
                
                $data['data'] = $detail;
            }
        }
        
        $this->load->view('edit-'.$type, $data);
    }
    
    // delete socialmedia data, include of image
    public function delete($type='account', $id) {
        // check session login
        $this->_check_login_session();
        $id_user = $this->admin_data;
        $data['admin_data'] = $this->_user_info($id_user);
        
        if( $data['admin_data']['role'] == 1 ) {
            // provide error message when given ID is empty or 0
            $subject = str_replace('-', ' ', $type);
            $msg = 'An error occured while deleting '.$subject.'. Please try again later.';
            $alertClass = 'alert-warning';
            if( !empty($id) && is_numeric($id) && $id != 0 ) {
                // check if given ID is existing in database
                $detail = $this->Social_media_model->getSocialMediaById($type, $id);
                // if data is found
                if( !empty($detail) ) {
                    switch($type) {
                        case 'account':
                            // get detail of social_media type
                            $id_social_media = $detail['id_social_media'];
                            $sosmed_type = $this->Social_media_model->getSocialMediaById('social-media', $id_social_media);
                            break;
                        case 'credential':
                            break;
                        case 'social-media':
                            break;
                    }

                    // run delete action
                    $delete = $this->Social_media_model->deleteSocialMedia($type, $id);
                    // if delete was succeed
                    if( $delete == 'SUCCESSFUL' ) {
                        // provide success message
                        switch($type) {
                            case 'account':
                                $msg = $sosmed_type['social_media_name'].' account with name <b>'.$detail['account_name'].'</b> has been deleted.';
                                break;
                            case 'credential':
                                break;
                            case 'social-media':
                                break;
                        }

                        $alertClass = 'alert-success';
                    }
                }
            }
        }
        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/'.$type.'/list');
    }
}