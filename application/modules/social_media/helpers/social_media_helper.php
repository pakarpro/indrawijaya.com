<?php

/* 
 * Name             : count_social_media
 * Parameters       : -
 * Functionality    : count all social media data
 */
if(!function_exists('count_social_media')){
    function count_social_media() {
        $CI = &get_instance();
        
        $CI->load->model('social_media/Social_media_model');
        $count = new Social_media_model();
        $data = $count->countSocialMedia();
        
        return $data;
    }
}

if( !function_exists('get_social_account') ) {
    function get_social_account($type='') {
        $CI = &get_instance();
        $CI->load->model('social_media/Social_media_model');
        
        $param['fields'] = array(
            SOCIAL_MEDIA.'.social_media_gid',
            SOCIAL_MEDIA.'.social_media_url',
            SOCIAL_MEDIA_ACCOUNT.'.account_name'
        );
        $param['join'] = array(
            SOCIAL_MEDIA=> SOCIAL_MEDIA.'.social_media_id = '.SOCIAL_MEDIA_ACCOUNT.'.id_social_media'
        );
        if( !empty($type) ) {
            switch($type) {
                case 'active':
                    $where['where'] = array('account_status' => 1);
                    break;
                case 'inactive':
                    $where['where'] = array('account_status' => 0);
                    break;
            }
        }
        $account = $CI->Social_media_model->getSocialMedia('account', $param);
        
        return $account;
    }
}