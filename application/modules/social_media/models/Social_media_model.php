<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Social_media_model extends CI_model {
    
    public $data;
    public $return = array();
    private $social_media = array(
        'social_media_id',
        'social_media_gid',
        'social_media_name',
        'social_media_url',
        'social_media_rule',
        'social_media_status'
    );
    private $social_media_account = array(
        'account_id',
        'id_social_media',
        'account_registered',
        'account_modified',
        'account_name',
        'account_status'
    );
    private $social_media_credentials = array(
        'credential_id',
        'client_id_key',
        'client_secret_key',
        'access_token'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewSocialMedia($type, $data) {
        switch($type) {
            case 'social-media': $table = SOCIAL_MEDIA; break;
            case 'account': $table = SOCIAL_MEDIA_ACCOUNT; break;
            case 'credential': $table = SOCIAL_MEDIA_CREDENTIALS; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getSocialMedia($type, $params=array()) {
        switch($type) {
            case 'social-media':
                $fields = $this->social_media;
                $table = SOCIAL_MEDIA;
                break;
            case 'account':
                $fields = $this->social_media_account;
                $table = SOCIAL_MEDIA_ACCOUNT;
                break;
            case 'credential':
                $fields = $this->social_media_credentials;
                $table = SOCIAL_MEDIA_CREDENTIALS;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getSocialMediaById($type, $id) {
        switch($type) {
            case 'social-media':
                $field_id = 'social_media_id';
                $fields = $this->social_media;
                $table = SOCIAL_MEDIA;
                break;
            case 'account':
                $field_id = 'account_id';
                $fields = $this->social_media_account;
                $table = SOCIAL_MEDIA_ACCOUNT;
                break;
            case 'credential':
                $field_id = 'credential_id';
                $fields = $this->social_media_credentials;
                $table = SOCIAL_MEDIA_CREDENTIALS;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countSocialMedia($type, $params=array()) {
        switch($type) {
            case 'social-media':
                $field_id = 'social_media_id';
                $table = SOCIAL_MEDIA;
                break;
            case 'account':
                $field_id = 'account_id';
                $table = SOCIAL_MEDIA_ACCOUNT;
                break;
            case 'credential':
                $field_id = 'credential_id';
                $table = SOCIAL_MEDIA_CREDENTIALS;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateSocialMedia($type, $data, $param) {
        switch($type) {
            case 'social-media':
                $field_id = 'social_media_id';
                $table = SOCIAL_MEDIA;
                break;
            case 'account':
                $field_id = 'account_id';
                $table = SOCIAL_MEDIA_ACCOUNT;
                break;
            case 'credential':
                $field_id = 'credential_id';
                $table = SOCIAL_MEDIA_CREDENTIALS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteSocialMedia($type, $id) {
        switch($type) {
            case 'social-media':
                $field_id = 'social_media_id';
                $table = SOCIAL_MEDIA;
                break;
            case 'account':
                $field_id = 'account_id';
                $table = SOCIAL_MEDIA_ACCOUNT;
                break;
            case 'credential':
                $field_id = 'credential_id';
                $table = SOCIAL_MEDIA_CREDENTIALS;
                break;
        }
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}