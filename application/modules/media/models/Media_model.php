<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Media_model extends CI_model {
    
    public $data;
    public $return = array();
    private $medias = array(
        'media_id',
        'media_type',
        'media_date',
        'media_modified',
        'media_title',
        'media_caption',
        'media_alt_text',
    );
    private $media_relationships = array(
        'rel_id',
        'object_id',
        'media_id'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function setNewMedia($type, $data) {
        switch($type) {
            case 'media': $table = MEDIAS; break;
            case 'relationship': $table = MEDIA_RELATIONSHIPS; break;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getMedia($type, $params=array()) {
        switch($type) {
            case 'media':
                $fields = $this->medias;
                $table = MEDIAS;
                break;
            case 'relationship':
                $fields = $this->media_relationships;
                $table = MEDIA_RELATIONSHIPS;
                break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getMediaById($type, $id) {
        switch($type) {
            case 'media':
                $field_id = 'media_id';
                $fields = $this->medias;
                $table = MEDIAS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $fields = $this->media_relationships;
                $table = MEDIA_RELATIONSHIPS;
                break;
        }
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            $field_id => $id
        );
        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countMedia($type, $params=array()) {
        switch($type) {
            case 'media':
                $field_id = MEDIAS.'.media_id';
                $table = MEDIAS;
                break;
            case 'relationship':
                $field_id = MEDIA_RELATIONSHIPS.'.rel_id';
                $table = MEDIA_RELATIONSHIPS;
                break;
        }
        $params['fields'] = $field_id;
        $params['table'] = $table;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateMedia($type, $data, $param) {
        switch($type) {
            case 'media':
                $field_id = 'media_id';
                $table = MEDIAS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $table = MEDIA_RELATIONSHIPS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteMedia($type, $param) {
        switch($type) {
            case 'media':
                $field_id = 'media_id';
                $table = MEDIAS;
                break;
            case 'relationship':
                $field_id = 'rel_id';
                $table = MEDIA_RELATIONSHIPS;
                break;
        }
        $where = array($field_id => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}