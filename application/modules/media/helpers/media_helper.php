<?php
if( !function_exists('get_media_relationship') ){
    function get_media_relationship($type='category', $post_id) {
        $CI = &get_instance();
        $CI->load->model('media/Term_model');
        
        $where['fields'] = array(
            MEDIAS.'.media_gid',
            MEDIAS.'.media_name',
            MEDIA_TAXONOMY.'.media_taxonomy_id'
        );
        $where['join'] = array(
            MEDIA_TAXONOMY => MEDIA_TAXONOMY.'.media_id = '.MEDIAS.'.media_id',
            MEDIA_RELATIONSHIPS => MEDIA_RELATIONSHIPS.'.media_taxonomy_id = '.MEDIA_TAXONOMY.'.media_taxonomy_id',
        );
        $where['where'] = array(
            MEDIA_RELATIONSHIPS.'.object_id' => $post_id,
            MEDIA_TAXONOMY.'.taxonomy' => $type
        );
        $medias = $CI->Term_model->getTerm('media', $where);
        
        return $medias;
    }
}

if( !function_exists('get_media_list') ){
    function get_media_list($type='category') {
        $CI = &get_instance();
        $CI->load->model('media/Term_model');
        
        $where['fields'] = array(
            MEDIAS.'.media_name',
            MEDIA_TAXONOMY.'.media_taxonomy_id',
        );
        $where['join'] = array(
            MEDIA_TAXONOMY => MEDIA_TAXONOMY.'.media_id = '.MEDIAS.'.media_id',
        );
        $where['where'] = array(
            MEDIA_TAXONOMY.'.taxonomy' => $type
        );
        $medias = $CI->Term_model->getTerm('media', $where);
        
        return $medias;
    }
}
