<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller {
    
    public $CI;
    public $redirect = 'dashboard';
    public $user_data = array();

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('area');
        $this->load->model('Area_model');
        
        if( $this->session->userdata('logged_in') ) {
            $this->user_data = $this->session->userdata('logged_in');
        }
    }

    public function get_list($type='province') {
        $return['data'] = array();
        $area_id = $this->input->post('id');
        
        if( !empty($area_id) && is_numeric($area_id) ) {
            switch($type) {
                case 'city':
                case 'district':
                    $where['where'] = array('parent' => $area_id);
                    $where['order_by'] = array('area_name' => 'ASC');
                    $data = $this->Area_model->getArea($type, $where);

                    $return['data'] = $data;
                    break;
            }
        }
        
        echo json_encode($return['data']);
    }
}