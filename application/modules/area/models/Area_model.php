<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Area_model extends CI_model {
    
    private $data;
    private $return = array();
    private $area = array(
        'area_id',
        'area_gid',
        'area_name',
        'area_taxonomy',
        'parent',
        'count'
    );

    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
//    public function saveArea($data) {
//        $params['table'] = AREA;
//        $params['data'] = $data;
//        
//        $this->return = $this->data->set($params);
//        
//        return $this->return;
//    }
    
#==================================== READ ====================================#
    public function countArea($taxonomy, $params=array()) {
        switch($taxonomy) {
            case 'province': $field_id = '';
            case 'province': $field_id = '';
                break;
        }
        $params['fields'] = 'area_id';
        $params['table'] = AREA;
        $params['count'] = TRUE;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getArea($taxonomy, $params=array()) {
        $where = array(
            'area_taxonomy' => $taxonomy
        );
        if( isset($params['where']) && !empty($params['where']) ) {
            $where = array_merge($where, $params['where']);
        }
        $fields = $this->area;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = AREA;
        $params['where'] = $where;
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getAreaByGid($taxonomy, $gid) {
        $fields = $this->area;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = AREA;
        $params['where'] = array(
            'area_gid' => $gid,
            'area_taxonomy' => $taxonomy
        );
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getAreaById($taxonomy, $id) {
        $fields = $this->area;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = AREA;
        $params['where'] = array(
            'area_id' => $id,
            'area_taxonomy' => $taxonomy
        );
        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateArea($taxonomy, $data, $param) {
        $where = array('area_id' => $param);
        if( !empty($param) && is_array($param) ) {
            $where = array(
                'area_taxonomy' => $taxonomy
            );
            $where = array_merge($where, $param);
        }
        $params['table'] = AREA;
        $params['data'] = $data;
        $params['where'] = $where;
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteArea($taxonomy, $id) {
        $params['table'] = AREA;
        $params['where'] = array(
            'area_id' => $id,
            'area_taxonomy' => $taxonomy,
        );
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}