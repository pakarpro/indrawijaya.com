<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# routing URL for main
$route['biography'] = 'main/biography';
$route['contact'] = 'main/contact';
$route['posts'] = 'article/posts';
$route['posts/(:any)'] = 'article/posts/$1';
$route['category'] = 'article/terms/category';
$route['category/(:any)'] = 'article/terms/category/$1';
$route['category/([a-z0-9\-]+)/(:num)'] = 'article/terms/category/$1/$2';
$route['tag'] = 'article/terms/tag';
$route['tag/(:any)'] = 'article/terms/tag/$1';
$route['tag/([a-z0-9\-]+)/(:num)'] = 'article/terms/tag/$1/$2';

# routing URL for panel
# slider
//$route['panel/slider'] = 'slider/lists';
//$route['panel/slider/changestatus'] = 'slider/changestatus';
//$route['panel/slider/(:num)'] = 'slider/lists/$1';
//$route['panel/slider/add'] = 'slider/add';
//$route['panel/slider/detail/(:num)'] = 'slider/detail/$1';
//$route['panel/slider/delete/(:num)'] = 'slider/delete/$1';

# social-media, done
$route['panel/account/list'] = 'social_media/lists/account';
$route['panel/account/list/(:num)'] = 'social_media/lists/account/$1';
$route['panel/account/create'] = 'social_media/create/account';
$route['panel/account/edit/(:num)'] = 'social_media/edit/account/$1';
$route['panel/account/delete/(:num)'] = 'social_media/delete/account/$1';
$route['account/change-status'] = 'social_media/changestatus';
$route['social-media/get-rule'] = 'social_media/get_rule';

# user, done
$route['panel/user/list'] = 'user/lists/user';
$route['panel/user/list/(:num)'] = 'user/lists/user/$1';
$route['panel/user/create'] = 'user/create/user';
$route['panel/user/edit/(:num)'] = 'user/edit/user/$1';
$route['panel/user/delete/(:num)'] = 'user/delete/user/$1';
$route['panel/user-role/list'] = 'user/lists/user-role';
$route['panel/user-role/list/(:num)'] = 'user/lists/user-role/$1';
$route['panel/user-role/create'] = 'user/create/user-role';
$route['panel/user-role/edit/(:num)'] = 'user/edit/user-role/$1';
$route['panel/user-role/delete/(:num)'] = 'user/delete/user-role/$1';
$route['panel/profile'] = 'user/profile';        
$route['user/change-role'] = 'user/changerole';        // ajax use
$route['user/change-status'] = 'user/changestatus';    // ajax use

# mail, done
$route['panel/mail/list'] = 'mail/lists';
$route['panel/mail/list/(:num)'] = 'mail/lists/$1';
$route['panel/mail/view/(:num)'] = 'mail/view/$1';
$route['panel/mail/delete/(:num)'] = 'mail/delete/$1';

# posts (article)
$route['panel/article/list'] = 'article/lists';
$route['panel/article/list/(:num)'] = 'article/lists/$1';
$route['panel/article/create'] = 'article/create';
$route['panel/article/edit/(:num)'] = 'article/edit/$1';
$route['panel/article/delete/(:num)'] = 'article/delete/$1';
$route['panel/article/change-status'] = 'article/changestatus';

# term, done
$route['panel/term/category/list'] = 'term/lists/category';
$route['panel/term/category/list/(:num)'] = 'term/lists/category/$1';
$route['panel/term/category/create'] = 'term/create/category';
$route['panel/term/category/edit'] = 'term/edit/category/$1';
$route['panel/term/category/edit/(:num)'] = 'term/edit/category/$1';
$route['panel/term/category/delete'] = 'term/delete/category/$1';
$route['panel/term/category/delete/(:num)'] = 'term/delete/category/$1';
$route['panel/term/tag/list'] = 'term/lists/tag';
$route['panel/term/tag/list/(:num)'] = 'term/lists/tag/$1';
$route['panel/term/tag/create'] = 'term/create/tag';
$route['panel/term/tag/edit'] = 'term/edit/tag/$1';
$route['panel/term/tag/edit/(:num)'] = 'term/edit/tag/$1';
$route['panel/term/tag/delete'] = 'term/delete/tag/$1';
$route['panel/term/tag/delete/(:num)'] = 'term/delete/tag/$1';
$route['panel/term/get-tag'] = 'term/get_tag';

# area
$route['area/get-province'] = 'area/get_list/province';
$route['area/get-city'] = 'area/get_list/city';
$route['area/get-district'] = 'area/get_list/district';

# biography
$route['panel/biograph'] = 'biograph/edit';
$route['panel/biograph/profile'] = 'biograph/edit/profile';
$route['panel/biograph/contact'] = 'biograph/edit/contact';
$route['panel/biograph/education'] = 'biograph/edit/education';
$route['panel/biograph/career'] = 'biograph/edit/career';
$route['panel/biograph/education/(:num)'] = 'biograph/edit/education/$1';
$route['panel/biograph/career/(:num)'] = 'biograph/edit/career/$1';
$route['panel/biograph/delete/education/(:num)'] = 'biograph/delete/education/$1';
$route['panel/biograph/delete/career/(:num)'] = 'biograph/delete/career/$1';
