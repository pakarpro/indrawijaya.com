<?php

/* 
 * Set Relative URL
 */
// protocol config
$host = $_SERVER['HTTP_HOST'];
define('RELATIVE_URL', 'http://'.$host.'/');

/* 
 * Set Global Configuration for Database
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'cctvcam_indra');
define('DB_PASS', 'iwDB4.2017');
define('DB_NAME', 'cctvcam_indrawijaya');

/* 
 * Set Global Configuration for accessing directory
 */
define('ASSETS', 'assets/');
define('MAIN_PATH', ASSETS.'main/');
define('PANEL_PATH', ASSETS.'panel/');
define('PLG_PATH', ASSETS.'plugins/');
define('MEDIA_PATH', 'media/');
define('IMG_PATH', MEDIA_PATH.'images/');
define('FILE_PATH', MEDIA_PATH.'files/');
define('VID_PATH', MEDIA_PATH.'videos/');

define('DATE', date('Y-m-d'));
define('TIME', date('H:i:s'));
define('DATETIME', date('Y-m-d H:i:s'));
define('DEFAULT_DATETIME', '0000-00-00 00:00:00');

define('IP_ADDR', $_SERVER['REMOTE_ADDR']);
